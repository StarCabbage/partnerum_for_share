const functions = require('firebase-functions');
const admin = require("firebase-admin");
const dotenv = require('dotenv');

admin.initializeApp();

let serviceAccount = require("./serviceAccountKey.json");
dotenv.config();

const search = require('./algolia').search;
const index = require('./algolia').index;
const notifications = require('./notifications');
const reviews = require('./reviews');

const db = admin.database();
const fcm = admin.messaging();


exports.catchRequestStatus = notifications.catchRequestStatus;

exports.catchHouseStatus = notifications.catchHouseStatus;

exports.catchMessage = notifications.catchMessage;

exports.catchReview = reviews.catchReview;

exports.catchUserReview = reviews.catchUserReview;

exports.scheduledFunction = reviews.scheduledFunction;

async function sendMessage(userId, data2 = 'updateView') {
    let tokens = await db.ref(`/user_data/${userId}/notifications/`).once("value");
    tokens = tokens.val();
    if (tokens == null) return;
    tokens = Object.keys(tokens);
    if (tokens != null) {
        const payload = {
            "data": {
                "link": data2,
            }
        };

        await fcm.sendToDevice(tokens, payload).then(function (response) {
            console.log('updateView data sent successfully:', response);
        })
            .catch(function (error) {
                console.log('data sent failed:', error);
            });
    }
}

function getAnotherUid(users, context) {

    for (let user in users) {
        if (users[user] !== context.auth.uid) {
            return users[user];
        }
    }
}

//important
//if (type !== 'pure_message') {
//                         notificationId = await db.ref(`/user_data/${rUser}/notification_list/`).push();
//                         await notificationId.set({
//                             "type": type,
//                             data: data,
//                             s: body,
//                             ti: title,
//                             time: admin.database.ServerValue.TIMESTAMP
//                         });
//                     } else {
//                         notificationId = await db.ref(`/user_data/${rUser}/message_notification_list/`).child(`${context.auth.uid};${context.params.messageInstanceId}`);
//                         await notificationId.set({
//                             "type": type,
//                             data: data,
//                             s: body,
//                             ti: title,
//                             time: admin.database.ServerValue.TIMESTAMP
//                         });
//                     }


async function sendDataToType(uid, messageInstanceId, type, title, body, url = "", data, data2 = 'updateView') {
    const databaseReference = db.ref(`/message_data/${messageInstanceId}/`);
    let tokensUid = userTokens(uid);
    let tokensAnotherUid = userTokens(uid);

    if (tokens != null) {
        const payload = {
            "data": {
                "link": data2,
            }
        };

        await fcm.sendToDevice(tokens, payload).then(function (response) {
            console.log('data sent successfully:', response);
        })
            .catch(function (error) {
                console.log('data sent failed:', error);
            });
    }


    //if (user != null && user.length > 0) user = user[0];
    let notificationId;

    if (tokens != null) {
        const payload = {
            "notification": {
                "title": title,
                "body": body,
                "icon": url,
                "sound": "default",
                "click_action": 'FLUTTER_NOTIFICATION_CLICK',
            },
            "android": {
                "priority": "normal"
            },
            "data": {
                "link": data,
                "key": notificationId.key,
                "title": title,
                "body": body
            }
        };

        return fcm.sendToDevice(tokens, payload, {priority: 'high'}).then(function (response) {
            console.log('Notification sent successfully:', response);
        })
            .catch(function (error) {
                console.log('Notification sent failed:', error);
            });
    }
}

//if (type !== 'pure_message') {
//         notificationId = await db.ref(`/user_data/${rUser}/notification_list/`).push();
//         await notificationId.set({
//             "type": type,
//             data: data,
//             s: body,
//             ti: title,
//             time: admin.database.ServerValue.TIMESTAMP
//         });
//     } else {
//         notificationId = await db.ref(`/user_data/${rUser}/message_notification_list/`).child(`${context.auth.uid};${context.params.messageInstanceId}`);
//         await notificationId.set({
//             "type": type,
//             data: data,
//             s: body,
//             ti: title,
//             time: admin.database.ServerValue.TIMESTAMP
//         });
//     }




function remove(array, element) {
    const index = array.indexOf(element);
    if (index > -1) {
        array.splice(index, 1);
    }
}


exports.catchNotification = functions.database.ref('/user_data/{userId}/notification_list/')
    .onWrite(async (change, context) => {


            return sendMessage(context.params.userId, 'updateBottom');
        }
    );


exports.catchUserTopicRequest = functions.database.ref('/user_data/{ownerUserId}/message_data/requests/{requestId}/{userId}/{houseId}/')
    .onWrite(async (change, context) => {

        if (!change.after.exists()) {
            return null;
        }

        // Grab the current value of what was written to the Realtime Database.
        //console.log(change.after.val());
        let request = change.after.val();
        let newRef = db.ref(`/user_data/${context.params.userId}/message_data/houses_by_house/${context.params.houseId}/${context.params.ownerUserId}/${context.params.requestId}/`);
        await newRef.set(request);
        newRef = db.ref(`/user_data/${context.params.userId}/message_data/houses/${context.params.ownerUserId}/${context.params.requestId}/${context.params.houseId}`);
        return newRef.set(request);
        console.log(request);


        return null;
        let info = request["info"];
        let stations = info["stations"];
        let city = info["city"];
        let price_range = info["price_range"];
        let facetList = [];
        //let newRef = db.ref("/user_data/" + context.params.userId + "/request_offers/" + context.params.requestId);

        facetList.push(`city:${city}`);
        let i = 0, l = stations.length;
        for (; i < l; i++) {
            facetList.push(`stations:${stations[i]}`);
        }

        let value = await search(facetList, price_range);
        return newRef.set(value["hits"]);
    });


exports.catchUserTopicPure = functions.database.ref('/user_data/{userId}/message_data/pure_messages/{anotherUid}/')
    .onWrite(async (change, context) => {

        let pathUid = `${context.params.anotherUid};${change.after.val()}`;
        let pathAUid = `${context.params.userId};${change.after.val()}`;
        if (!change.after.exists()) {

            await db.ref(`/user_data/${context.params.userId}/chats/`).update({[pathUid]: null});

            return await db.ref(`/user_data/${context.params.anotherUid}/chats/`).update({[pathAUid]: null});
        }

        await db.ref(`/user_data/${context.params.userId}/chats/`).update({[pathUid]: false});
        return await db.ref(`/user_data/${context.params.anotherUid}/chats/`).update({[pathAUid]: false});

    });


exports.updateRequest = functions.database.ref('/user_data/{userId}/requests/{requestId}/')
    .onWrite(async (change, context) => {
        // Only edit data when it is first created.
        //console.log(change.before.val());
        //console.log(change.after.val());
        //console.log(context.params.userId);
        //console.log(context.params.houseId);
        //here we need to make a copy and update value in global list.
        //if (change.before.exists()) {
        //    return null;
        // }
        // Exit when the data is deleted.
        if (!change.after.exists()) {
            return null;
        }
        // Grab the current value of what was written to the Realtime Database.
        //console.log(change.after.val());
        let request = change.after.val();
        let info = request["info"];
        let stations = info["stations"];
        let city = info["city"];
        let price_range = info["price_range"];
        let facetList = [];
        let newRef = db.ref("/user_data/" + context.params.userId + "/request_offers/" + context.params.requestId);

        facetList.push([`city:${city}`]);
        let i = 0, l = stations.length;
        let cityList = [];
        for (; i < l; i++) {
            cityList.push(`stations:${stations[i]}`);
        }
        facetList.push(cityList);
        console.log("start_look");
        console.log(facetList);
        console.log(price_range);
        let value = await search(facetList, price_range);
        console.log("end_look");
        console.log(value);
        //console.log(value);
        // const original = change.after.val();
        // if (original.hasOwnProperty("outerId"))
        //     return uploadNew(original, original["outerId"]);//db.ref("/houses/" + original["outerId"]).update(change.after.val());
        await newRef.set(value["hits"]);
        return sendMessage(context.params.userId);
        //console.log('Uppercasing', context.params.pushId, original);
        //db.ref("/houses/").push().update(change.after.val());
        //const uppercase = original.toUpperCase();
        // You must return a Promise when performing asynchronous tasks inside a Functions such as
        // writing to the Firebase Realtime Database.
        // Setting an "uppercase" sibling in the Realtime Database returns a Promise.
        //return db.ref("/houses/").push().update(change.after.val());//change.after.ref.parent.child('uppercase').set(uppercase);
    });

exports.updateHouse = functions.database.ref('/user_data/{userId}/houses/{houseId}/')
    .onWrite(async (change, context) => {
        let showNotification = false;
        if (!change.before.exists()) {
            showNotification = true;
            //return null;
        }
        // Exit when the data is deleted.
        if (!change.after.exists()) {
            //    return null;
            let object = {};
            object.objectID = context.params.userId + ";" + context.params.houseId;
            return index
                .saveObject(object);
        }

        // Grab the current value of what was written to the Realtime Database.

        async function uploadNew(object1) {
            let object = {};
            //let newRef = db.ref("/houses/");
            // if (outerId === null) {
            //     newRef = await newRef.push();
            //     await change.after.ref.child('outerId').set(newRef.key);
            // } else {
            //     newRef = newRef.child(outerId);
            // }
            object.objectID = context.params.userId + ";" + context.params.houseId;
            //object["userId"] = context.params.userId;
            //object["houseId"] = context.params.houseId;
            if (object1.archive == null || object1.archive === false) {
                object["city"] = object1.info.city;
                object["latLon"] = object1.info.latLon == null ? "" : object1.info.latLon;
                object["stations"] = object1.info.stations;
                object["address"] = object1.info.address;
                object["price_start"] = parseInt(object1.info.price_range[0]);
                object["price_end"] = parseInt(object1.info.price_range[1]);
            }
            await index
                .saveObject(object);
            if (showNotification) {
                const payload = {
                    notification: {
                        title: 'Новая квартира',
                        body: `Внимание, была добавлена новая квартира рядом с метро: ${object1.info.stations.join(", ")}\nПроверьте свою завку, возможно, именно эта квартира подойдет вашему клиенту`,
                        //icon: 'your-icon-url',
                        click_action: 'FLUTTER_NOTIFICATION_CLICK' // required only for onResume or onLaunch callbacks
                    }
                };

                return fcm.sendToTopic('houses', payload);
            }
            //newRef.update(object);

        }

        const original = change.after.val();
        //if (original.hasOwnProperty("outerId"))
        //    return uploadNew(original, original["outerId"]);//db.ref("/houses/" + original["outerId"]).update(change.after.val());
        return uploadNew(original);
        //console.log('Uppercasing', context.params.pushId, original);
        //db.ref("/houses/").push().update(change.after.val());
        //const uppercase = original.toUpperCase();
        // You must return a Promise when performing asynchronous tasks inside a Functions such as
        // writing to the Firebase Realtime Database.
        // Setting an "uppercase" sibling in the Realtime Database returns a Promise.
        //return db.ref("/houses/").push().update(change.after.val());//change.after.ref.parent.child('uppercase').set(uppercase);
    });

