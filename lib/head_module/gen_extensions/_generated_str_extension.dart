/*Внимание! В данном файле нельзя производить изменения вручную. Т.к. генератор просто удалит их.
              Используйте плагин flutter_serve для внесения изменений: flutter pub run flutter_serve*/

class Tooltips {
  ///Меню
  String get hamburger => 'tooltips.hamburger'; /*Меню*/
  ///Поиск
  String get search => 'tooltips.search'; /*Поиск*/
  ///Корзина
  String get cart => 'tooltips.cart'; /*Корзина*/
  ///Закладка
  String get bookmark => 'tooltips.bookmark'; /*Закладка*/
}

class GlobalOnes {
  ///В архив
  String get archive => 'global_ones.archive'; /*В архив*/
  ///Разархивировать
  String get unarchive => 'global_ones.unarchive'; /*Разархивировать*/
  ///Ошибка
  String get error => 'global_ones.error'; /*Ошибка*/
  ///Ошибка вовремя загрузки
  String get upoadError =>
      'global_ones.upoad_error'; /*Ошибка вовремя загрузки*/
  ///Анонимный пользователь
  String get anonymousUser =>
      'global_ones.anonymous_user'; /*Анонимный пользователь*/
}

class Common {
  ///Ошибка
  String get error => 'common.error'; /*Ошибка*/
}

class Pages {
  HomePage get homePage => HomePage();
  RequestsPage get requestsPage => RequestsPage();
  HousesPage get housesPage => HousesPage();
  SuitableApartmentsPage get suitableApartmentsPage => SuitableApartmentsPage();
  ProfilePage get profilePage => ProfilePage();
  ProfileEditPage get profileEditPage => ProfileEditPage();
  ChatsPage get chatsPage => ChatsPage();
  OrdersPage get ordersPage => OrdersPage();
  MessagesPage get messagesPage => MessagesPage();
  LoginPage get loginPage => LoginPage();
  NotificationPage get notificationPage => NotificationPage();
  RequestOfferPage get requestOfferPage => RequestOfferPage();
  MessageInstancePage get messageInstancePage => MessageInstancePage();
  Metro get metro => Metro();
  NotificationsPage get notificationsPage => NotificationsPage();
  Extra get extra => Extra();
}

class HomePage {
  BottomBar get bottomBar => BottomBar();
}

class BottomBar {
  ///Оповещения
  String get notificationsText =>
      'pages.home_page.bottom_bar.notifications_text'; /*Оповещения*/
  ///Заявки
  String get requestText => 'pages.requests_page.title_text'; /*Заявки*/
  ///Профиль
  String get profileText =>
      'pages.home_page.bottom_bar.profile_text'; /*Профиль*/
  ///Чат
  String get chat => 'pages.home_page.bottom_bar.chat'; /*Чат*/
  ///Квартиры
  String get housesText => 'pages.houses_page.title_text'; /*Квартиры*/
}

class RequestsPage {
  ///Архивные заявки
  String get archivedTitleText =>
      'pages.requests_page.archived_title_text'; /*Архивные заявки*/
  ///Здесь будут показаны ваши заявки. Добавляйте заявки, чтобы получать комиссию со сделок.
  String get emptyRequests =>
      'pages.requests_page.empty_requests'; /*Здесь будут показаны ваши заявки. Добавляйте заявки, чтобы получать комиссию со сделок.*/
  ///Заявки
  String get titleText => 'pages.requests_page.title_text'; /*Заявки*/
  RequestsCard get requestsCard => RequestsCard();
  RequestCreatePage get requestCreatePage => RequestCreatePage();
}

class RequestsCard {
  ///Варианты квартир
  String get offerText =>
      'pages.suitable_apartments_page.title_text'; /*Варианты квартир*/
  ///По Вашему запросу ничего не найдено
  String get sorryText =>
      'pages.requests_page.requests_card.sorry_text'; /*По Вашему запросу ничего не найдено*/
}

class RequestCreatePage {
  RequestCreateHelper get requestCreateHelper => RequestCreateHelper();

  ///Более 4
  String get moreThan4 =>
      'pages.requests_page.request_create_page.more_than4'; /*Более 4*/
  ///Цена (от) больше цены (до)
  String get priceIssueSnackBar =>
      'pages.requests_page.request_create_page.price_issue_snack_bar'; /*Цена (от) больше цены (до)*/
  ///Заявка добавлена
  String get requestAddedSnackBar =>
      'pages.requests_page.request_create_page.request_added_snack_bar'; /*Заявка добавлена*/
  ///Заполните недостающие элементы
  String get notEnoughInfo =>
      'pages.requests_page.request_create_page.not_enough_info'; /*Заполните недостающие элементы*/
  ///Добавить заявку
  String get addRequest =>
      'pages.requests_page.request_create_page.add_request'; /*Добавить заявку*/
  ///Изменить заявку
  String get changeRequest =>
      'pages.requests_page.request_create_page.change_request'; /*Изменить заявку*/
  ///Выбран город
  String get choseCityMessage =>
      'pages.requests_page.request_create_page.chose_city_message'; /*Выбран город*/
  ///Выбраны станции
  String get choseMetroMessage =>
      'pages.requests_page.request_create_page.chose_metro_message'; /*Выбраны станции*/
  ///Город
  String get city => 'pages.requests_page.request_create_page.city'; /*Город*/
  ///Метро
  String get metro => 'pages.requests_page.request_create_page.metro'; /*Метро*/
  ///Пешком до метро
  String get timeToMetro =>
      'pages.requests_page.request_create_page.time_to_metro'; /*Пешком до метро*/
  ///мин
  String get timeMeasure =>
      'pages.requests_page.request_create_page.time_measure'; /*мин*/
  ///Обязательно
  String get necessarily =>
      'pages.requests_page.request_create_page.necessarily'; /*Обязательно*/
  ///Количество комнат
  String get roomsAmount =>
      'pages.requests_page.request_create_page.rooms_amount'; /*Количество комнат*/
  ///Неважно
  String get neverMind =>
      'pages.requests_page.request_create_page.never_mind'; /*Неважно*/
  ///Количество гостей
  String get guestsAmount =>
      'pages.requests_page.request_create_page.guests_amount'; /*Количество гостей*/
  ///Более
  String get more => 'pages.requests_page.request_create_page.more'; /*Более*/
  ///Размер комиссии
  String get comissionSize =>
      'pages.requests_page.request_create_page.comission_size'; /*Размер комиссии*/
  ///Цена (от)
  String get priceFrom =>
      'pages.requests_page.request_create_page.price_from'; /*Цена (от)*/
  ///Цена (до)
  String get priceTo =>
      'pages.requests_page.request_create_page.price_to'; /*Цена (до)*/
  ///Выбрать даты
  String get chooseDate =>
      'pages.requests_page.request_create_page.choose_date'; /*Выбрать даты*/
  ///Заезд
  String get entry => 'pages.requests_page.request_create_page.entry'; /*Заезд*/
  ///Выезд
  String get departure =>
      'pages.requests_page.request_create_page.departure'; /*Выезд*/
  ///Введите телефон заказчика
  String get askForPhoneNumber =>
      'pages.requests_page.request_create_page.ask_for_phone_number'; /*Введите телефон заказчика*/
  ///Телефон
  String get phoneNumber =>
      'pages.requests_page.request_create_page.phone_number'; /*Телефон*/
  RequestInfoWidget get requestInfoWidget => RequestInfoWidget();

  ///Телефон будет виден арендодателю только после того, как вы договоритесь
  String get phoneInfoSnackBar =>
      'pages.requests_page.request_create_page.phone_info_snack_bar'; /*Телефон будет виден арендодателю только после того, как вы договоритесь*/
}

class RequestCreateHelper {
  ///Новая заявка
  String get newRequest =>
      'pages.requests_page.request_create_page.request_create_helper.new_request'; /*Новая заявка*/
}

class RequestInfoWidget {
  ///Укажите дополнительную информацию
  String get addExtraInfo =>
      'pages.requests_page.request_create_page.request_info_widget.add_extra_info'; /*Укажите дополнительную информацию*/
  ///Дополнительная информация
  String get extraInfo =>
      'pages.requests_page.request_create_page.request_info_widget.extra_info'; /*Дополнительная информация*/
}

class HousesPage {
  ///Здесь будут показаны ваши квартиры. Добавляйте квартиры и сервис автоматически подберёт подходящие заявки.
  String get emptyScreenMessage =>
      'pages.houses_page.empty_screen_message'; /*Здесь будут показаны ваши квартиры. Добавляйте квартиры и сервис автоматически подберёт подходящие заявки.*/
  ///Квартиры
  String get titleText => 'pages.houses_page.title_text'; /*Квартиры*/
  ///Мои квартиры
  String get myFlats => 'pages.houses_page.my_flats'; /*Мои квартиры*/
  ///Архив
  String get archive => 'pages.houses_page.archive'; /*Архив*/
  HousesCard get housesCard => HousesCard();
  HouseCreatePage get houseCreatePage => HouseCreatePage();
  HouseDetailsPage get houseDetailsPage => HouseDetailsPage();
  NewVersionHouseDetails get newVersionHouseDetails => NewVersionHouseDetails();
  HouseOffersPage get houseOffersPage => HouseOffersPage();
}

class HousesCard {
  Price get price => Price();

  ///На данный момент никто не подал заявку
  String get nobodyInterestedIn =>
      'pages.houses_page.houses_card.nobody_interested_in'; /*На данный момент никто не подал заявку*/
  ///Заявки
  String get offerText => 'pages.houses_page.houses_card.offer_text'; /*Заявки*/
}

class Price {
  ///От
  String get from => 'pages.houses_page.houses_card.price.from'; /*От*/
  ///до
  String get to => 'pages.houses_page.houses_card.price.to'; /*до*/
  ///руб/сут
  String get measure =>
      'pages.houses_page.houses_card.price.measure'; /*руб/сут*/
}

class HouseCreatePage {
  ///Новая квартира
  String get newFlat =>
      'pages.houses_page.house_create_page.new_flat'; /*Новая квартира*/
  ///Изменить квартиру
  String get changeFlat =>
      'pages.houses_page.house_create_page.change_flat'; /*Изменить квартиру*/
  ///Добавить квартиру
  String get addFlat =>
      'pages.houses_page.house_create_page.add_flat'; /*Добавить квартиру*/
  ///Цена (от) больше цены (до)
  String get priceSnackBar =>
      'pages.houses_page.house_create_page.price_snack_bar'; /*Цена (от) больше цены (до)*/
  ///Квартира добавлена
  String get flatAddedMessage =>
      'pages.houses_page.house_create_page.flat_added_message'; /*Квартира добавлена*/
  ///Заполните недостающие элементы
  String get notEnoughInfo =>
      'pages.houses_page.house_create_page.not_enough_info'; /*Заполните недостающие элементы*/
  InfoWidget get infoWidget => InfoWidget();

  ///Добавить фото
  String get addPhoto =>
      'pages.houses_page.house_create_page.add_photo'; /*Добавить фото*/
  ///Более 4
  String get moreThan4 =>
      'pages.houses_page.house_create_page.more_than4'; /*Более 4*/
  ///Количество комнат
  String get roomsAmount =>
      'pages.houses_page.house_create_page.rooms_amount'; /*Количество комнат*/
  ///Цена (от)
  String get priceFrom =>
      'pages.houses_page.house_create_page.price_from'; /*Цена (от)*/
  ///Цена (до)
  String get priceTo =>
      'pages.houses_page.house_create_page.price_to'; /*Цена (до)*/
  ///Поле не должно быть пустым
  String get notEmptyPriceField =>
      'pages.houses_page.house_create_page.not_empty_price_field'; /*Поле не должно быть пустым*/
  ///Город
  String get city => 'pages.houses_page.house_create_page.city'; /*Город*/
  ///Метро
  String get metro => 'pages.houses_page.house_create_page.metro'; /*Метро*/
  ///Пешком до метро
  String get timeToMetro =>
      'pages.houses_page.house_create_page.time_to_metro'; /*Пешком до метро*/
  ///мин
  String get timeMeasure =>
      'pages.houses_page.house_create_page.time_measure'; /*мин*/
  ///Обязательно
  String get necessarily =>
      'pages.houses_page.house_create_page.necessarily'; /*Обязательно*/
  ///Адрес
  String get address => 'pages.houses_page.house_create_page.address'; /*Адрес*/
  ///Загрузите фото
  String get uploadPhotoSnackBar =>
      'pages.houses_page.house_create_page.upload_photo_snack_bar'; /*Загрузите фото*/
  ///Введите адрес
  String get needAddress =>
      'pages.houses_page.house_create_page.need_address'; /*Введите адрес*/
}

class InfoWidget {
  ///Укажите дополнительную информацию
  String get addExtraInfo =>
      'pages.houses_page.house_create_page.info_widget.add_extra_info'; /*Укажите дополнительную информацию*/
  ///Дополнительная информация
  String get extraInfo =>
      'pages.houses_page.house_create_page.info_widget.extra_info'; /*Дополнительная информация*/
}

class HouseDetailsPage {
  ///О квартире
  String get aboutFlat =>
      'pages.houses_page.house_details_page.about_flat'; /*О квартире*/
  ///Более 4
  String get moreThan4 =>
      'pages.houses_page.house_details_page.more_than4'; /*Более 4*/
  ///Информация
  String get info => 'pages.houses_page.house_details_page.info'; /*Информация*/
  ///Не указана
  String get notStated =>
      'pages.houses_page.house_details_page.not_stated'; /*Не указана*/
  ///Количество комнат
  String get roomsAmount =>
      'pages.houses_page.house_details_page.rooms_amount'; /*Количество комнат*/
  ///Цена
  String get price => 'pages.houses_page.house_details_page.price'; /*Цена*/
  ///От
  String get priceFrom =>
      'pages.houses_page.house_details_page.price_from'; /*От*/
  ///до
  String get priceTo => 'pages.houses_page.house_details_page.price_to'; /*до*/
  ///руб/сут
  String get priceMeasure =>
      'pages.houses_page.house_details_page.price_measure'; /*руб/сут*/
  ///Город
  String get city => 'pages.houses_page.house_details_page.city'; /*Город*/
  ///Метро
  String get metro => 'pages.houses_page.house_details_page.metro'; /*Метро*/
  ///Пешком до метро
  String get timeToMetro =>
      'pages.houses_page.house_details_page.time_to_metro'; /*Пешком до метро*/
  ///мин
  String get timeMeasure =>
      'pages.houses_page.house_details_page.time_measure'; /*мин*/
  ///Обязательно
  String get necessarily =>
      'pages.houses_page.house_details_page.necessarily'; /*Обязательно*/
  ///Адрес
  String get address =>
      'pages.houses_page.house_details_page.address'; /*Адрес*/
  ///Станции метро
  String get metroStations =>
      'pages.houses_page.house_details_page.metro_stations'; /*Станции метро*/
}

class NewVersionHouseDetails {
  ///Больше 4
  String get moreThan4 =>
      'pages.houses_page.new_version_house_details.more_than4'; /*Больше 4*/
  ///Ошибка
  String get mistake =>
      'pages.houses_page.new_version_house_details.mistake'; /*Ошибка*/
  ///Назад
  String get back =>
      'pages.houses_page.new_version_house_details.back'; /*Назад*/
  ///Настроить закрытые даты
  String get setClosingDates =>
      'pages.houses_page.new_version_house_details.set_closing_dates'; /*Настроить закрытые даты*/
  ///Редактирование дат бронирования
  String get editBookDates =>
      'pages.houses_page.new_version_house_details.edit_book_dates'; /*Редактирование дат бронирования*/
  ///Включено
  String get turnedOn =>
      'pages.houses_page.new_version_house_details.turned_on'; /*Включено*/
  ///Выключено
  String get turnedOff =>
      'pages.houses_page.new_version_house_details.turned_off'; /*Выключено*/
  ///автоматическое добавление дат бронирования при заключении сделки
  String get autoDatesSettingsTitle =>
      'pages.houses_page.new_version_house_details.auto_dates_settings_title'; /*автоматическое добавление дат бронирования при заключении сделки*/
  ///Добавить новый период бронирования
  String get addNewBookPeriod =>
      'pages.houses_page.new_version_house_details.add_new_book_period'; /*Добавить новый период бронирования*/
  ///Подробнее о предложении
  String get moreAboutOffer =>
      'pages.houses_page.new_version_house_details.more_about_offer'; /*Подробнее о предложении*/
  ///Информация
  String get info =>
      'pages.houses_page.new_version_house_details.info'; /*Информация*/
  ///Не указана
  String get notStated =>
      'pages.houses_page.new_version_house_details.not_stated'; /*Не указана*/
  ///Количество комнат
  String get roomsAmount =>
      'pages.houses_page.new_version_house_details.rooms_amount'; /*Количество комнат*/
  ///Цена
  String get price =>
      'pages.houses_page.new_version_house_details.price'; /*Цена*/
  ///От
  String get priceFrom =>
      'pages.houses_page.new_version_house_details.price_from'; /*От*/
  ///до
  String get priceTo =>
      'pages.houses_page.new_version_house_details.price_to'; /*до*/
  ///руб/сут
  String get priceMeasure =>
      'pages.houses_page.new_version_house_details.price_measure'; /*руб/сут*/
  ///Город
  String get city =>
      'pages.houses_page.new_version_house_details.city'; /*Город*/
  ///Метро
  String get metro =>
      'pages.houses_page.new_version_house_details.metro'; /*Метро*/
  ///Пешком до метро
  String get timeToMetro =>
      'pages.houses_page.new_version_house_details.time_to_metro'; /*Пешком до метро*/
  ///мин
  String get timeMeasure =>
      'pages.houses_page.new_version_house_details.time_measure'; /*мин*/
  ///Обязательно
  String get necessarily =>
      'pages.houses_page.new_version_house_details.necessarily'; /*Обязательно*/
  ///Адрес
  String get address =>
      'pages.houses_page.new_version_house_details.address'; /*Адрес*/
  ///Станции метро
  String get metroStations =>
      'pages.houses_page.new_version_house_details.metro_stations'; /*Станции метро*/
}

class HouseOffersPage {
  ///Новые заявки
  String get newRequests =>
      'pages.houses_page.house_offers_page.new_requests'; /*Новые заявки*/
  ///Одобренные заявки
  String get acceptedRequests =>
      'pages.houses_page.house_offers_page.accepted_requests'; /*Одобренные заявки*/
  ///Заявки в брони
  String get bookedRequests =>
      'pages.houses_page.house_offers_page.booked_requests'; /*Заявки в брони*/
  ///Отклоненные заявки
  String get declinedRequests =>
      'pages.houses_page.house_offers_page.declined_requests'; /*Отклоненные заявки*/
  ///активные
  String get active =>
      'pages.houses_page.house_offers_page.active'; /*активные*/
  ///Скрыть архивные заявки
  String get hideArchivedRequests =>
      'pages.houses_page.house_offers_page.hide_archived_requests'; /*Скрыть архивные заявки*/
  ///Показывать заявки
  String get showRequests =>
      'pages.houses_page.house_offers_page.show_requests'; /*Показывать заявки*/
  ///Порядок расположения заявок
  String get orderOfRequests =>
      'pages.houses_page.house_offers_page.order_of_requests'; /*Порядок расположения заявок*/
  ///Активные заявки
  String get activeRequests =>
      'pages.houses_page.house_offers_page.active_requests'; /*Активные заявки*/
  ///Заявки
  String get requests =>
      'pages.houses_page.house_offers_page.requests'; /*Заявки*/
  ///Архивные заявки
  String get archivedRequests =>
      'pages.houses_page.house_offers_page.archived_requests'; /*Архивные заявки*/
  ///Здесь будут показаны заявки на вашу квартиру
  String get emptyRequestsList =>
      'pages.houses_page.house_offers_page.empty_requests_list'; /*Здесь будут показаны заявки на вашу квартиру*/
}

class SuitableApartmentsPage {
  ///Варианты квартир
  String get titleText =>
      'pages.suitable_apartments_page.title_text'; /*Варианты квартир*/
  ///Подходящие квартиры
  String get suitableApartmentsText =>
      'pages.suitable_apartments_page.suitable_apartments_text'; /*Подходящие квартиры*/
  ///Ожидающие одобрения арендодателя
  String get selectedApartmentsText =>
      'pages.suitable_apartments_page.selected_apartments_text'; /*Ожидающие одобрения арендодателя*/
  ///Предложить заявку
  String get makeOfferText =>
      'pages.suitable_apartments_page.make_offer_text'; /*Предложить заявку*/
  ///Отменить заявку
  String get cancelOfferText =>
      'pages.suitable_apartments_page.cancel_offer_text'; /*Отменить заявку*/
  ///Ваша заявка
  String get yoursRequest =>
      'pages.suitable_apartments_page.yours_request'; /*Ваша заявка*/
  ///Больше нет квартир
  String get noMoreFlats =>
      'pages.suitable_apartments_page.no_more_flats'; /*Больше нет квартир*/
  ///Подходящих квартир не найдено
  String get noVariants =>
      'pages.suitable_apartments_page.no_variants'; /*Подходящих квартир не найдено*/
  ///Заявка
  String get request => 'pages.suitable_apartments_page.request'; /*Заявка*/
  ///Подробнее
  String get more => 'pages.suitable_apartments_page.more'; /*Подробнее*/
  ///Скрыть уже поданные заявки
  String get hideSendedRequests =>
      'pages.suitable_apartments_page.hide_sended_requests'; /*Скрыть уже поданные заявки*/
}

class ProfilePage {
  ProfileHelper get profileHelper => ProfileHelper();

  ///Ошибка
  String get error => 'pages.profile_page.error'; /*Ошибка*/
  ///Профиль
  String get profile => 'pages.profile_page.profile'; /*Профиль*/
  ///Анонимный пользователь
  String get anonymous =>
      'pages.profile_page.anonymous'; /*Анонимный пользователь*/
  ///Загрузить документ
  String get uploadDocument =>
      'pages.profile_page.upload_document'; /*Загрузить документ*/
  ///Статус: в обработке
  String get inProcess =>
      'pages.profile_page.in_process'; /*Статус: в обработке*/
  ///Обновить документ
  String get updateDocument =>
      'pages.profile_page.update_document'; /*Обновить документ*/
  ///Статус
  String get status => 'pages.profile_page.status'; /*Статус*/
  ///Удаление аккаунта
  String get deleteAccount =>
      'pages.profile_page.delete_account'; /*Удаление аккаунта*/
  ///Подтвердите удаление учетной записи
  String get deleteWarning =>
      'pages.profile_page.delete_warning'; /*Подтвердите удаление учетной записи*/
  ///Да
  String get yes => 'pages.profile_page.yes'; /*Да*/
  ///Нет
  String get no => 'pages.profile_page.no'; /*Нет*/
  ///Подтверждение
  String get confirm => 'pages.profile_page.confirm'; /*Подтверждение*/
  ///Отправлено SMS на этот номер телефона. Введите полученный код
  String get sendCodeMessage =>
      'pages.profile_page.send_code_message'; /*Отправлено SMS на этот номер телефона. Введите полученный код*/
  ///Загрузите фотографию паспорта
  String get askForUploadDocument =>
      'pages.profile_page.ask_for_upload_document'; /*Загрузите фотографию паспорта*/
  ///После загрузки фотографии, Вам напишет поддержка
  String get askForUploadDocumentSubtitle =>
      'pages.profile_page.ask_for_upload_document_subtitle'; /*После загрузки фотографии, Вам напишет поддержка*/
  ///Сделать фото
  String get makePhoto => 'pages.profile_page.make_photo'; /*Сделать фото*/
  ///Выбрать в галерее
  String get chooseInGallery =>
      'pages.profile_page.choose_in_gallery'; /*Выбрать в галерее*/
  ///Документ загружен
  String get documentUploaded =>
      'pages.profile_page.document_uploaded'; /*Документ загружен*/
  ///Ваш профиль не подтвержден. Чтобы подтвердить его, загрузите фото паспорта.
  String get askForConfirm =>
      'pages.profile_page.ask_for_confirm'; /*Ваш профиль не подтвержден. Чтобы подтвердить его, загрузите фото паспорта.*/
  ///Фото паспорта ожидает проверки админа.
  String get uploadDocumentStatus =>
      'pages.profile_page.upload_document_status'; /*Фото паспорта ожидает проверки админа.*/
}

class ProfileHelper {
  ///в обработке
  String get inProgress =>
      'pages.profile_page.profile_helper.in_progress'; /*в обработке*/
  ///подтверждено
  String get accepted =>
      'pages.profile_page.profile_helper.accepted'; /*подтверждено*/
}

class ProfileEditPage {
  ///Редактирование профиля
  String get title =>
      'pages.profile_edit_page.title'; /*Редактирование профиля*/
  ///Введите свое имя и инициалы
  String get enterName =>
      'pages.profile_edit_page.enter_name'; /*Введите свое имя и инициалы*/
  ///Имя изменено
  String get changedNameMessage =>
      'pages.profile_edit_page.changed_name_message'; /*Имя изменено*/
  ///Фото загружено
  String get photoUploaded =>
      'pages.profile_edit_page.photo_uploaded'; /*Фото загружено*/
}

class ChatsPage {
  RateWidget get rateWidget => RateWidget();

  ///Диалоги
  String get titleDialogs => 'pages.chats_page.title_dialogs'; /*Диалоги*/
  ChatsHelper get chatsHelper => ChatsHelper();

  ///Топик
  String get topic => 'pages.chats_page.topic'; /*Топик*/
  ///Заблокировать
  String get block => 'pages.chats_page.block'; /*Заблокировать*/
  ///Внимание
  String get attention => 'pages.chats_page.attention'; /*Внимание*/
  ///Вы уверены, что хотите заблокировать этого пользователя?
  String get blockingMessage =>
      'pages.chats_page.blocking_message'; /*Вы уверены, что хотите заблокировать этого пользователя?*/
  ///ДА
  String get yes => 'pages.chats_page.yes'; /*ДА*/
  ///НЕТ
  String get no => 'pages.chats_page.no'; /*НЕТ*/
  ///Ваша заявка принята и будет расмотрена в течении 24 часов!
  String get reviewMessage =>
      'pages.chats_page.review_message'; /*Ваша заявка принята и будет расмотрена в течении 24 часов!*/
}

class RateWidget {
  ///Оставить отзыв
  String get giveFeedback =>
      'pages.chats_page.rate_widget.give_feedback'; /*Оставить отзыв*/
  ///Подождите, происходит загрузка отзыва
  String get waitForLoading =>
      'pages.chats_page.rate_widget.wait_for_loading'; /*Подождите, происходит загрузка отзыва*/
  ///Посмотреть отзыв
  String get checkReview =>
      'pages.chats_page.rate_widget.check_review'; /*Посмотреть отзыв*/
  ///отзыв
  String get review => 'pages.chats_page.rate_widget.review'; /*отзыв*/
  ///Оценка
  String get mark => 'pages.chats_page.rate_widget.mark'; /*Оценка*/
  ///Вам отзыв!
  String get newReview =>
      'pages.chats_page.rate_widget.new_review'; /*Вам отзыв!*/
  ///Поделитесь вашим опытом с другими
  String get askForReview =>
      'pages.chats_page.rate_widget.ask_for_review'; /*Поделитесь вашим опытом с другими*/
  ///Прочитайте и напишите ответ^^
  String get askForAnswer =>
      'pages.chats_page.rate_widget.ask_for_answer'; /*Прочитайте и напишите ответ^^*/
  ///Прошло слишком много времени
  String get timeOutMessage =>
      'pages.chats_page.rate_widget.time_out_message'; /*Прошло слишком много времени*/
  ///Оспорить
  String get argue => 'pages.chats_page.rate_widget.argue'; /*Оспорить*/
  ///Отправить
  String get send => 'pages.chats_page.rate_widget.send'; /*Отправить*/
  ///Отмена
  String get cancel => 'pages.chats_page.rate_widget.cancel'; /*Отмена*/
  ///Оставьте подробное описание
  String get reviewHintText =>
      'pages.chats_page.rate_widget.review_hint_text'; /*Оставьте подробное описание*/
  ///Оставьте Ваш комментарий
  String get answerHintText =>
      'pages.chats_page.rate_widget.answer_hint_text'; /*Оставьте Ваш комментарий*/
}

class ChatsHelper {
  ///Пока у вас нет чатов
  String get noChats =>
      'pages.chats_page.chats_helper.no_chats'; /*Пока у вас нет чатов*/
}

class OrdersPage {
  OrderDetail get orderDetail => OrderDetail();

  ///Редактировать
  String get edit => 'pages.orders_page.edit'; /*Редактировать*/
  ///От
  String get priceFrom => 'pages.orders_page.price_from'; /*От*/
  ///до
  String get priceTo => 'pages.orders_page.price_to'; /*до*/
  ///руб/сут
  String get priceMeasure => 'pages.orders_page.price_measure'; /*руб/сут*/
  HouseRequestCard get houseRequestCard => HouseRequestCard();
  HomeOrderDetailPage get homeOrderDetailPage => HomeOrderDetailPage();
}

class OrderDetail {
  ///Информация
  String get info => 'pages.orders_page.order_detail.info'; /*Информация*/
  ///Размер комиссии
  String get comissionSize =>
      'pages.orders_page.order_detail.comission_size'; /*Размер комиссии*/
  ///Количество гостей
  String get guestsAmount =>
      'pages.orders_page.order_detail.guests_amount'; /*Количество гостей*/
  ///Количество комнат
  String get roomsAmount =>
      'pages.orders_page.order_detail.rooms_amount'; /*Количество комнат*/
  ///Цена
  String get price => 'pages.orders_page.order_detail.price'; /*Цена*/
  ///Город
  String get city => 'pages.orders_page.order_detail.city'; /*Город*/
  ///Станции метро
  String get metroStations =>
      'pages.orders_page.order_detail.metro_stations'; /*Станции метро*/
  ///Пешком до метро
  String get timeToMetro =>
      'pages.orders_page.order_detail.time_to_metro'; /*Пешком до метро*/
  OrderDetailHelper get orderDetailHelper => OrderDetailHelper();
}

class OrderDetailHelper {
  ///Новая заявка
  String get newRequest =>
      'pages.orders_page.order_detail.order_detail_helper.new_request'; /*Новая заявка*/
  ///Не указана
  String get notStated =>
      'pages.orders_page.order_detail.order_detail_helper.not_stated'; /*Не указана*/
  ///- минут
  String get noMinutes =>
      'pages.orders_page.order_detail.order_detail_helper.no_minutes'; /*- минут*/
  ///минут(а)
  String get timeMeasure =>
      'pages.orders_page.order_detail.order_detail_helper.time_measure'; /*минут(а)*/
  ///Бесценно
  String get noPrice =>
      'pages.orders_page.order_detail.order_detail_helper.no_price'; /*Бесценно*/
  ///От
  String get priceFrom =>
      'pages.orders_page.order_detail.order_detail_helper.price_from'; /*От*/
  ///до
  String get priceTo =>
      'pages.orders_page.order_detail.order_detail_helper.price_to'; /*до*/
  ///руб/сут
  String get priceMeasure =>
      'pages.orders_page.order_detail.order_detail_helper.price_measure'; /*руб/сут*/
}

class HouseRequestCard {
  ///Бронь утверждена
  String get bookApproved =>
      'pages.orders_page.house_request_card.book_approved'; /*Бронь утверждена*/
  ///Дополнительные действия
  String get extraActions =>
      'pages.orders_page.house_request_card.extra_actions'; /*Дополнительные действия*/
  ///Чат
  String get chat => 'pages.orders_page.house_request_card.chat'; /*Чат*/
  ///Одобрить заявку
  String get acceptRequest =>
      'pages.orders_page.house_request_card.accept_request'; /*Одобрить заявку*/
  ///Вы собираетесь отклонить заявку
  String get declineRequestMessage =>
      'pages.orders_page.house_request_card.decline_request_message'; /*Вы собираетесь отклонить заявку*/
  ///Если есть возможность, напишите причину
  String get reasonToDeclineAsking =>
      'pages.orders_page.house_request_card.reason_to_decline_asking'; /*Если есть возможность, напишите причину*/
  ///Причина
  String get reasonToDecline =>
      'pages.orders_page.house_request_card.reason_to_decline'; /*Причина*/
  ///Отправить
  String get send => 'pages.orders_page.house_request_card.send'; /*Отправить*/
  ///Отклонить заявку
  String get declineRequest =>
      'pages.orders_page.house_request_card.decline_request'; /*Отклонить заявку*/
  ///пользователь отклонил заявку
  String get userDeclinedRequest =>
      'pages.orders_page.house_request_card.user_declined_request'; /*пользователь отклонил заявку*/
  ///Бронировать
  String get book =>
      'pages.orders_page.house_request_card.book'; /*Бронировать*/
  ///Бронирование дат
  String get datesBooking =>
      'pages.orders_page.house_request_card.dates_booking'; /*Бронирование дат*/
  ///Забронировать указанный период
  String get bookPeriod =>
      'pages.orders_page.house_request_card.book_period'; /*Забронировать указанный период*/
  ///для выбранной квартиры
  String get forChosenFlat =>
      'pages.orders_page.house_request_card.for_chosen_flat'; /*для выбранной квартиры*/
  ///Нет
  String get no => 'pages.orders_page.house_request_card.no'; /*Нет*/
  ///Да
  String get yes => 'pages.orders_page.house_request_card.yes'; /*Да*/
  ///От
  String get priceFrom =>
      'pages.orders_page.house_request_card.price_from'; /*От*/
  ///до
  String get priceTo => 'pages.orders_page.house_request_card.price_to'; /*до*/
  ///руб/сут
  String get priceMeasure =>
      'pages.orders_page.house_request_card.price_measure'; /*руб/сут*/
}

class HomeOrderDetailPage {
  ///Ошибка загрузки
  String get uploadIssue =>
      'pages.orders_page.home_order_detail_page.upload_issue'; /*Ошибка загрузки*/
  ///размер комиссии
  String get commissionSize =>
      'pages.orders_page.home_order_detail_page.commission_size'; /*размер комиссии*/
  ///Количество гостей
  String get guestsAmount =>
      'pages.orders_page.home_order_detail_page.guests_amount'; /*Количество гостей*/
  ///чел
  String get guestsAmountMeasure =>
      'pages.orders_page.home_order_detail_page.guests_amount_measure'; /*чел*/
  ///Цена
  String get price => 'pages.orders_page.home_order_detail_page.price'; /*Цена*/
  ///От
  String get priceFrom =>
      'pages.orders_page.home_order_detail_page.price_from'; /*От*/
  ///до
  String get priceTo =>
      'pages.orders_page.home_order_detail_page.price_to'; /*до*/
  ///руб/сут
  String get priceMeasure =>
      'pages.orders_page.home_order_detail_page.price_measure'; /*руб/сут*/
  ///Город
  String get city => 'pages.orders_page.home_order_detail_page.city'; /*Город*/
  ///Станции метро ()
  String get metroStations =>
      'pages.orders_page.home_order_detail_page.metro_stations'; /*Станции метро ()*/
  ///Пешком до метро
  String get timeToMetro =>
      'pages.orders_page.home_order_detail_page.time_to_metro'; /*Пешком до метро*/
  ///Чат
  String get chat => 'pages.orders_page.home_order_detail_page.chat'; /*Чат*/
}

class MessagesPage {
  ///Начало переписки
  String get chattingBegin =>
      'pages.messages_page.chatting_begin'; /*Начало переписки*/
  Card get card => Card();
  Actions get actions => Actions();
}

class Card {
  ///Событие
  String get event => 'pages.messages_page.card.event'; /*Событие*/
}

class Actions {
  ///Бронировать
  String get book => 'pages.messages_page.actions.book'; /*Бронировать*/
  ///Пожаловаться
  String get complain =>
      'pages.messages_page.actions.complain'; /*Пожаловаться*/
  ///Показать клиенту
  String get shareWithClient =>
      'pages.messages_page.actions.share_with_client'; /*Показать клиенту*/
  ///Отказаться от заявки
  String get refuseOffer =>
      'pages.messages_page.actions.refuse_offer'; /*Отказаться от заявки*/
}

class LoginPage {
  ///Для входа в приложение введите свой номер телефона с кодом страны \n(Россия: +7)
  String get registrationMessage =>
      'pages.login_page.registration_message'; /*Для входа в приложение введите свой номер телефона с кодом страны \n(Россия: +7)*/
  ///Согласен с условиями пользовательского соглашения
  String get agreeWithRules =>
      'pages.login_page.agree_with_rules'; /*Согласен с условиями пользовательского соглашения*/
  ///или
  String get or => 'pages.login_page.or'; /*или*/
  ///ВОЙТИ
  String get enter => 'pages.login_page.enter'; /*ВОЙТИ*/
  ///ПОЛУЧИТЬ КОД
  String get getCode => 'pages.login_page.get_code'; /*ПОЛУЧИТЬ КОД*/
  ///Телефон
  String get phone => 'pages.login_page.phone'; /*Телефон*/
  ///+7 (xxx) xxx-xx-xx
  String get phoneNumber =>
      'pages.login_page.phone_number'; /*+7 (xxx) xxx-xx-xx*/
  ///Телефон не может быть пустым
  String get notEmptyPhoneNumber =>
      'pages.login_page.not_empty_phone_number'; /*Телефон не может быть пустым*/
  ///Код
  String get code => 'pages.login_page.code'; /*Код*/
  ///Введите 6-ти значный код подтверждения
  String get askForEnterCode =>
      'pages.login_page.ask_for_enter_code'; /*Введите 6-ти значный код подтверждения*/
  ///Одноразовый пароль не может быть пустым
  String get oneOffCodeEmptyIssue =>
      'pages.login_page.one_off_code_empty_issue'; /*Одноразовый пароль не может быть пустым*/
  ///Пожалуйста, введите 6-значный правильный номер
  String get oneOffCodeIncompleteIssue =>
      'pages.login_page.one_off_code_incomplete_issue'; /*Пожалуйста, введите 6-значный правильный номер*/
  ///Отправить код повторно
  String get sendCodeAgain =>
      'pages.login_page.send_code_again'; /*Отправить код повторно*/
  ///Сменить номер
  String get changeNumber => 'pages.login_page.change_number'; /*Сменить номер*/
  ///Введите свой номер телефона вместе с кодом страны (Россия: +7).
  String get changeNumberMessage =>
      'pages.login_page.change_number_message'; /*Введите свой номер телефона вместе с кодом страны (Россия: +7).*/
  ///Код подтверждения отправлен на номер:
  String get oneOffCodeSentToNumber =>
      'pages.login_page.one_off_code_sent_to_number'; /*Код подтверждения отправлен на номер:*/
  ///Пожалуйста, укажите правильный номер!
  String get askForCorrectPhoneNumber =>
      'pages.login_page.ask_for_correct_phone_number'; /*Пожалуйста, укажите правильный номер!*/
  ///Пожалуйста, укажите код страны (Россия: +7)!
  String get askForCorrectPhoneCode =>
      'pages.login_page.ask_for_correct_phone_code'; /*Пожалуйста, укажите код страны (Россия: +7)!*/
  ///Сайт
  String get website => 'pages.login_page.website'; /*Сайт*/
  ///не найден
  String get notFound => 'pages.login_page.not_found'; /*не найден*/
}

class NotificationPage {
  MessageNotifications get messageNotifications => MessageNotifications();
  ApplicationsNotifications get applicationsNotifications =>
      ApplicationsNotifications();
  ApartmentsNotifications get apartmentsNotifications =>
      ApartmentsNotifications();
  YourApplicationsNotifications get yourApplicationsNotifications =>
      YourApplicationsNotifications();
}

class MessageNotifications {
  ///Сообщения
  String get messages =>
      'pages.notification_page.message_notifications.messages'; /*Сообщения*/
  ///Сообщение
  String get message =>
      'pages.notification_page.message_notifications.message'; /*Сообщение*/
  ///Открыть больше
  String get openMore =>
      'pages.notification_page.message_notifications.open_more'; /*Открыть больше*/
}

class ApplicationsNotifications {
  ///Новые заявки
  String get newRequests =>
      'pages.notification_page.applications_notifications.new_requests'; /*Новые заявки*/
  ///Москва. Требуется..
  String get moscowNeed =>
      'pages.notification_page.applications_notifications.moscow_need'; /*Москва. Требуется..*/
  ///СПБ. Фрунзенская. Требуется..
  String get spbNeed =>
      'pages.notification_page.applications_notifications.spb_need'; /*СПБ. Фрунзенская. Требуется..*/
  ///Просмотреть весь список заявок
  String get allRequests =>
      'pages.notification_page.applications_notifications.all_requests'; /*Просмотреть весь список заявок*/
}

class ApartmentsNotifications {
  ///Ваши квартиры
  String get yourFlats =>
      'pages.notification_page.apartments_notifications.your_flats'; /*Ваши квартиры*/
  ///Москва. Требуется..
  String get moscowNeed =>
      'pages.notification_page.apartments_notifications.moscow_need'; /*Москва. Требуется..*/
  ///СПБ. Фрунзенская. Требуется..
  String get spbNeed =>
      'pages.notification_page.apartments_notifications.spb_need'; /*СПБ. Фрунзенская. Требуется..*/
  ///В данный момент все квартиры арендованы
  String get allFlatsBooked =>
      'pages.notification_page.apartments_notifications.all_flats_booked'; /*В данный момент все квартиры арендованы*/
  ///Управление
  String get management =>
      'pages.notification_page.apartments_notifications.management'; /*Управление*/
}

class YourApplicationsNotifications {
  ///Ваши заявки
  String get yourRequests =>
      'pages.notification_page.your_applications_notifications.your_requests'; /*Ваши заявки*/
  ///Москва. Требуется..
  String get moscowNeed =>
      'pages.notification_page.your_applications_notifications.moscow_need'; /*Москва. Требуется..*/
  ///СПБ. Фрунзенская. Требуется..
  String get spbNeed =>
      'pages.notification_page.your_applications_notifications.spb_need'; /*СПБ. Фрунзенская. Требуется..*/
  ///Редактировать
  String get edit =>
      'pages.notification_page.your_applications_notifications.edit'; /*Редактировать*/
}

class RequestOfferPage {
  ///От
  String get priceFrom => 'pages.request_offer_page.price_from'; /*От*/
  ///до
  String get priceTo => 'pages.request_offer_page.price_to'; /*до*/
  ///руб/сут
  String get priceMeasure =>
      'pages.request_offer_page.price_measure'; /*руб/сут*/
  ///Станции метро
  String get metroStations =>
      'pages.request_offer_page.metro_stations'; /*Станции метро*/
  ///Адрес
  String get address => 'pages.request_offer_page.address'; /*Адрес*/
  ///Цена
  String get price => 'pages.request_offer_page.price'; /*Цена*/
  ///К сожалению в выбраннные Вами даты невозможно запросить
  String get datesIssueApologizing =>
      'pages.request_offer_page.dates_issue_apologizing'; /*К сожалению в выбраннные Вами даты невозможно запросить*/
  /// бронь, квартира забронирована в следующеи даты.
  String get bookingMessage =>
      'pages.request_offer_page.booking_message'; /* бронь, квартира забронирована в следующеи даты.*/
  ///Изменить даты в текущей заявке
  String get changeDatesInOffer =>
      'pages.request_offer_page.change_dates_in_offer'; /*Изменить даты в текущей заявке*/
  ///Рейтинг
  String get rating => 'pages.request_offer_page.rating'; /*Рейтинг*/
  ///Запросить больше фото
  String get askForMorePhotos =>
      'pages.request_offer_page.ask_for_more_photos'; /*Запросить больше фото*/
  ///Попросите владельца добавить больше фото
  String get askOwnerForPhotos =>
      'pages.request_offer_page.ask_owner_for_photos'; /*Попросите владельца добавить больше фото*/
  ///Нет ни оценок, не отзывов
  String get emptyRating =>
      'pages.request_offer_page.empty_rating'; /*Нет ни оценок, не отзывов*/
  ///владелец отклонил заявку
  String get ownerDeclinedRequest =>
      'pages.request_offer_page.owner_declined_request'; /*владелец отклонил заявку*/
  ///нужно дождаться ответа от владельца дома
  String get needToWaitForAnswer =>
      'pages.request_offer_page.need_to_wait_for_answer'; /*нужно дождаться ответа от владельца дома*/
  ///Необходимо предложить заявку
  String get needToMakeAnOffer =>
      'pages.request_offer_page.need_to_make_an_offer'; /*Необходимо предложить заявку*/
  ///Редактировать
  String get edit => 'pages.request_offer_page.edit'; /*Редактировать*/
  ///Хотите изменить даты у себя в заявке?
  String get wantToChangeDates =>
      'pages.request_offer_page.want_to_change_dates'; /*Хотите изменить даты у себя в заявке?*/
  ///ДА
  String get yes => 'pages.request_offer_page.yes'; /*ДА*/
  RequestOfferHelper get requestOfferHelper => RequestOfferHelper();
  Share get share => Share();
  RequestOfferPageNotification get requestOfferPageNotification =>
      RequestOfferPageNotification();
}

class RequestOfferHelper {
  ///Немного о месте
  String get aboutThePlace =>
      'pages.request_offer_page.request_offer_helper.about_the_place'; /*Немного о месте*/
  ///Город
  String get city =>
      'pages.request_offer_page.request_offer_helper.city'; /*Город*/
  ///Ближайшие станции метро
  String get nearbyStations =>
      'pages.request_offer_page.request_offer_helper.nearby_stations'; /*Ближайшие станции метро*/
  ///мин пешком до метро
  String get timeToMetro =>
      'pages.request_offer_page.request_offer_helper.time_to_metro'; /*мин пешком до метро*/
  ///Адрес не указан
  String get noAddress =>
      'pages.request_offer_page.request_offer_helper.no_address'; /*Адрес не указан*/
  ///Адрес
  String get address =>
      'pages.request_offer_page.request_offer_helper.address'; /*Адрес*/
  ///Комнат
  String get rooms =>
      'pages.request_offer_page.request_offer_helper.rooms'; /*Комнат*/
  ///Больше 4х комнат
  String get moreThanXRooms =>
      'pages.request_offer_page.request_offer_helper.more_than_x_rooms'; /*Больше 4х комнат*/
}

class Share {
  ///Экспорт нескольких фотографий и текста
  String get title =>
      'pages.request_offer_page.share.title'; /*Экспорт нескольких фотографий и текста*/
  ///К сожалению, если Ваш клиент находится в Whatsapp, мы не можем ему сразу отправить несколько фотографий, а также текст. Поэтому, если это так воспользуйтесь раздельным поиском ниже. В противном случае, можете использовать полный экспорт.
  String get whatsappIssue =>
      'pages.request_offer_page.share.Whatsapp_issue'; /*К сожалению, если Ваш клиент находится в Whatsapp, мы не можем ему сразу отправить несколько фотографий, а также текст. Поэтому, если это так воспользуйтесь раздельным поиском ниже. В противном случае, можете использовать полный экспорт.*/
  ///Экспорт всего
  String get fullExport =>
      'pages.request_offer_page.share.full_export'; /*Экспорт всего*/
  ///Экспорт только фотографий
  String get onlyPhotos =>
      'pages.request_offer_page.share.only_photos'; /*Экспорт только фотографий*/
  ///Экспорт только текста
  String get onlyText =>
      'pages.request_offer_page.share.only_text'; /*Экспорт только текста*/
}

class RequestOfferPageNotification {
  ///От
  String get priceFrom =>
      'pages.request_offer_page.request_offer_page_notification.price_from'; /*От*/
  ///до
  String get priceTo =>
      'pages.request_offer_page.request_offer_page_notification.price_to'; /*до*/
  ///руб/сут
  String get priceMeasure =>
      'pages.request_offer_page.request_offer_page_notification.price_measure'; /*руб/сут*/
  ///Станции метро
  String get metroStations =>
      'pages.request_offer_page.request_offer_page_notification.metro_stations'; /*Станции метро*/
  ///Адрес
  String get address =>
      'pages.request_offer_page.request_offer_page_notification.address'; /*Адрес*/
  ///Цена
  String get price =>
      'pages.request_offer_page.request_offer_page_notification.price'; /*Цена*/
  ///Рейтинг
  String get rating =>
      'pages.request_offer_page.request_offer_page_notification.rating'; /*Рейтинг*/
  ///Запросить больше фото
  String get askForMorePhotos =>
      'pages.request_offer_page.request_offer_page_notification.ask_for_more_photos'; /*Запросить больше фото*/
  ///Попросите владельца добавить больше фото
  String get askOwnerForMorePhotos =>
      'pages.request_offer_page.request_offer_page_notification.ask_owner_for_more_photos'; /*Попросите владельца добавить больше фото*/
  ///Нет ни оценок, не отзывов
  String get noReviews =>
      'pages.request_offer_page.request_offer_page_notification.no_reviews'; /*Нет ни оценок, не отзывов*/
  ///владелец отклонил заявку
  String get ownerDeclinedMessage =>
      'pages.request_offer_page.request_offer_page_notification.owner_declined_message'; /*владелец отклонил заявку*/
  ///нужно дождаться ответа от владельца дома
  String get waitForAnswerMessage =>
      'pages.request_offer_page.request_offer_page_notification.wait_for_answer_message'; /*нужно дождаться ответа от владельца дома*/
  ///К сожалению в выбраннные Вами даты невозможно запросить бронь, квартира забронирована в следующеи даты.
  String get datesIntersectionIssue =>
      'pages.request_offer_page.request_offer_page_notification.dates_intersection_issue'; /*К сожалению в выбраннные Вами даты невозможно запросить бронь, квартира забронирована в следующеи даты.*/
  ///Хотите изменить даты у себя в заявке?
  String get askingForChangeDatesInRequest =>
      'pages.request_offer_page.request_offer_page_notification.asking_for_change_dates_in_request'; /*Хотите изменить даты у себя в заявке?*/
  ///ДА
  String get yes =>
      'pages.request_offer_page.request_offer_page_notification.yes'; /*ДА*/
}

class MessageInstancePage {
  ///Подробно о топике
  String get moreTopicDetails =>
      'pages.message_instance_page.more_topic_details'; /*Подробно о топике*/
  ///Ваш статус
  String get yourStatus =>
      'pages.message_instance_page.your_status'; /*Ваш статус*/
  ///Статус отправителя заявки
  String get requestorStatus =>
      'pages.message_instance_page.requestor_status'; /*Статус отправителя заявки*/
  ///Статус владельца
  String get ownerStatus =>
      'pages.message_instance_page.owner_status'; /*Статус владельца*/
  ///Квартира - Текущее состояние
  String get flatCurrentState =>
      'pages.message_instance_page.flat_current_state'; /*Квартира - Текущее состояние*/
  ///Заявка - Текущее состояние
  String get requestCurrentState =>
      'pages.message_instance_page.request_current_state'; /*Заявка - Текущее состояние*/
  ///Возможные действия
  String get possibleActions =>
      'pages.message_instance_page.possible_actions'; /*Возможные действия*/
}

class Metro {
  ChooseMetro get chooseMetro => ChooseMetro();
}

class ChooseMetro {
  ///По алфавиту
  String get byAlphabet =>
      'pages.metro.choose_metro.by_alphabet'; /*По алфавиту*/
  ///По линиям
  String get byLines => 'pages.metro.choose_metro.by_lines'; /*По линиям*/
  ///По округам
  String get byDistricts =>
      'pages.metro.choose_metro.by_districts'; /*По округам*/
  ///Поиск по названию
  String get searchByName =>
      'pages.metro.choose_metro.search_by_name'; /*Поиск по названию*/
  ///ДОБАВИТЬ СТАНЦИИ
  String get addStations =>
      'pages.metro.choose_metro.add_stations'; /*ДОБАВИТЬ СТАНЦИИ*/
  ///Выберите метро
  String get chooseStations =>
      'pages.metro.choose_metro.choose_stations'; /*Выберите метро*/
  ///Метро
  String get metro => 'pages.metro.choose_metro.metro'; /*Метро*/
}

class NotificationsPage {
  ///Оповещения
  String get notifications =>
      'pages.notifications_page.notifications'; /*Оповещения*/
  ///Очистить оповещения
  String get clearNotifications =>
      'pages.notifications_page.clear_notifications'; /*Очистить оповещения*/
  ///Вы точно хотите очистить ВСЕ оповещения ?
  String get clearAlertWarning =>
      'pages.notifications_page.clear_alert_warning'; /*Вы точно хотите очистить ВСЕ оповещения ?*/
  ///Позже
  String get later => 'pages.notifications_page.later'; /*Позже*/
  ///Да, хочу
  String get agreement => 'pages.notifications_page.agreement'; /*Да, хочу*/
  ///Здесь будут показаны ваши оповещения.
  String get emptyNotifications =>
      'pages.notifications_page.empty_notifications'; /*Здесь будут показаны ваши оповещения.*/
  ///Удалить
  String get delete => 'pages.notifications_page.delete'; /*Удалить*/
}

class Extra {
  GetMetroName get getMetroName => GetMetroName();
  UserTile get userTile => UserTile();
}

class GetMetroName {
  ///По алфавиту
  String get byAlphabet =>
      'pages.extra.get_metro_name.by_alphabet'; /*По алфавиту*/
  ///По линиям
  String get byLines => 'pages.extra.get_metro_name.by_lines'; /*По линиям*/
  ///По округам
  String get byDistricts =>
      'pages.extra.get_metro_name.by_districts'; /*По округам*/
}

class UserTile {
  ///Отзывы отсуствуют
  String get noReviews =>
      'pages.extra.user_tile.no_reviews'; /*Отзывы отсуствуют*/
  ///нет оценок
  String get noMarks => 'pages.extra.user_tile.no_marks'; /*нет оценок*/
}
