import 'package:flutter/foundation.dart';

const assetsPrefix = kIsWeb ? '' : 'assets/';

class Fonts {
  String get partnerumTtf => '${assetsPrefix}fonts/Partnerum.ttf';
}

class Images {
  String get icLauncherPng => '${assetsPrefix}images/ic_launcher.png';
  String get defaultPng => '${assetsPrefix}images/default.png';
  String get zeroOrdersSvg => '${assetsPrefix}images/zero_orders.svg';
  String get logoPng => '${assetsPrefix}images/logo.png';
  Undraw get undraw => Undraw();
  String get phonePng => '${assetsPrefix}images/phone.png';
  String get zeroOffersSvg => '${assetsPrefix}images/zero_offers.svg';
  String get phoneNumberSvg => '${assetsPrefix}images/phone_number.svg';
  String get logoSvg => '${assetsPrefix}images/logo.svg';
}

class Undraw {
  String get confirmedSvg => '${assetsPrefix}images/undraw/confirmed.svg';
  String get postOnlineSvg => '${assetsPrefix}images/undraw/post_online.svg';
  String get messagingSvg => '${assetsPrefix}images/undraw/messaging.svg';
  String get updateSvg => '${assetsPrefix}images/undraw/update.svg';
  String get makeItRainSvg => '${assetsPrefix}images/undraw/makeItRain.svg';
  String get notifySvg => '${assetsPrefix}images/undraw/notify.svg';
  String get apartmentRentSvg =>
      '${assetsPrefix}images/undraw/apartment_rent.svg';
}

class Langs {
  String get citiesJson => '${assetsPrefix}langs/cities.json';
  String get ruRUJson => '${assetsPrefix}langs/ru_RU.json';
}
