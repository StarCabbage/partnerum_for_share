import '_generated_files_extension.dart';
/*Внимание! В данном файле нельзя производить изменения вручную. Т.к. генератор просто удалит их.
  Используйте плагин flutter_serve для внесения изменений: flutter pub run flutter_serve*/

_Files files = _Files();

class _Files {
  Fonts get fonts => Fonts();
  Images get images => Images();
  Langs get langs => Langs();
}
