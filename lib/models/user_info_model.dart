import 'package:partnerum/widgets/user_title/user_tile.dart';

class UserModel {
  String avatar = '';
  String rawUsername = '';
  int lastOnline;
  String document = 'empty';
  bool isConfirmed = false;
  bool isAdmin = false;
  String role = 'Пользователь';
  String uid;
  Rating rating;

  get username => rawUsername.trim();

  UserModel({this.avatar, this.rawUsername, this.lastOnline, this.document});

/*
  UserModel.fromJson(dynamic json)
      : this._fromJson(json == null ? {} : Map.from(json));
*/
  bool isUsernameExist() {
    return rawUsername != 'Имя не введено';
  }

  String isConfirmedHumanReadable() {
    if (isConfirmed) {
      return 'Верефицирован';
    } else {
      return 'Неверефицирован';
    }
  }

/*
  UserModel._fromJson(Map<String, dynamic> json) {
    avatar = json['avatar'] ?? '';
    rawUsername = json['username'] ?? 'Имя не введено';
    lastOnline = json['last_online'] ?? -1;
    document = json['document'] ?? '';
    isConfirmed = json['confirmed'] ?? false;
    isAdmin = json["admin"] ?? false;
    rating = Rating.fromJson(json['rating'], json['reviews']);
  }
*/
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['avatar'] = avatar;
    data['username'] = rawUsername;
    data['last_online'] = lastOnline;
    data['document'] = document;
    data['admin'] = isAdmin;
    return data;
  }

  @override
  String toString() {
    return toJson().toString();
  }
}
