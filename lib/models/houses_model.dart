//{
//"timestamp": 12345,
//"info": {
//"city": "SPB",
//"stations": [
//"metro"
//],
//"extra_information": "",
//"price_range": [
//""
//],
//"address": "",
//"rooms": 0,
//"photo_urls": [
//""
//]
//}
//}

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:intl/intl.dart';
import 'package:partnerum/models/requests_model.dart';
import 'package:partnerum/models/user_info_model.dart';
import 'package:partnerum/utils/brain_module.dart';

extension CheckN on Map {
  dynamic getMap(String key) {
    if (!containsKey(key)) {
      return {};
    } else {
      return this[key];
    }
  }
}

class Status {
  static const String send_request = 'send_request';
  static const String deny_request = 'deny_request';

  static const String confirm_request = 'confirm_request';
  static const String refuse_request = 'refuse_request';

  static const String book = 'book';
  String request, house;
  String requestUid, houseUid;
  bool isMessagesEmpty = true;

  String get currentRequestStatus {
    if (house == confirm_request && request == send_request) {
      return 'Заявка';
    }
    return '';
  }

  bool get canShare => house != refuse_request && house != null;

  bool get canChat =>
      house != refuse_request &&
          house != null &&
          request != null &&
          request != deny_request ||
      (!isMessagesEmpty && house != refuse_request);

  bool get wip =>
      house != refuse_request && request != null && request != deny_request;

  bool get offerRequest => request == null || request == deny_request;

  bool get denyRequest => request != null && request != deny_request;

  bool get acceptOffer => house == null || house == refuse_request;

  bool get bookOffer => house == confirm_request && request == send_request;

  bool get refuseOffer => house == null || house != refuse_request;

  bool get empty =>
      house == null ||
      request == null ||
      requestUid == null ||
      houseUid == null;

  static Future<Status> loadStatus(String messageInstanceId,
      [Map _header]) async {}

  Status(this.request, this.house, this.requestUid, this.houseUid,
      {this.isMessagesEmpty = true});
}

//makeReview(String messageInstanceId, String review, double rating) async {
//     await rootReference
//         .child(DatabasePaths.message_data)
//         .child(messageInstanceId)
//         .child("header")
//         .child("review")
//         .child(user.uid)
//         .set({"rating": rating, "review": review});
//   }
class Review {
  Status status;

  ///[house] - Обзор арендодателя
  ///[request] - Обзор агента
  Review house, request;
  String reviewText;
  double rating;
  int timestamp;
  String reviewAnswer;
  int reviewAnswerTimestamp;
  String userId;
  bool isMyReview = false;

  Widget get humanReadableRating {
    if (rating != null) {
      return RichText(
          textAlign: TextAlign.start,
          text: TextSpan(
              text: '$rating',
              style:
                  TextStyle(color: Colors.orange, fontWeight: FontWeight.w400),
              children: [
                WidgetSpan(
                    child: RatingBar.builder(
                  initialRating: rating,
                  minRating: 0,
                  direction: Axis.horizontal,
                  allowHalfRating: true,
                  itemCount: 5,
                  ignoreGestures: true,
                  itemSize: 16,
                  itemBuilder: (context, _) => Icon(
                    Icons.star,
                    color: Colors.amber,
                  ),
                  onRatingUpdate: (rating) {},
                  glow: false,
                )),
                TextSpan(
                    text:
                        '${DateFormat.yMMMd('ru').format(DateTime.fromMillisecondsSinceEpoch(timestamp))}',
                    style: TextStyle(color: Colors.grey.shade500))
              ]));
    }
  }

  static Review fromJson(Map _reviewObj, [uid]) {
    double _rating = (_reviewObj['rating'] ?? -1.0) * 1.0;
    String _review = _reviewObj['review'];
    int _timestamp = _reviewObj['timestamp'];
    String _reviewAnswer = _reviewObj.getMap('answer')['message'];
    int _reviewAnswerTimestamp = _reviewObj.getMap('answer')['timestamp'];
    return Review.only(_rating, _review, _timestamp, _reviewAnswer,
        _reviewAnswerTimestamp, uid);
  }

  static Future<Review> loadReview(String messageInstanceId) async {
    var _status = await Status.loadStatus(messageInstanceId);
    var _house = await loadReviewOnly(messageInstanceId, _status.houseUid);
    var _request =
        await loadReviewOnly(messageInstanceId, _status.requestUid);

    return Review(_status, _request, _house);
  }

  Review get myReview {
    Review review;
    if (status.requestUid == mindClass.user.uid) {
      review = request;
    } else {
      review = house;
    }

    review.isMyReview = true;
    return review;
  }

  Review get reviewToMe {
    /*
    if (status.requestUid != mindClass.user.uid)

     */
    if (false) {
      return request;
    } else {
      return house;
    }
  }

  static Future<Review> loadReviewOnly(
      String messageInstanceId, String uid) async {
/*
    Map _reviewObj = (await mindClass.rootReference
                .child(DatabasePaths.message_data)
                .child(messageInstanceId)
                .child("header")
                .child("review")
                .child(uid)
                .once())
            .value ??
        {};
    double _rating = (_reviewObj['rating'] ?? -1.0) * 1.0;
    String _review = _reviewObj['review'];
    int _timestamp = _reviewObj['timestamp'];
    String _reviewAnswer = _reviewObj.getMap('answer')['message'];
    int _reviewAnswerTimestamp = _reviewObj.getMap('answer')['timestamp'];
    return Review.only(_rating, _review, _timestamp, _reviewAnswer,
        _reviewAnswerTimestamp, uid);
*/
  }

  Review(this.status, this.request, this.house);

  Review.only(this.rating, this.reviewText, this.timestamp, this.reviewAnswer,
      this.reviewAnswerTimestamp, this.userId);
}

class House {
  int timestamp;
  HouseInfo info;
  bool archive;
  String houseId = '';
  String uid;
  UserModel userModel;

  bool offered = false;

  String offerType;

  String messageInstanceId = '';

  Status status;

  get complexId => userModel.uid + houseId + messageInstanceId;

  House({this.timestamp, this.info, this.archive});

  House.fromJson(dynamic json) : this._fromJson(Map.from(json));

  House._fromJson(Map<String, dynamic> json) {
    timestamp = json['timestamp'];
    info = json['info'] != null
        ? new HouseInfo.fromJson(Map.from(json['info']))
        : null;
    archive = json['archive'] ?? false;
  }

  Map<String, dynamic> toJson() {
    final data = new Map<String, dynamic>();
    data['timestamp'] = timestamp;
    if (info != null) {
      data['info'] = info.toJson();
    }
    return data;
  }
}

extension operations on DateTime {}

class HouseInfo {
  String city;
  List<String> stations;
  String extraInformation;
  List<String> priceRange;
  String address = 'Адрес не указан';

  isAddressEmpty() {
    return address == 'Адрес не указан';
  }

  String rooms;
  List<String> photoUrls;
  String minutesToMetro;
  String latLon;

  List<DateTimeRange> bookedDates;

  possibleToBook(Request request) {
    var requestDateTimeRange = request.info.dateTimeRange;
    if (bookedDates == null || bookedDates.isEmpty) return true;
    bookedDates.forEach((bookedDate) {
      var firstDateBeforeBooking =
          requestDateTimeRange.start.isBefore(bookedDate.start) &&
              requestDateTimeRange.start.isBefore(bookedDate.end);
      var secondDateBeforeBooking =
          requestDateTimeRange.end.isBefore(bookedDate.start) &&
              requestDateTimeRange.end.isBefore(bookedDate.end);

      if (!firstDateBeforeBooking || !secondDateBeforeBooking) return false;
    });
    return true;
  }

  int rating;

  ///TODO [rating]

  List reviews;

  HouseInfo(
      {this.city,
      this.stations,
      this.extraInformation,
      this.priceRange,
      this.address,
      this.rooms,
      this.photoUrls,
      this.minutesToMetro,
      this.latLon});

  HouseInfo.fromJson(Map<String, dynamic> json) {
    city = json['city'];
    stations = json['stations'].cast<String>();
    extraInformation = json['extra_information'];
    priceRange = json['price_range'].cast<String>();
    address = json['address'] ?? 'Адрес не указан';
    if (address.trim().isEmpty) address = 'Адрес не указан';
    rooms = json['rooms'];
    photoUrls = json['photo_urls'].cast<String>();
    minutesToMetro = (json['minutes_to_metro'] ?? 'Неизвестно').toString();
    latLon = json['lat;lon'];

    if (json['booked_dates'] != null) {
      bookedDates = [];
      for (var dateRange in (json['booked_dates'] as Map).entries) {
        List<String> _bookedDate = dateRange.key.split('-');
        bookedDates.add(DateTimeRange(
            start:
                DateTime.fromMillisecondsSinceEpoch(int.parse(_bookedDate[0])),
            end: DateTime.fromMillisecondsSinceEpoch(
                int.parse(_bookedDate[1]))));
      }
    }
  }

  Map<String, dynamic> toJson() {
    final data = new Map<String, dynamic>();
    data['city'] = city;
    data['stations'] = stations;
    data['extra_information'] = extraInformation;
    data['price_range'] = priceRange;
    data['address'] = address;
    data['rooms'] = rooms;
    data['photo_urls'] = photoUrls;
    data['minutes_to_metro'] = minutesToMetro;
    data['lat;lon'] = latLon;
    return data;
  }

  String toExportableJson() {
    var export = '';
    if (extraInformation.isNotEmpty)
      export += 'Немного о месте: ${extraInformation}\n';
    export += 'Город: ${city}\n';
    if (stations != null && stations.length > 0) {
      export += 'Ближайшие станции метро: ${stations.join(', ')}\n';
      export += '${minutesToMetro} мин пешком до метро\n';
    }
    if (address != null && address.isNotEmpty && address != 'Адрес не указан')
      export += 'Адрес: ${address}\n';

    if (rooms != '5') {
      export += 'Комнат: ${rooms}\n';
    } else {
      export += 'Больше 4х комнат\n';
    }
    return export;
  }
}
