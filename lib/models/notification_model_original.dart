import 'dart:convert';

import 'package:partnerum/models/header_model.dart' show Header;

class NotificationMessage {
  NotificationM notification;
  Data data;

  isDataMessage() {
    return notification.title == null;
  }

  NotificationMessage({this.notification, this.data});

  NotificationMessage.fromJson(Map<String, dynamic> json)
      : this._fromJson(Map.from(json));

  NotificationMessage._fromJson(Map<String, dynamic> json) {
    notification = json['notification'] != null
        ? new NotificationM.fromJson(Map.from(json['notification']))
        : null;
    data =
        json['data'] != null ? new Data.fromJson(Map.from(json['data'])) : null;
    if (data != null && notification != null) if (notification.title == null) {
      notification.title = data.title;
      notification.body = data.body;
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (notification != null) {
      data['notification'] = notification.toJson();
    }
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }

  @override
  String toString() {
    return toJson().toString();
  }
}

class NotificationM {
  String title = "";
  String body = "";

  NotificationM({this.title, this.body});

  NotificationM.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    body = json['body'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['title'] = title;
    data['body'] = body;
    return data;
  }
}

class Data {
  String link;
  String key;
  String title, body;

  Data({this.link});

  Data.fromJson(Map<String, dynamic> json) {
    link = json['link'];
    key = json['key'];
    title = json['title'];
    body = json['body'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['link'] = link;
    return data;
  }
}

enum NotificationType { message, not_message }

class NotificationInternal {
  final body, title, img, messageInstanceId;

  final Header header;
  final NotificationType type;
  String key;
  int timestamp = -1;
  bool read = false;

  getHumanReadableType() {
    if (title != null) return title;
    return 'Ошибка';
  }

  NotificationInternal(this.type, this.body, this.title, this.key, this.img,
      this.header, this.messageInstanceId,
      {this.read, this.timestamp});

  static Future<NotificationInternal> fromJson(dynamic jsonData) async {
    print('JsonData');
    print(jsonData);
    var notification = Map.from(jsonData);
    if (notification.containsKey('data')) notification = notification['data'];
    String body = notification['body'] ?? 'Подробная информация отсуствует';
    String title = notification['title'];

    String img = notification['image'];
    var type = notification['type'] == 'message'
        ? NotificationType.message
        : NotificationType.not_message;
    bool read = notification['r'] ?? false;
    int timestamp = notification['time'] ?? -1;
    String messageInstanceId = notification['messageInstanceId'];
    var header = notification.containsKey('header')
        ? await Header.fromJson(
            messageInstanceId, Map.from(jsonDecode(notification['header'])))
        : null;
    return NotificationInternal(
        type, body, title, notification['key'], img, header, messageInstanceId,
        timestamp: timestamp, read: read);
  }
}
