import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Request {
  List<String> houses;
  int timestamp;
  Info info;
  String status;
  String requestId;
  bool archive;

  String uid;

  Request({this.houses, @required this.timestamp, this.info, this.archive});

  Request.fromJson(dynamic json) : this._fromJson(Map.from(json));

  Request._fromJson(Map<String, dynamic> json) {
    houses = (json['houses'] ?? []).cast<String>();
    timestamp = json['timestamp'];
    status = json['status'];
    info =
        json['info'] != null ? new Info.fromJson(Map.from(json['info'])) : null;
    archive = json['archive'] ?? false;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['houses'] = houses;
    data['status'] = status;
    data['timestamp'] = timestamp;
    if (info != null) {
      data['info'] = info.toJson();
    }
    data['acrhive'] = archive;
    return data;
  }

  @override
  String toString() {
    return toJson().toString();
  }
}

class Info {
  String city;
  List<String> rooms;
  String guests;
  List<String> stations;
  List<String> datesRange;
  DateTimeRange dateTimeRange;
  List<String> priceRange;
  String extraInformation;
  String commission;
  String latLon;
  String minutesToMetro;

  Info(
      {this.city,
      this.rooms,
      this.guests,
      this.stations,
      this.datesRange,
      this.priceRange,
      this.extraInformation,
      this.commission,
      this.latLon,
      this.minutesToMetro});

  Info.fromJson(Map<String, dynamic> json) {
    city = json['city'];
    rooms = (json['rooms'] ?? []).cast<String>();
    guests = json['guests'];
    stations = (json['stations'] ?? []).cast<String>();
    datesRange = (json['dates_range'] ?? []).cast<String>();
    if (datesRange != null && datesRange.isNotEmpty) {
      dateTimeRange = DateTimeRange(
          start: DateFormat('d.M.yyyy').parse(datesRange[0]),
          end: DateFormat('d.M.yyyy').parse(datesRange[1]));
    }
    priceRange = (json['price_range'] ?? []).cast<String>();
    extraInformation = json['extra_information'];
    commission = json['commission'];
    latLon = json['lat;lon'];
    minutesToMetro = json['minutes_to_metro'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['city'] = city;
    data['rooms'] = rooms;
    data['guests'] = guests;
    data['stations'] = stations;
    data['dates_range'] = datesRange;
    data['price_range'] = priceRange;
    data['extra_information'] = extraInformation;
    data['commission'] = commission;
    data['lat;lon'] = latLon;
    data['minutes_to_metro'] = minutesToMetro;
    return data;
  }
}
