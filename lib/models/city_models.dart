import 'dart:ui';

class JsonCity {
  String id;
  String name;
  String url;
  List<Lines> lines;

  JsonCity({this.id, this.name, this.url, this.lines});

  JsonCity.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    url = json['url'];
    if (json['lines'] != null) {
      lines = <Lines>[];
      json['lines'].forEach((v) {
        lines.add(Lines.fromJson(v));
      });
      lines.sort((l1, l2) {
        return l1.name.toLowerCase().compareTo(l2.name.toLowerCase());
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['url'] = url;
    if (lines != null) {
      data['lines'] = lines.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Lines {
  String id;
  String hexColor;
  String name;
  List<Stations> stations=[];

  Lines(this.name);
  Lines.f({this.id, this.hexColor, this.name, this.stations});

  Lines.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    hexColor = json['hex_color'];
    name = json['name'];
    if (json['stations'] != null) {
      stations = <Stations>[];
      json['stations'].forEach((v) {
        stations.add(Stations.fromJson(v));
      });
      stations.sort((s1, s2) {
        return s1.name.toLowerCase().compareTo(s2.name.toLowerCase());
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['hex_color'] = hexColor;
    data['name'] = name;
    if (stations != null) {
      data['stations'] = stations.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

List<Rounds> rounds = [
  Rounds('ЗАПАДНЫЙ АДМИНИСТРАТИВНЫЙ ОКРУГ (ЗАО)',
      'Киевская, Парк Победы, Проспект Вернадского, Юго-западная, Крылатское, Молодежная, Кунцевская, Пионерская, Филевский Парк, Багратионовская, Фили, Кутузовская, Студенческая, Киевская ,Киевская'),
  Rounds('ВОСТОЧНЫЙ АДМИНИСТРАТИВНЫЙ ОКРУГ (ВАО)',
      'Новогиреево, Перово, Шоссе Энтузиастов, Щелковская, Первомайская, Измайловская, Измайловский Парк, Семеновская, Электрозаводская , Улица Подбельского, Черкизовская, Преображенская Площадь, Сокольники ,Выхино'),
  Rounds('ЦЕНТРАЛЬНЫЙ АДМИНИСТРАТИВНЫЙ ОКРУГ(ЦАО)',
      'Рижская, Проспект Мира, Сухаревская, Тургеневская, Китай-город, Третьяковская, Октябрьская ,Белорусская, Маяковская, Тверская, Театральная, Новокузнецкая, Павелецкая ,Площадь Ильича, Марксистская, Третьяковская ,Бауманская, Курская, Площадь Революции, Арбатская, Смоленская ,Красносельская, Комсомольская, Красные Ворота, Чистые Пруды, Лубянка, Охотный Ряд, Библиотека им. Ленина, Парк Культуры, Фрунзенская, Спортивная, Воробьевы Горы, Кропоткинская ,Смоленская, Арбатская, Александровский Сад ,Проспект Мира, Комсомольская, Курская, Таганская, Павелецкая, Добрынинская, Октябрьская, Парк Культуры, Краснопресненская, Белорусская, Новослободская ,Улица 1905 года, Баррикадная, Пушкинская, Кузнецкий Мост, Китай-город, Пролетарская, Таганская ,Менделеевская, Цветной Бульвар, Чеховская, Боровицкая, Полянка, Серпуховская'),
  Rounds('СЕВЕРНЫЙ АДМИНИСТРАТИВНЫЙ ОКРУГ (САО)',
      'Речной Вокзал, Водный Стадион, Войковская, Сокол, Аэропорт, Динамо ,Полежаевская, Беговая ,Петровско-Разумовская, Тимирязевская'),
  Rounds('СЕВЕРО-ВОСТОЧНЫЙ АДМИНИСТРАТИВЫЙ ОКРУГ (СВАО)',
      'ВДНХ, Медведково, Бабушкинская, Свиблово, Ботанический Сад, Алексеевская , Алтуфьево, Бибирево, Отрадное, Владыкино, Дмитровская, Савеловская'),
  Rounds('СЕВЕРО-ЗАПАДНЫЙ АДМИНИСТРАТИВНЫЙ ОКРУГ (СЗА0)',
      'Планерная, Сходненская, Тушинская, Щукинская, Октябрьское Поле'),
  Rounds('ЮГО-ВОСТОЧНЫЙ АДМИНИСТРАТИВНЫЙ ОКРУГ (ЮАО)',
      'Авиамоторная ,Волгоградский Проспект, Текстильщики, Кузьминки, Рязанский Проспект , Дубровка, Кожуховская, Печатники, Волжская, Люблино, Братиславская, Марьино'),
  Rounds('ЮЖНЫЙ-АДМИНИСТРАТИВНЫЙ ОКРУГ (ЮАО)',
      ' Шаболовская ,Автозаводская, Коломенская, Каширская, Кантемировская, Царицыно, Орехово, Домодедовская, Красногвардейская, Варшавская ,Тульская, Нагатинская, Нагорная, Южная, Пражская, ул. Академика Янгеля, Аннино'),
  Rounds('ЮГО-ЗАПАДНЫЙ АДМИНИСТРАТИВНЫЙ ОКРУГ (ЮЗАО)',
      'Ленинский Проспект, Академическая, Профсоюзная, Новые Черемушки, Калужская, Беляево, Коньково, Теплый Стан, Ясенево, Битцевский Парк, Каховская, Университет, Нахимовский Проспект, Севастопольская, Чертановская, Бульвар Дмитрия Донского')
];

class Rounds {
  final String name;
  String stations;
  List<Stations> lStations = [];

  Rounds(this.name, String stations) {
    this.stations = stations.toLowerCase();
    var counter = 0;
    lStations = stations.split(',').map<Stations>((v) {
      counter += 1;
      return Stations(id: (counter - 1).toString(), name: v.trim());
    }).toList();
  }
}

class Stations {
  String id;
  String name;
  double lat;
  double lng;
  int order;
  Color color;
  String number;

  Stations({this.id, this.name, this.lat, this.lng, this.order});

  Stations.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    lat = json['lat'];
    lng = json['lng'];
    order = json['order'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['lat'] = lat;
    data['lng'] = lng;
    data['order'] = order;
    return data;
  }
}
