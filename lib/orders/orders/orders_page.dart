import 'dart:async';

import 'package:another_flushbar/flushbar.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:db_partnerum/db_partnerum.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:partnerum/head_module/gen_extensions/generated_str_extension.dart';
import 'package:partnerum/models/houses_requests_model.dart';
import 'package:partnerum/orders/order_detail/order_detail.dart';
import 'package:partnerum/pages/chat/messages/messages_page.dart';
import 'package:partnerum/pages/house/house_details/house_details_page.dart';
import 'package:partnerum/pages/house/house_offers/house_offers_helper.dart';
import 'package:partnerum/pages/house/house_offers/house_offers_page.dart';
import 'package:partnerum/pages/request/request_create/request_create_page.dart';
import 'package:partnerum/pages/request/request_offer/request_offer_helper.dart';
import 'package:partnerum/pages/request/request_offer/request_offer_page.dart';
import 'package:partnerum/pages/request/suitable_apartments/suitable_apartments_page.dart';
import 'package:partnerum/utils/brain_module.dart';
import 'package:partnerum/utils/theme_constants.dart';
import 'package:partnerum/utils/utils.dart';
import 'package:partnerum/widgets/context_menu.dart';
import 'package:partnerum/widgets/partnerum_icons.dart';
import 'package:partnerum/widgets/user_title/user_tile.dart';
import '../../main.dart';
import 'orders_helper.dart';
import 'dart:math' as math;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:partnerum/models/status_model.dart';

class RequestCard extends StatelessWidget with OrdersHelper {
  final Request request;
  final bool hideActions;
  final bool editable;
  final bool isArchive;
  final bool justDemonstration;
  final cardForm;

  final Function onArchive;

  RequestCard(
      {Key key,
      @required this.request,
      this.hideActions = false,
      this.onArchive,
      this.isArchive,
      this.editable = true,
      this.justDemonstration = false,
      this.cardForm = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _buildOrderItem(context, request);
  }

  Widget _buildOrderItem(BuildContext context, Request request) {
    var key = '#4#${request.key}#${ModalRoute.of(context).settings.name}';
    if (!keys.containsKey(key)) keys[key] = GlobalKey();
    Widget body = Column(
      //crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        // LayoutBuilder(builder: (context, constraints) {
        //   // print(constraints);
        //   // return Container(color: Colors.red, width: 20, height: 40,);
        //
        //   return StatefulBuilder(builder: (context, setState) {
        //     return FutureBuilder<List<ConversationUsers>>(
        //       key: keys[key],
        //       initialData: [],
        //       future: acceptedOffersConversationUsers(request),
        //       builder: (BuildContext context, AsyncSnapshot asyncSnapshot) {
        //         // print('asyncSnapshot.data: ${asyncSnapshot.data}  ${request.key}');
        //         if (asyncSnapshot.hasData && asyncSnapshot.data.length > 0) {
        //           // print('len: ${asyncSnapshot.data.length}');
        //           return AnimatedSwitcher(
        //             transitionBuilder:
        //                 (Widget child, Animation<double> animation) {
        //               return SizeTransition(
        //                 axis: Axis.vertical,
        //                 sizeFactor: animation,
        //                 child: child,
        //               );
        //             },
        //             duration: kThemeAnimationDuration,
        //             child: SizedBox(
        //               width: constraints.maxWidth,
        //               child: ListTile(
        //                 contentPadding: cardForm ? null : EdgeInsets.zero,
        //                 title: ChoseList(
        //                   cardForm: cardForm,
        //                   request: request,
        //                   conversationUsers: asyncSnapshot.data,
        //                   acceptedOffers: acceptedOffers,
        //                 ),
        //               ),
        //             ),
        //           );
        //         }
        //         return Container();
        //       },
        //     );
        //   });
        // }),
        ListTile(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              // Container(color: Colors.red, width: 20, height: 40,)
              _buildDate(context, request),
            ],
          ),
          subtitle: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              _buildCity(context, request),

              _buildMetro(context, request),

              _buildPrice(context, request),

//                _buildGuests(document),
//                _buildRooms(document),
            ],
          ),
        ),
        if (!hideActions) _buildBtns(context, request),
      ],
    );
//     Widget body = Container(color: Colors.green, width: 20, height: 30,);
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          CupertinoPageRoute(
            builder: (context) => OrderDetailPage(
              // requestId: request.key,
              editable: editable,
              request: request,
            ),
          ),
        );
      },
      child: cardForm
          ? Card(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16))),
              child: body,
            )
          : body,
    );
  }

  static Map<String, GlobalKey> keys = {};

  Widget _buildBtns(BuildContext context, Request request) {
    var key = '#1#${request.key}#${ModalRoute.of(context).settings.name}';
    var key2 = '#2#${request.key}#${ModalRoute.of(context).settings.name}';
    var key3 = '#3#${request.key}#${ModalRoute.of(context).settings.name}';
    if (!keys.containsKey(key)) {
      keys[key] = GlobalKey();
      keys[key2] = GlobalKey();
      keys[key3] = GlobalKey();
    }

    return Container(
      width: double.maxFinite,
      child: Column(
        children: <Widget>[
          if (request?.offers?.values == null || request.offers.values.isEmpty)
            Builder(builder: (BuildContext buildContext) {
              if (justDemonstration || isArchive) return SizedBox(height: 16);
              return ListTile(
                title: Text(
                  Strings.pages.ordersPage.edit.l(context),
                  style: TextStyle(
                    color: Theme.of(context).primaryColor,
                  ),
                ),
                subtitle: Text(
                  Strings.pages.requestsPage.requestsCard.sorryText.l(context),
                ),
                onTap: () {
                  Navigator.push(
                    context,
                    CupertinoPageRoute(
                      builder: (context) => RequestCreate(
                        firstLoad: false,
                        requestId: request.key,
                        request: request,
                      ),
                    ),
                  );
                },
              );
            }),
          Container(
            width: double.maxFinite,
            child: Wrap(
              alignment: WrapAlignment.end,
              children: <Widget>[
                if (onArchive != null)
                  TextButton(
                    onPressed: () {
                      // removeFromArchive(request);
                      onArchive();
                    },
                    child: Text(
                      isArchive ? Strings.globalOnes.unarchive.l(context) : Strings.globalOnes.archive.l(context),
                      style: TextStyle(color: Colors.black, fontSize: 16),
                    ),
                  ),
                if (editable && !isArchive && request?.offers?.values != null && request.offers.values.isNotEmpty)
                  TextButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        CupertinoPageRoute(
                          builder: (context) => SearchingOfSuitableApartments(request: request),
                        ),
                      ).then((value) => mindClass.updateEvent('updateRequestScreen'));
                    },
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          Strings.pages.requestsPage.requestsCard.offerText.l(context),
                          style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 16),
                        ),
                        ClipOval(
                            child: Container(
                          width: 6,
                          height: 6,
                          color: Colors.red,
                        ))
                      ],
                    ),
                  )
                /*
                      Container(
                        color: Colors.lightBlueAccent,
                      )

                       */
              ],
            ),
          ),
        ],
      ),
    );

    // return FutureBuilder(
    //     key: keys[key2],
    //     /*
    //                 future: mindClass.getFilteredR(request.requestId),
    //
    //                  */
    //     future: requests(request),
    //     initialData: [],
    //     builder: (context, snapshot) {
    //       if (snapshot.connectionState == ConnectionState.done) {
    //         return
    //       }
    //       return Container(color: Colors.red, width: 100, height: 100,);
    //     });
  }

  Widget _buildPrice(BuildContext context, Request request) {
    return Row(
      children: <Widget>[
        Icon(
          Partnerum.ruble,
          color: Colors.grey,
          size: 14,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Text(
            '${Strings.pages.ordersPage.priceFrom.l(context)} ${request.requestInformation.priceRange.startPosition.value} ${Strings.pages.ordersPage.priceTo.l(context)} ${request.requestInformation.priceRange.endPosition.value} ${Strings.pages.ordersPage.priceMeasure.l(context)}',
            style: TextStyle(
              fontSize: 14,
              color: Colors.grey,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildMetro(BuildContext context, Request request) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Icon(
          Partnerum.metro,
          color: Colors.grey,
          size: 14,
        ),
        Flexible(
          child: Padding(
            padding: const EdgeInsets.only(left: 8),
            child: Container(
              child: Builder(builder: (context) {
                var stations = requestStations(request);
                // print('stations: ${stations.runtimeType} ${stations}');
                return Text(
                  stations.join(', '),
                  softWrap: true,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: 14,
                    color: Colors.grey,
                  ),
                );
              }),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildCity(BuildContext context, Request request) {
    return Row(
      children: <Widget>[
        Icon(
          Partnerum.city,
          color: Colors.grey,
          size: 14,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Text(
            '${request.requestInformation.city.value}',
            style: TextStyle(
              fontSize: 14,
              color: Colors.grey,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildDate(BuildContext context, Request request) {
    return Text(
      // '1213',
      requestDatesRange(request),
      style: TextStyle(
        fontSize: 16,
        color: Colors.black,
        fontWeight: FontWeight.bold,
      ),
    );
  }
}

class ChoseList extends StatefulWidget {
  const ChoseList({
    Key key,
    @required this.cardForm,
    @required this.conversationUsers,
    @required this.acceptedOffers,
    @required this.request,
  }) : super(key: key);

  final bool cardForm;
  final Request request;
  final List<ConversationUsers> conversationUsers;
  final List<Offer> acceptedOffers;

  @override
  _ChoseListState createState() => _ChoseListState();
}

class _ChoseListState extends State<ChoseList> with ChoseListHelper {
  int _selectedElement = -1;

  @override
  void initState() {
    // mindClass.eventSteam.outStream.listen((event) {
    //   if (event == 'removeRoundClick') {
    //     setState(() {
    //       _selectedElement = -1;
    //     });
    //   }
    // });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget decoratedImage(ImageProvider imageProvider) {
      return Container(
          width: 50,
          height: 50,
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(image: imageProvider, fit: BoxFit.cover, colorFilter: ColorFilter.mode(Colors.white, BlendMode.colorBurn))));
    }

    /*
    Stream acceptedHouses() {}
    var offersStream = Stream.fromIterable(widget.offers);
    // print(offersStream.runtimeType);
    offersStream.where((event) {
      print('AAAAAAAAAAA');
      var ownerStatus =
          (root.chatTopic.offer2Room[event.offerId.value].conversationUsers.usersStatuses.ownerUserStatus.stream).map((event) => event.value);
      print('${event.offerId.value}: ${ownerStatus}');
      return ((ownerStatus == 'request_accept') || ((ownerStatus == 'request_booked')));
    });
    // print (offersStream);


     */
    // Future<House> OfferHouse(Offer offer) {
    //   var house = db
    //       .root.userData[offer.userId.value].houses[offer.userId.value].stream
    //       .where((event) => false);
    //   // return house;
    // }

    print('Selected element: ${_selectedElement}');

    return SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        child: Row(
          children: [
            if (!widget.cardForm) SizedBox(width: 16),
            for (var i = 0, l = widget.conversationUsers.length; i < l; i++)
              /*
              if (widget.houses[i].status.wip)
               */
              if (true)
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Builder(builder: (context) {
                      var ownerUser = widget.conversationUsers[i].ownerUser.value;
                      var ownerId = ownerUser.split(';').first;
                      var houseId = ownerUser.split(';').last;

                      return FutureBuilder<HouseInformation>(
                        future: loadHouseInformation(ownerId, houseId),
                        builder: (BuildContext context, AsyncSnapshot<HouseInformation> houseInformationSnapshot) {
                          CachedNetworkImage img;
                          // var imagesList = houseInformationSnapshot
                          //     ?.data?.photos?.values
                          //     ?.toList();
                          var imagesList = photosList;
                          if (houseInformationSnapshot.hasData) {
                            img = CachedNetworkImage(
                                // imageUrl: imagesList?.first?.url,
                                // httpHeaders: imagesList?.first?.syncHttpHeaders,
                                imageUrl: imagesList?.first?.bigImg?.url,
                                httpHeaders: imagesList?.first?.bigImg?.syncHttpHeaders,
                                imageBuilder: (context, imageProvider) => decoratedImage(imageProvider),
                                placeholder: (context, url) =>
                                    Theme.of(context).platform == TargetPlatform.iOS ? CupertinoActivityIndicator() : CircularProgressIndicator(),
                                errorWidget: (context, url, error) => Container(
                                    width: 40,
                                    height: 40,
                                    decoration: BoxDecoration(border: Border.all(color: Theme.of(context).primaryColor), shape: BoxShape.circle),
                                    child: Center(child: Icon(Icons.info_outline, color: Theme.of(context).primaryColor))));
                          }

                          return AnimatedContainer(
                            decoration: BoxDecoration(
                                border: Border.all(width: 2, style: BorderStyle.solid, color: _selectedElement != i ? Colors.orange : goodGreen),
                                borderRadius: BorderRadius.circular(30)),
                            padding: EdgeInsets.all(1),
                            duration: kThemeAnimationDuration,
                            child: GestureDetector(
                              onTap: () async {
                                if (!widget.cardForm) {
                                  // if (widget.cardForm) {
                                  setState(() {
                                    if (_selectedElement == i) {
                                      _selectedElement = -1;
                                    } else {
                                      vibrate();
                                      // mindClass.updateEvent('scrollToElement${widget.houses[i].complexId}'
                                      print(
                                          'scrollToElement: ${widget.conversationUsers[i].ownerUser.value} ${(widget.conversationUsers[i].ownerUser.value).hashCode}');
                                      mindClass.updateEvent('scrollToElement${(widget.conversationUsers[i].ownerUser.value)}');
                                      /*
                                        Navigator.push(
                                          context,
                                          CupertinoPageRoute(
                                            builder: (context) => SearchingOfSuitableApartments(
                                              request: widget.request,
                                            ),
                                          ),
                                        ).then((value) => mindClass.updateEvent('updateRequestScreen'));

                                         */

                                      _selectedElement = i;
                                    }
                                  });
                                } else {
                                  /*
                                    await Navigator.push(
                                        context,
                                        CupertinoPageRoute(
                                            builder: (context) => Messages(
                                                  messageInstanceId: widget.conversationUsers[i].chatId.value,
                                                  isOwner: false,
                                                )));

                                     */
                                  /*
                                    FutureBuilder(
                                      future: ,
                                      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {

                                      },
                                    );
*/

                                  await getCardModal(context, ownerId, houseId, widget.request, widget.acceptedOffers[i]);
                                }
                              },
                              child: img ??
                                  CircleAvatar(
                                    /*
                                  backgroundColor: Colors.grey.shade100,

                                   */
                                    backgroundColor: Color((math.Random().nextDouble() * 0xFFFFFF).toInt()).withOpacity(1.0),

                                    // backgroundImage: CachedNetworkImageProvider(
                                    //     widget.houses[i].info.photoUrls.first),
                                  ),
                            ),
                          );
                        },
                      );
                    }),
                    /*
                    FutureBuilder(
                      future: OfferHouse(widget.offers[i]),
                      builder: (context, snapshot) {
                        return AnimatedContainer(
                          decoration: BoxDecoration(
                              border: Border.all(width: 2, style: BorderStyle.solid, color: _selectedElement == i ? Colors.orange : goodGreen),
                              borderRadius: BorderRadius.circular(30)),
                          padding: EdgeInsets.all(1),
                          duration: kThemeAnimationDuration,
                          child: GestureDetector(
                            onTap: () async {
                              if (!widget.cardForm) {
                                setState(() {
                                  if (_selectedElement == i) {
                                    _selectedElement = -1;
                                  } else {
                                    vibrate();
                                    /*
                                    mindClass.updateEvent(
                                        'scrollToElement${widget.houses[i].complexId}'
                                    );

                                     */
                                    _selectedElement = i;
                                  }
                                });
                              } else {
                                /*
                                await getCardModal(
                                    context, widget.houses[i], widget.request);

                                 */
                              }
                            },
                            child: CircleAvatar(
                              /*
                              backgroundColor: Colors.grey.shade100,

                               */
                              backgroundColor: Color((math.Random().nextDouble() * 0xFFFFFF).toInt()).withOpacity(1.0),
                              /*
                              backgroundImage: CachedNetworkImageProvider(
                                  widget.houses[i].info.photoUrls.first),

                               */
                            ),
                          ),
                        );
                      }
                    ),

                     */
                    spacer(4, 0),
                  ],
                ),
          ],
        ));
  }

  Future getCardModal(BuildContext buildContext, String ownerId, String houseId, Request request, Offer offer) {
    return showModalBottomSheet(
        // elevation: 1,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(borderRadius: borderRadius),
        clipBehavior: Clip.antiAlias,
        context: buildContext,
        builder: (context) {
          return FutureBuilder(
              future: loadOwnerInformation(ownerId),
              builder: (context, ownerInformationSnapshot) {
                if (ownerInformationSnapshot.hasData) {
                  return FutureBuilder(
                      future: loadHouseInformation(ownerId, houseId),
                      builder: (context, houseInformationSnapshot) {
                        print('Check Check orders');
                        return DraggableScrollableSheet(
                            expand: false,
                            maxChildSize: .7,
                            //MediaQuery.of(context).size.height*0.5,
                            builder: (BuildContext context, ScrollController scrollController) {
                              return SingleChildScrollView(
                                controller: scrollController,
                                /*
                        child: RequestOfferCard(
                            house: house,
                            newStyle: true,
                            isReady: true,
                            close: () => Navigator.pop(context)),

                         */
                                child: (houseInformationSnapshot.hasData)
                                    ? RequestOffer(
                                        offer: offer,
                                        houseInformation: houseInformationSnapshot.data,
                                        userInformation: ownerInformationSnapshot.data,
                                        requestInformation: widget.request.requestInformation,
                                        requestId: widget.request.key,
                                        photosList: photosList,
                                        bookedDates: [],
                                      )
                                    : Container(
                                        height: 50,
                                      ),
                              );
                            });
                      });
                } else {
                  return Container(
                    height: 0,
                  );
                }
              });
        });
  }
}

class HouseRequestCard extends StatefulWidget {
  final OfferObject offerObject;

  // final HousesRequest housesRequest;
  final Function onArchive, onSend;
  final bool isArchive;
  final House house;
  final bool backOff;
  final User requestUser;

  const HouseRequestCard(
      {Key key, @required this.offerObject, this.onArchive, this.isArchive, this.house, this.onSend, this.backOff = true, this.requestUser})
      : super(key: key);

  @override
  _HouseRequestCardState createState() => _HouseRequestCardState();
}

class All {}

class _HouseRequestCardState extends State<HouseRequestCard> with HouseOffersHelper {
//   complexModel() async {
//     /*
//     if (_forceToReload == UserModel || _forceToReload == All) {
//       _currentModel.v1 = await mindClass.userInfo(
//           widget.housesRequest.requestOwnerId ??
//               widget.housesRequest.request.uid);
//     }
//     if (_forceToReload == HousesRequest || _forceToReload == All) {
//       _currentModel.v2 = await mindClass.houseOffer(widget.house,
//           widget.housesRequest.requestOwnerId, widget.housesRequest.requestId);
//       widget.housesRequest.archived = _currentModel.v2.archived;
//       mindClass.updateEvent('housesRequestUpdate');
//     }
//     _currentModel.v2 ??= widget.housesRequest;
//     _forceToReload = null;
//     _stream.add(_currentModel);
// */
//   }

/*
  SMapEntry<UserModel, HousesRequest> _currentModel = SMapEntry(null, null);

  Type _forceToReload = UserModel;
  */
  // SMapEntry<User, HousesRequest> _currentModel = SMapEntry(null, null);
  SMapEntry<User, OfferObject> _currentModel = SMapEntry(null, null);
  Type _forceToReload = User;

  // final StreamController _stream = StreamController();

  // Stream conversationUsersStream;

  @override
  void initState() {
    _currentModel.v2 = widget.offerObject;
    // complexModel();
    super.initState();

    // conversationUsersStream =
    //     widget.offerObject.conversationUsers.stream.asBroadcastStream();
  }

  @override
  void dispose() {
    // _stream.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // return Container(width: 100, height: 100, color: Colors.blue);
    return _buildOrderItem(context);
  }

  Widget _buildOrderItem(BuildContext context) => AnimatedSwitcher(
        duration: kThemeChangeDuration,
        child: Padding(
          key: ValueKey(_currentModel.toString()),
          padding: EdgeInsets.only(top: 16),
          child: SingleChildScrollView(
            child: Card(
              clipBehavior: Clip.antiAlias,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16))),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
/*                      if (debugMode) SelectableText(widget.housesRequest.messageInstanceId.toString()),*/
                  // if (debugMode) SelectableText('debug message mode'),
//                   Builder(builder: (context) {
//                     /*
//                       if (_currentModel.v1 != null && _currentModel.v1.username.isNotEmpty)
//                       if (_currentModel.v1 != null && _currentModel.v1.username.isNotEmpty)
//
//                        */
//
//                       return AnimatedSwitcher(
//                         duration: kThemeChangeDuration,
//                         transitionBuilder:
//                             (Widget child, Animation<double> animation) {
//                           return SizeTransition(
//                             axis: Axis.vertical,
//                             sizeFactor: animation,
//                             child: child,
//                           );
//                         },
//                         // child: UserTile(uid: widget.housesRequest.requestOwnerId ?? widget.housesRequest.request.uid, user: _currentModel.v1),
//                         // child: UserTile(uid: widget.offerObject.conversationUsers.requestUser.value, user: _currentModel.v1),
//                         child: UserTile(
//                             uid: widget.offerObject.conversationUsers
//                                 .requestUser.value),
// /*
//                             child: Container(
//                               color: Colors.grey,
//                               width: 40,
//                               height: 200,
//                             )*/
//                       );
//
//                   }),
//                   widget.offerObject.conversationUsers
                  // .requestUser.value

                  UserTile(
                    userInformation: widget.requestUser.userInformation,
                    uid: widget.requestUser.key,
                  ),
                  /*if (widget.housesRequest.status.currentRequestStatus.isNotEmpty)*/
                  if (widget?.offerObject?.conversationUsers?.usersStatuses?.requestUserStatus?.value != null)
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0).copyWith(top: 4, bottom: 4),
                      child: Text(StatusModel.convertStatus(widget.offerObject.conversationUsers.usersStatuses.requestUserStatus.value),
                          style: Theme.of(context).textTheme.headline6.copyWith(color: Colors.orange.shade800)),
                    ),
                  ListTile(
                    onTap: () {
                      return Navigator.push(
                        context,
                        CupertinoPageRoute(
                          builder: (context) => OrderDetailPage(
                            editable: false,
// requestId: '-M_RfBzXqtSo39ZSZp99',
                            requestCopy: widget.offerObject.requestCopy,
                            house: widget.house,
                            /*extraActions: _extraActions(context, widget.housesRequest.requestStateType, widget.housesRequest.messageInstanceId),*/
                            extraActions: _extraActions(context),
                          ),
                        ),
                      );
                    },
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        _buildDate(context),
//                         FutureBuilder<Map>(
// /*                              future: mindClass.getFType(widget.housesRequest.messageInstanceId),*/
//                           future: mindClass.getFType('getFType'),
//                           builder: (BuildContext context,
//                               AsyncSnapshot<Map> snapshot) {
//                             if (snapshot.hasData) if (snapshot.data != null &&
//                                 snapshot.data.length == 2) {
//                               return Text(
//                                   Strings.pages.ordersPage.houseRequestCard
//                                       .bookApproved
//                                       .l(context),
//                                   style: TextStyle(
//                                     color: Theme.of(context).primaryColor,
//                                   ));
//                             }
//                             return Container(
//                               width: 0,
//                               height: 0,
//                             );
//                           },
//                         )
                      ],
                    ),
                    subtitle: Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
                      _buildCity(context),
                      _buildMetro(context),
                      _buildPrice(context),
/*                          _buildBtns(context, widget.housesRequest.messageInstanceId),*/
                      _buildBtns(context, widget.offerObject.conversationUsers.chatId.value),
                      if (widget.isArchive != null)
                        Align(
                            alignment: Alignment.bottomRight,
                            child: IconButton(
                              onPressed: () {
                                widget.onArchive();
                                /*
                                    await mindClass.archiveHouseOffer(
                                        widget.house,
                                        widget.housesRequest.requestOwnerId,
                                        widget.housesRequest.requestId,
                                        !(_currentModel?.v2?.archived ??
                                            false));
                                    _forceToReload = HousesRequest;
                                    complexModel();
                                     */
                              },
                              // icon: Icon((_currentModel?.v2?.houseCopy
                              //             ?.isArchived?.value ??
                              //         false)
                              icon: Icon((widget.isArchive) ? Icons.unarchive : Icons.archive),
                            )),
                    ]),
                  ),
                ],
              ),
            ),
          ),
        ),
      );

//   Widget _buildOrderItem(BuildContext context) => StreamBuilder(
//       stream: _stream.stream,
//       builder: (context, snapshot) {
//         return AnimatedSwitcher(
//           duration: kThemeChangeDuration,
//           child: Padding(
//             key: ValueKey(_currentModel.toString()),
//             padding: EdgeInsets.only(
//               top: 16,
//             ),
//             child: MaterialContextMenu(
//               actions: [
//                 /*_buildBtns(context, widget.housesRequest.messageInstanceId),*/
//                 _buildBtns(context, 'message'),
//               ],
//               child: SingleChildScrollView(
//                 child: Card(
//                   clipBehavior: Clip.antiAlias,
//                   shape: RoundedRectangleBorder(
//                       borderRadius: BorderRadius.all(Radius.circular(16))),
//                   child: Column(
//                     mainAxisSize: MainAxisSize.min,
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
// /*                      if (debugMode) SelectableText(widget.housesRequest.messageInstanceId.toString()),*/
//                       if (debugMode) SelectableText('debug message mode'),
//                       Builder(builder: (context) {
//                         /*
//                         if (_currentModel.v1 != null && _currentModel.v1.username.isNotEmpty)
//                         if (_currentModel.v1 != null && _currentModel.v1.username.isNotEmpty)
//
//                          */
//                         if (true) {
//                           return AnimatedSwitcher(
//                             duration: kThemeChangeDuration,
//                             transitionBuilder:
//                                 (Widget child, Animation<double> animation) {
//                               return SizeTransition(
//                                 axis: Axis.vertical,
//                                 sizeFactor: animation,
//                                 child: child,
//                               );
//                             },
//                             // child: UserTile(uid: widget.housesRequest.requestOwnerId ?? widget.housesRequest.request.uid, user: _currentModel.v1),
//                             child: UserTile(
//                                 uid: widget.offerObject.conversationUsers
//                                     .requestUser.value,
//                                 user: _currentModel.v1),
// /*
//                               child: Container(
//                                 color: Colors.grey,
//                                 width: 40,
//                                 height: 200,
//                               )*/
//                           );
//                         }
//                         return SizedBox();
//                       }),
//                       /*if (widget.housesRequest.status.currentRequestStatus.isNotEmpty)*/
//                       if (widget?.offerObject?.conversationUsers?.usersStatuses
//                               ?.requestUserStatus?.value !=
//                           null)
//                         Padding(
//                           padding: const EdgeInsets.symmetric(horizontal: 16.0)
//                               .copyWith(top: 4, bottom: 4),
//                           child: Text(
//                               widget.offerObject.conversationUsers.usersStatuses
//                                   .requestUserStatus.value,
//                               style: Theme.of(context)
//                                   .textTheme
//                                   .headline6
//                                   .copyWith(color: Colors.orange.shade800)),
//                         ),
//                       ListTile(
//                         onTap: () => Navigator.push(
//                           context,
//                           CupertinoPageRoute(
//                             builder: (context) => OrderDetailPage(
//                               editable: false,
//                               // request: widget.housesRequest.request,
//                               house: widget.house,
//                               /*extraActions: _extraActions(context, widget.housesRequest.requestStateType, widget.housesRequest.messageInstanceId),*/
//                               extraActions:
//                                   _extraActions(context, 'message instance'),
//                             ),
//                           ),
//                         ),
//                         title: Row(
//                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                           children: <Widget>[
//                             _buildDate(context),
//                             FutureBuilder<Map>(
// /*                              future: mindClass.getFType(widget.housesRequest.messageInstanceId),*/
//                               future: mindClass.getFType('getFType'),
//                               builder: (BuildContext context,
//                                   AsyncSnapshot<Map> snapshot) {
//                                 if (snapshot.hasData) if (snapshot.data !=
//                                         null &&
//                                     snapshot.data.length == 2) {
//                                   return Text(Strings.pages.ordersPage.houseRequestCard.bookApproved.l(context),
//                                       style: TextStyle(
//                                         color: Theme.of(context).primaryColor,
//                                       ));
//                                 }
//                                 return Container(
//                                   width: 0,
//                                   height: 0,
//                                 );
//                               },
//                             )
//                           ],
//                         ),
//                         subtitle: Column(
//                             mainAxisAlignment: MainAxisAlignment.start,
//                             crossAxisAlignment: CrossAxisAlignment.start,
//                             children: [
//                               _buildCity(context),
//                               _buildMetro(context),
//                               _buildPrice(context),
// /*                          _buildBtns(context, widget.housesRequest.messageInstanceId),*/
//                               _buildBtns(
//                                   context,
//                                   widget.offerObject.conversationUsers.chatId
//                                       .value),
//                               Align(
//                                   alignment: Alignment.bottomRight,
//                                   child: IconButton(
//                                     onPressed: () {
//                                       widget.onArchive();
//                                       /*
//                                       await mindClass.archiveHouseOffer(
//                                           widget.house,
//                                           widget.housesRequest.requestOwnerId,
//                                           widget.housesRequest.requestId,
//                                           !(_currentModel?.v2?.archived ??
//                                               false));
//                                       _forceToReload = HousesRequest;
//                                       complexModel();
//                                        */
//                                     },
//                                     // icon: Icon((_currentModel?.v2?.houseCopy
//                                     //             ?.isArchived?.value ??
//                                     //         false)
//                                     icon: Icon((!widget.isArchive)
//                                         ? Icons.archive
//                                         : Icons.unarchive),
//                                   )),
//                             ]),
//                       ),
//                     ],
//                   ),
//                 ),
//               ),
//             ),
//           ),
//         );
//       });

  Widget _extraActions(BuildContext context) {
    var isOpened = false;
    return Padding(
      padding: EdgeInsets.only(left: 16, right: 16, bottom: 16),
      child: Container(
        decoration: BoxDecoration(border: Border.all(color: Colors.black38, width: 1), borderRadius: BorderRadius.circular(16)),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsets.only(left: 14 + 8.0, top: 8),
                child: Text(
                  Strings.pages.ordersPage.houseRequestCard.extraActions.l(context),
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Wrap(
                alignment: WrapAlignment.center,
                spacing: 10,
                children: <Widget>[
                  true //TODO
                      ? FlatButton(
                          padding: EdgeInsets.all(0),
                          onPressed: () async {
                            setOwnerStatus(widget.offerObject, 'request_accept');
                            //await mindClass.requestConfirm(messageInstanceId);
                          },
                          child: Text(
                            Strings.pages.ordersPage.houseRequestCard.acceptRequest.l(context),
                            style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 16),
                          ),
                        )
                      : Container(),
//                  type == DatabaseModule.chat_request
//                      ?
                  FlatButton(
                    padding: EdgeInsets.all(0),
                    onPressed: () async {
                      var denyReason = '';
                      isOpened = true;
                      await Flushbar(
                          borderRadius: BorderRadius.circular(8),
                          routeColor: Colors.black54,
                          routeBlur: 0.1,
                          margin: EdgeInsets.all(8),
                          backgroundColor: Colors.white,
                          flushbarStyle: FlushbarStyle.FLOATING,
                          userInputForm: Form(
                              child: Padding(
                                  padding: const EdgeInsets.all(16.0),
                                  child: Container(
                                      child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                                    ListTile(
                                      title: Text(Strings.pages.ordersPage.houseRequestCard.declineRequestMessage.l(context)),
                                      subtitle: Text(Strings.pages.ordersPage.houseRequestCard.reasonToDeclineAsking.l(context)),
                                    ),
                                    SizedBox(height: 16 * 2.0),
                                    TextField(
                                      decoration: InputDecoration(hintText: Strings.pages.ordersPage.houseRequestCard.reasonToDecline.l(context)),
                                      onChanged: (val) {
                                        denyReason = val;
                                      },
                                    ),
                                    SizedBox(height: 16 * 2.0),
                                    Container(
                                        width: double.maxFinite,
                                        child: Wrap(alignment: WrapAlignment.end, spacing: 8, children: <Widget>[
                                          FlatButton(
                                              color: Theme.of(context).primaryColor,
                                              onPressed: () async {
                                                // setRequestDenyReason(
                                                //    widget.offerObject,
                                                //    denyReason);
                                                setOwnerStatus(
                                                    widget.offerObject, denyReason == '' ? 'request_denied' : 'request_denied;$denyReason');

                                                if (isOpened) {
                                                  Navigator.pop(context);
                                                  isOpened = false;
                                                }
                                                /*
                                                      await mindClass
                                                          .offerRefuse(
                                                              messageInstanceId,
                                                              value);

                                                      _forceToReload =
                                                          HousesRequest;
                                                      complexModel();
                                                      Navigator.pop(
                                                          context);

                                                       */
                                              },
                                              child: Text(
                                                Strings.pages.ordersPage.houseRequestCard.send.l(context),
                                                style: TextStyle(
                                                  fontSize: 16,
                                                  color: Colors.white,
                                                ),
                                              ))
                                        ]))
                                  ]))))).show(context);
                      // var value = '';
                      //
                      // await Flushbar(
                      //   borderRadius: BorderRadius.circular(8),
                      //   routeColor: Colors.black54,
                      //   routeBlur: 0.1,
                      //   margin: EdgeInsets.all(8),
                      //   backgroundColor: Colors.white,
                      //   flushbarStyle: FlushbarStyle.FLOATING,
                      //   userInputForm: Form(
                      //     child: Padding(
                      //       padding: const EdgeInsets.all(16.0),
                      //       child: Container(
                      //         child: Column(
                      //           mainAxisSize: MainAxisSize.min,
                      //           children: <Widget>[
                      //             ListTile(
                      //               title: Text(Strings.pages.ordersPage
                      //                   .houseRequestCard.declineRequestMessage
                      //                   .l(context)),
                      //               subtitle: Text(Strings.pages.ordersPage
                      //                   .houseRequestCard.reasonToDeclineAsking
                      //                   .l(context)),
                      //             ),
                      //             SizedBox(
                      //               height: 16 * 2.0,
                      //             ),
                      //             TextField(
                      //               decoration: InputDecoration(
                      //                   hintText: Strings.pages.ordersPage
                      //                       .houseRequestCard.reasonToDecline
                      //                       .l(context)),
                      //               onChanged: (val) {
                      //                 value = val;
                      //               },
                      //             ),
                      //             SizedBox(
                      //               height: 16 * 2.0,
                      //             ),
                      //             Container(
                      //               width: double.maxFinite,
                      //               child: Wrap(
                      //                 alignment: WrapAlignment.end,
                      //                 spacing: 8,
                      //                 children: <Widget>[
                      //                   FlatButton(
                      //                     color: Theme.of(context).primaryColor,
                      //                     child: Text(
                      //                       Strings.pages.ordersPage
                      //                           .houseRequestCard.send
                      //                           .l(context),
                      //                       style: TextStyle(
                      //                         fontSize: 16,
                      //                         color: Colors.white,
                      //                       ),
                      //                     ),
                      //                     onPressed: () async {
                      //                       // await mindClass.requestRefuse(
                      //                       //     messageInstanceId, value);
                      //                       Navigator.pop(context);
                      //                     },
                      //                   ),
                      //                 ],
                      //               ),
                      //             )
                      //           ],
                      //         ),
                      //       ),
                      //     ),
                      //   ),
                      // ).show(context);
                    },
                    child: Text(
                      Strings.pages.ordersPage.houseRequestCard.declineRequest.l(context),
                      style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 16),
                    ),
                  ),
                  true //TODO
                      ? FlatButton(
                          padding: EdgeInsets.all(0),
                          onPressed: () async {
                            var chatIdFuture = await widget?.offerObject?.conversationUsers?.chatId?.future;
                            var chatId = chatIdFuture?.value;
                            if (chatId != null) {
                              await Navigator.push(
                                  context,
                                  CupertinoPageRoute(
                                      builder: (context) => Messages(
                                            isOwner: true,
                                            finalButton: true,
                                            messageInstanceId: chatId,
                                            bookedPeriods: widget?.house?.bookingDates?.values?.toList() ?? [],
                                            bookingPeriod: widget.offerObject.requestCopy.datesRange,
                                          )));
                              return;
                            } else {
                              chatId ??= await createChat(widget.offerObject.conversationUsers.ownerUser.value, widget.offerObject.key);

                              await Navigator.push(
                                  context,
                                  CupertinoPageRoute(
                                      builder: (context) => Messages(
                                            isOwner: true,
                                            finalButton: true,
                                            messageInstanceId: chatId,
                                            bookedPeriods: widget?.house?.bookingDates?.values?.toList() ?? [],
                                            bookingPeriod: widget.offerObject.requestCopy.datesRange,
                                          )));
                            }

                            // if (requestUserStatus !=
                            //     'house_request_canceled') {
                            //   chatId ??= await createChat(
                            //       widget.offerObject.conversationUsers
                            //           .ownerUser.value,
                            //       widget.offerObject.key);
                            //
                            //   await Navigator.push(
                            //       context,
                            //       CupertinoPageRoute(
                            //           builder: (context) => Messages(
                            //               isOwner: true,
                            //               finalButton: true,
                            //               messageInstanceId: chatId)));
                            // } else if (requestUserStatus ==
                            //     'house_request_canceled') {
                            //   await Fluttertoast.showToast(
                            //     textColor: Colors.white,
                            //     backgroundColor: Colors.black54,
                            //     msg: Strings.pages.ordersPage
                            //         .houseRequestCard.userDeclinedRequest
                            //         .l(context),
                            //     toastLength: Toast.LENGTH_SHORT,
                            //     gravity: ToastGravity.BOTTOM,
                            //     timeInSecForIosWeb: 2,
                            //   );
                            // }

                            // // print('ChatId: ${chatIdFuture.value}');
                            // CupertinoPageRoute(
                            //   builder: (context) => Messages(
                            //     isOwner: true,
                            //     messageInstanceId: chatIdFuture.value,
                            //   ),
                            // );
                          },
                          child: Text(
                            Strings.pages.ordersPage.houseRequestCard.chat.l(context),
                            style: TextStyle(color: Colors.black, fontSize: 16),
                          ),
                        )
                      : Container(),

                  // : Container(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildBtns(BuildContext context, messageInstanceId) {
    Widget extraActions = Padding(
        padding: EdgeInsets.only(left: 16, right: 16, bottom: 16),
        child: Container(
            decoration: BoxDecoration(border: Border.all(color: Colors.black38, width: 1), borderRadius: BorderRadius.circular(16)),
            child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
              Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: const EdgeInsets.only(left: 14 + 8.0, top: 8),
                  child: Text(
                    Strings.pages.ordersPage.houseRequestCard.extraActions.l(context),
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
              Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Wrap(alignment: WrapAlignment.end, children: <Widget>[
                    true //TODO
                        ? FlatButton(
                            padding: EdgeInsets.all(0),
                            onPressed: () {
                              Navigator.push(
                                context,
                                CupertinoPageRoute(
                                  builder: (context) => Messages(
                                    isOwner: true,
                                    messageInstanceId: messageInstanceId,
                                    bookedPeriods: widget?.house?.bookingDates?.values?.toList() ?? [],
                                    bookingPeriod: widget.offerObject.requestCopy.datesRange,
                                  ),
                                ),
                              );
                            },
                            child: Text(
                              Strings.pages.ordersPage.houseRequestCard.chat.l(context),
                              style: TextStyle(color: Colors.black, fontSize: 16),
                            ),
                          )
                        : Container(),
                    true //TODO
                        ? FlatButton(
                            padding: EdgeInsets.all(0),
                            onPressed: () async {
                              // await mindClass.requestConfirm(messageInstanceId);
                            },
                            child: Text(
                              Strings.pages.ordersPage.houseRequestCard.acceptRequest.l(context),
                              style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 16),
                            ),
                          )
                        : Container(),
//                  type == DatabaseModule.chat_request
//                      ?
                    FlatButton(
                        padding: EdgeInsets.all(0),
                        onPressed: () async {
                          var value = '';

                          await Flushbar(
                              borderRadius: BorderRadius.circular(8),
                              routeColor: Colors.black54,
                              routeBlur: 0.1,
                              margin: EdgeInsets.all(8),
                              backgroundColor: Colors.white,
                              flushbarStyle: FlushbarStyle.FLOATING,
                              userInputForm: Form(
                                  child: Padding(
                                      padding: const EdgeInsets.all(16.0),
                                      child: Container(
                                          child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                                        ListTile(
                                          title: Text(Strings.pages.ordersPage.houseRequestCard.declineRequestMessage.l(context)),
                                          subtitle: Text(Strings.pages.ordersPage.houseRequestCard.reasonToDeclineAsking.l(context)),
                                        ),
                                        SizedBox(
                                          height: 16 * 2.0,
                                        ),
                                        TextField(
                                          decoration: InputDecoration(hintText: Strings.pages.ordersPage.houseRequestCard.reasonToDecline.l(context)),
                                          onChanged: (val) {
                                            value = val;
                                          },
                                        ),
                                        SizedBox(
                                          height: 16 * 2.0,
                                        ),
                                        Container(
                                            width: double.maxFinite,
                                            child: Wrap(alignment: WrapAlignment.end, spacing: 8, children: <Widget>[
                                              FlatButton(
                                                  color: Theme.of(context).primaryColor,
                                                  child: Text(
                                                    Strings.pages.ordersPage.houseRequestCard.send.l(context),
                                                    style: TextStyle(
                                                      fontSize: 16,
                                                      color: Colors.white,
                                                    ),
                                                  ),
                                                  onPressed: () async {
                                                    // await mindClass.requestRefuse(
                                                    //     messageInstanceId, value);
                                                    Navigator.pop(context);
                                                  })
                                            ]))
                                      ]))))).show(context);
                        },
                        child: Text(
                          Strings.pages.ordersPage.houseRequestCard.declineRequest.l(context),
                          style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 16),
                        ))
                    // : Container(),
                  ]))
            ])));
    var chipTheme = Theme.of(context).copyWith(
        chipTheme: Theme.of(context)
            .chipTheme
            .copyWith(pressElevation: 0, labelStyle: Theme.of(context).chipTheme.labelStyle.copyWith(color: Theme.of(context).primaryColor)));
    // Future isOpened;
    var isOpened = false;

    void closeFlushBar() {
      if (isOpened) {
        Navigator.pop(context);
        isOpened = false;
      }
    }

    // print(
    //     'Price start: ${widget.offerObject.houseCopy.priceRange.startPosition.value}');
    // print(
    //     'Price end: ${widget.offerObject.houseCopy.priceRange.endPosition.value}');

    var offerObjectChatId = widget?.offerObject?.conversationUsers?.chatId?.value;
    var offerObjectRequestUserStatus = widget?.offerObject?.conversationUsers?.usersStatuses?.requestUserStatus?.value;
    return Theme(
        data: chipTheme,
        child: Wrap(spacing: 16, alignment: WrapAlignment.center, children: <Widget>[
          ActionChip(
              padding: EdgeInsets.all(0),
              backgroundColor: Colors.transparent,
              onPressed: () async {
                if (offerObjectRequestUserStatus != 'house_request_canceled') {
                  offerObjectChatId ??= await createChat(widget.offerObject.conversationUsers.ownerUser.value, widget.offerObject.key);
                  enterInChat(offerObjectChatId);
                  await Navigator.push(
                      context,
                      CupertinoPageRoute(
                          builder: (context) => Messages(
                            isOwner: true,
                            finalButton: true,
                            messageInstanceId: offerObjectChatId,
                            bookedPeriods: widget?.house?.bookingDates?.values?.toList() ?? [],
                            bookingPeriod: widget.offerObject.requestCopy.datesRange,
                          )));
                } else if (offerObjectRequestUserStatus == 'house_request_canceled') {
                  await Fluttertoast.showToast(
                    textColor: Colors.white,
                    backgroundColor: Colors.black54,
                    msg: Strings.pages.ordersPage.houseRequestCard.userDeclinedRequest.l(context),
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.BOTTOM,
                    timeInSecForIosWeb: 2,
                  );
                }

                return;
                /*
                if (offerObjectChatId != null) {
                  // print('Pushed to the chat');
                  enterInChat(offerObjectChatId);
                  await Navigator.push(
                      context,
                      CupertinoPageRoute(
                          builder: (context) => Messages(
                                isOwner: true,
                                finalButton: true,
                                messageInstanceId: offerObjectChatId,
                                bookedPeriods: widget?.house?.bookingDates?.values?.toList() ?? [],
                                bookingPeriod: widget.offerObject.requestCopy.datesRange,
                              )));
                  return;
                }

                if (offerObjectRequestUserStatus != 'house_request_canceled') {
                  offerObjectChatId ??= await createChat(widget.offerObject.conversationUsers.ownerUser.value, widget.offerObject.key);
                  enterInChat(offerObjectChatId);
                  await Navigator.push(
                      context,
                      CupertinoPageRoute(
                          builder: (context) => Messages(
                                isOwner: true,
                                finalButton: true,
                                messageInstanceId: offerObjectChatId,
                                bookedPeriods: widget?.house?.bookingDates?.values?.toList() ?? [],
                                bookingPeriod: widget.offerObject.requestCopy.datesRange,
                              )));
                } else if (offerObjectRequestUserStatus == 'house_request_canceled') {
                  await Fluttertoast.showToast(
                    textColor: Colors.white,
                    backgroundColor: Colors.black54,
                    msg: Strings.pages.ordersPage.houseRequestCard.userDeclinedRequest.l(context),
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.BOTTOM,
                    timeInSecForIosWeb: 2,
                  );
                }
                */
                // Navigator.push(
                //     context,
                //     CupertinoPageRoute(
                //         builder: (context) => Messages(
                //             isOwner: true,
                //             finalButton: true,
                //             messageInstanceId: messageInstanceId)));
              },
              label: Text(Strings.pages.ordersPage.homeOrderDetailPage.chat.l(context))),

          // StreamBuilder<ConversationUsers>(
          //         stream: conversationUsersStream,
          //         builder: (context, snapshot) {
          //           if (snapshot.hasData && !snapshot.hasError) {
          //             var chatId = snapshot?.data?.chatId?.value;
          //             var requestUserStatus =
          //                 snapshot.data.usersStatuses.requestUserStatus.value;
          //
          //             return ActionChip(
          //                 padding: EdgeInsets.all(0),
          //                 backgroundColor: Colors.transparent,
          //                 onPressed: () async {
          //                   if (snapshot.data.chatId.value != null) {
          //                     // print('Pushed to the chat');
          //                     await Navigator.push(
          //                         context,
          //                         CupertinoPageRoute(
          //                             builder: (context) => Messages(
          //                                 isOwner: true,
          //                                 finalButton: true,
          //                                 messageInstanceId: chatId)));
          //                     return;
          //                   }
          //
          //                   if (requestUserStatus != 'house_request_canceled') {
          //                     chatId ??= await createChat(
          //                         widget.offerObject.conversationUsers.ownerUser
          //                             .value,
          //                         widget.offerObject.key);
          //
          //                     await Navigator.push(
          //                         context,
          //                         CupertinoPageRoute(
          //                             builder: (context) => Messages(
          //                                 isOwner: true,
          //                                 finalButton: true,
          //                                 messageInstanceId: chatId)));
          //                   } else if (requestUserStatus ==
          //                       'house_request_canceled') {
          //                     await Fluttertoast.showToast(
          //                       textColor: Colors.white,
          //                       backgroundColor: Colors.black54,
          //                       msg: Strings.pages.ordersPage.houseRequestCard
          //                           .userDeclinedRequest
          //                           .l(context),
          //                       toastLength: Toast.LENGTH_SHORT,
          //                       gravity: ToastGravity.BOTTOM,
          //                       timeInSecForIosWeb: 2,
          //                     );
          //                   }
          //
          //                   // Navigator.push(
          //                   //     context,
          //                   //     CupertinoPageRoute(
          //                   //         builder: (context) => Messages(
          //                   //             isOwner: true,
          //                   //             finalButton: true,
          //                   //             messageInstanceId: messageInstanceId)));
          //                 },
          //                 label: Text(Strings
          //                     .pages.ordersPage.homeOrderDetailPage.price
          //                     .l(context)));
          //
          //             // return FlatButton(
          //             //   padding: EdgeInsets.all(0),
          //             //   onPressed: () async {
          //             //     if (requestUserStatus != 'house_request_canceled') {
          //             //       if (chatId == null) {
          //             //         await createChat(
          //             //             widget.offerObject.conversationUsers.ownerUser
          //             //                 .value,
          //             //             widget.offerObject.key);
          //             //       }
          //             //
          //             //       await Navigator.push(
          //             //         context,
          //             //         CupertinoPageRoute(
          //             //           builder: (context) => Messages(
          //             //             isOwner: true,
          //             //             messageInstanceId: chatId,
          //             //           ),
          //             //         ),
          //             //       );
          //             //     } else if (requestUserStatus !=
          //             //         'house_request_canceled') {
          //             //       await Fluttertoast.showToast(
          //             //         textColor: Colors.white,
          //             //         backgroundColor: Colors.black54,
          //             //         msg: Strings.pages.ordersPage.houseRequestCard
          //             //             .userDeclinedRequest
          //             //             .l(context),
          //             //         toastLength: Toast.LENGTH_SHORT,
          //             //         gravity: ToastGravity.BOTTOM,
          //             //         timeInSecForIosWeb: 2,
          //             //       );
          //             //     }
          //             //   },
          //             //   child: Text(
          //             //     Strings.pages.ordersPage.houseRequestCard.chat
          //             //         .l(context),
          //             //     style: TextStyle(color: Colors.black, fontSize: 16),
          //             //   ),
          //             // );
          //           }
          //           return Container();
          //         }),

          // ActionChip(
          //     padding: EdgeInsets.all(0),
          //     onPressed: () {
          //       Navigator.push(
          //           context,
          //           CupertinoPageRoute(
          //               builder: (context) => Messages(
          //                   isOwner: true,
          //                   finalButton: true,
          //                   messageInstanceId: messageInstanceId)));
          //     },
          //     label: const Text(Strings.pages.ordersPage.houseRequestCard.chat.l(context))),
          /*if (_currentModel.v2.status.acceptOffer)*/
          ActionChip(
              padding: EdgeInsets.all(0),
              backgroundColor: Colors.transparent,
              onPressed: () async {
                closeFlushBar();
                setOwnerStatus(widget.offerObject, 'request_accept');

                /*
                  await mindClass.offerConfirm(messageInstanceId);
                  if(widget.backOff)
                  Navigator.pop(context);
                  _forceToReload = HousesRequest;
                  complexModel();
                   */
              },
              label: Text(Strings.pages.ordersPage.houseRequestCard.acceptRequest.l(context))),
          /*if (_currentModel.v2.status.bookOffer)*/

          /*if (_currentModel.v2.status.refuseOffer)*/

          ActionChip(
              padding: EdgeInsets.all(0),
              backgroundColor: Colors.transparent,
              onPressed: () async {
                var denyReason = '';
                isOpened = true;
                await Flushbar(
                    borderRadius: BorderRadius.circular(8),
                    routeColor: Colors.black54,
                    routeBlur: 0.1,
                    margin: EdgeInsets.all(8),
                    backgroundColor: Colors.white,
                    flushbarStyle: FlushbarStyle.FLOATING,
                    userInputForm: Form(
                        child: Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Container(
                                child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                              ListTile(
                                title: Text(Strings.pages.ordersPage.houseRequestCard.declineRequestMessage.l(context)),
                                subtitle: Text(Strings.pages.ordersPage.houseRequestCard.reasonToDeclineAsking.l(context)),
                              ),
                              SizedBox(height: 16 * 2.0),
                              TextField(
                                decoration: InputDecoration(hintText: Strings.pages.ordersPage.houseRequestCard.reasonToDecline.l(context)),
                                onChanged: (val) {
                                  denyReason = val;
                                },
                              ),
                              SizedBox(height: 16 * 2.0),
                              Container(
                                  width: double.maxFinite,
                                  child: Wrap(alignment: WrapAlignment.end, spacing: 8, children: <Widget>[
                                    FlatButton(
                                        color: Theme.of(context).primaryColor,
                                        onPressed: () async {
                                          // setRequestDenyReason(
                                          //     widget.offerObject,
                                          //     denyReason);
                                          setOwnerStatus(widget.offerObject, denyReason == '' ? 'request_denied' : 'request_denied;$denyReason');

                                          closeFlushBar();
                                          /*
                                                      await mindClass
                                                          .offerRefuse(
                                                              messageInstanceId,
                                                              value);

                                                      _forceToReload =
                                                          HousesRequest;
                                                      complexModel();
                                                      Navigator.pop(
                                                          context);

                                                       */
                                        },
                                        child: Text(
                                          Strings.pages.ordersPage.houseRequestCard.send.l(context),
                                          style: TextStyle(
                                            fontSize: 16,
                                            color: Colors.white,
                                          ),
                                        ))
                                  ]))
                            ]))))).show(context);
                // .then((value) {
                //   if (widget.backOff) Navigator.pop(context);
                //   return isOpened = null;
                // });
              },
              label: Text(Strings.pages.ordersPage.houseRequestCard.declineRequest.l(context))),
          if (widget?.offerObject?.conversationUsers?.usersStatuses?.requestUserStatus?.value == 'request_booked')
            ActionChip(
                padding: EdgeInsets.all(0),
                backgroundColor: Colors.transparent,
                onPressed: () async {
                  closeFlushBar();

                  var bookedList = widget.house.bookingDates.values.toList();
                  var newBookingDates = widget.offerObject.requestCopy.datesRange;
                  var ifAnyIntersections = checkDatesIntersection(bookedList, newBookingDates);

                  if (ifAnyIntersections) {
                    await Fluttertoast.showToast(
                        textColor: Colors.white,
                        backgroundColor: Colors.black54,
                        msg: 'Бронирование недоступно из-за переечения дат в заявке',
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.BOTTOM,
                        timeInSecForIosWeb: 1);
                  } else {
                    setOwnerStatus(widget.offerObject, 'request_booked');
                    if (widget.offerObject.conversationUsers.usersStatuses.requestUserStatus.value == 'request_booked') {
                      var autoBookingSetup = await apartmentsAutoBookingBool(widget.house.key);
                      if (autoBookingSetup != null && autoBookingSetup == true) {
                        await addApartmentBookingDates(widget.house.key, widget.offerObject.requestCopy.datesRange);
                      } else if (autoBookingSetup != null && autoBookingSetup == false) {
                        var bookingPeriod = widget.offerObject.requestCopy.datesRange;
                        var convertedBookingPeriod = bookingPeriodString(bookingPeriod);

                        await _addPeriodOrNot(context, convertedBookingPeriod);
                      }
                    }
                  }

                  /*
                  await mindClass.offerBooked(messageInstanceId);
                  if(widget.backOff)
                    Navigator.pop(context);
                  _forceToReload = HousesRequest;
                  complexModel();
                   */
                },
                label: Text(Strings.pages.ordersPage.houseRequestCard.book.l(context))),
        ]));
  }

  bool checkDatesIntersection(List<BookingPeriod> bookedDates, DatesRange bookingDates) {
    var result = true;
    for (var date in bookedDates) {
      var startDate = date.checkIn.value;
      var endDate = date.checkOut.value;
      if (!((startDate > bookingDates.last.value) || (endDate < bookingDates.first.value))) {
        return true;
      }
    }
    return false;
  }

  Future<void> _addPeriodOrNot(BuildContext context, String convertedBookingPeriod) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(Strings.pages.ordersPage.houseRequestCard.datesBooking.l(context)),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                    '${Strings.pages.ordersPage.houseRequestCard.bookPeriod.l(context)} ($convertedBookingPeriod) ${Strings.pages.ordersPage.houseRequestCard.forChosenFlat.l(context)}?'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              style: TextButton.styleFrom(
                primary: Colors.orange,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(Strings.pages.ordersPage.houseRequestCard.no.l(context)),
            ),
            TextButton(
              style: TextButton.styleFrom(
                primary: Colors.orange,
              ),
              onPressed: () async {
                await addApartmentBookingDates(widget.house.key, widget.offerObject.requestCopy.datesRange);
                Navigator.of(context).pop();
              },
              child: Text(Strings.pages.ordersPage.houseRequestCard.yes.l(context)),
            ),
          ],
        );
      },
    );
  }

  Widget _buildPrice(BuildContext context) {
    return Row(children: <Widget>[
      Icon(
        Partnerum.ruble,
        color: Colors.grey,
        size: 14,
      ),
      Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Text(
              Strings.pages.ordersPage.houseRequestCard.priceFrom.l(context) +
                  ' ' +
                  (widget?.offerObject?.houseCopy?.priceRange?.startPosition?.value ?? '0') +
                  ' ' +
                  Strings.pages.ordersPage.houseRequestCard.priceTo.l(context) +
                  ' ' +
                  (widget?.offerObject?.houseCopy?.priceRange?.endPosition?.value ?? '00') +
                  ' ' +
                  Strings.pages.ordersPage.houseRequestCard.priceMeasure.l(context),
              style: TextStyle(fontSize: 14, color: Colors.grey)))
    ]);
  }

  Widget _buildMetro(BuildContext context) => Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
        Icon(
          Partnerum.metro,
          color: Colors.grey,
          size: 14,
        ),
        Flexible(
            child: Padding(
                padding: const EdgeInsets.only(left: 8),
                child: Container(
                    //height: 16,
                    /*
                    child: Text(widget.housesRequest.request.info.stations.join(", "),
   */
/*                    child: Text('housesRequest.stations',*/
                    child: Text(getStationsList().join(', '),
                        softWrap: true,
                        //overflow: TextOverflow.fade,
                        style: TextStyle(
                          fontSize: 14,
                          color: Colors.grey,
                        )))))
      ]);

  Widget _buildCity(BuildContext context) => Row(children: <Widget>[
        Icon(
          Partnerum.city,
          color: Colors.grey,
          size: 14,
        ),
        Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Text('${widget.offerObject.houseCopy.city.value}', style: TextStyle(fontSize: 14, color: Colors.grey)))
        // child: Text('housesRequest.city', style: TextStyle(fontSize: 14, color: Colors.grey)))
      ]);

  String formatDate(int timestamp) {
    return DateFormat('dd MMM', 'ru').format(DateTime.fromMillisecondsSinceEpoch(timestamp));
  }

  Widget _buildDate(BuildContext context) => Text(
      '${formatDate(widget.offerObject.requestCopy.datesRange.first.value.inMilliseconds)} ' +
          '-' +
          ' ${formatDate(widget.offerObject.requestCopy.datesRange.last.value.inMilliseconds)}',
      style: TextStyle(fontSize: 16, color: Colors.black, fontWeight: FontWeight.bold));

  List getStationsList() {
    var stations = widget?.offerObject?.houseCopy?.stations?.values?.map((e) => e.key)?.toList() ?? [];
    return stations;
  }

// Text('dates', style: TextStyle(fontSize: 16, color: Colors.black, fontWeight: FontWeight.bold));
}

class HomeOrderDetailPage extends StatelessWidget {
  final Request request;

  const HomeOrderDetailPage({Key key, this.request}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (request == null) {
      return Container(
        color: Colors.white,
        alignment: FractionalOffset.center,
        child: Center(
          child: Text(Strings.pages.ordersPage.homeOrderDetailPage.uploadIssue.l(context)),
        ),
      );
    }

    final Request document = request;

    return Scaffold(
        appBar: _buildAppBar(document, context),
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
            child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
          _buildCity(document, context),
          _buildMetro(document, context),
          // _buildDistance(document, context),
          _buildPrice(document, context),
          _buildRooms(document, context),
          _buildGuests(document, context),
          _buildComission(document, context),
          _buildInfo(document, context)
        ])));
  }

  Widget _buildAppBar(Request request, BuildContext context) => AppBar(
      leading: IconButton(
        icon: Theme.of(context).platform == TargetPlatform.iOS ? Icon(Icons.arrow_back_ios) : Icon(Icons.arrow_back),
        onPressed: () => Navigator.of(context).pop(),
      ),
      title: Text(
          /*
          '${DateFormat('dd MMM', 'ru').format(dateFormat.parse(request.info.datesRange.first)).toString()} - ${DateFormat('dd MMM', 'ru').format(dateFormat.parse(request.info.datesRange.last)).toString()}'
      */
          'text'),
      iconTheme: IconThemeData(
        color: Colors.black,
      ),
      centerTitle: false,
      textTheme: TextTheme(
          subtitle1: TextStyle(
        color: Colors.black,
        fontSize: 20.0,
      )),
      backgroundColor: Colors.white);

  Widget _buildInfo(Request document, BuildContext context) {
    /*
    return ExpansionTile(leading: Icon(Partnerum.info, color: Theme.of(context).primaryColor), title: Text('Информация'), children: <Widget>[
      Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: Align(
              alignment: Alignment.topLeft,
              child: document.info.extraInformation != '' ? Text('${document.info.extraInformation}') : Text('Не указана')))
    ]);
     */
  }

  Widget _buildComission(Request document, BuildContext context) {
    return ListTile(
        leading: Icon(Partnerum.percent, color: Theme.of(context).primaryColor),
        title: Text(Strings.pages.ordersPage.homeOrderDetailPage.commissionSize.l(context)),
        subtitle: Text(
            /*
            '${document.info.commission} %'

           */
            'abc'));
  }

  Widget _buildGuests(Request document, BuildContext context) => ListTile(
      leading: Icon(Partnerum.guest, color: Theme.of(context).primaryColor),
      title: Text(Strings.pages.ordersPage.homeOrderDetailPage.guestsAmount.l(context)),
      subtitle: Text(
          /*
          '${document.info.guests} чел'

         */
          Strings.pages.ordersPage.homeOrderDetailPage.guestsAmountMeasure.l(context)));

  Widget _buildRooms(Request document, BuildContext context) {
    /*
    return ListTile(
        leading: Icon(Partnerum.rooms, color: Theme.of(context).primaryColor),
        title: Text('Количество комнат'),
        subtitle: Container(
            height: 16,
            child: ListView.builder(
                shrinkWrap: true,
                physics: ClampingScrollPhysics(),
                itemCount: document.info.rooms.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return Text('${document.info.rooms[index]}, ');
                })));

     */
  }

  Widget _buildPrice(Request document, BuildContext context) => ListTile(
      leading: Icon(Partnerum.ruble, color: Theme.of(context).primaryColor),
      title: Text(Strings.pages.ordersPage.homeOrderDetailPage.price.l(context)),
      subtitle: Text(
          /*
          'Strings.pages.ordersPage.homeOrderDetailPage.priceFrom.l(context) ${document.info.priceRange[0]} Strings.pages.ordersPage.homeOrderDetailPage.priceTo.l(context) ${document.info.priceRange[1]} руб/сут'

         */
          Strings.pages.ordersPage.homeOrderDetailPage.priceMeasure.l(context)));

  Widget _buildCity(Request document, BuildContext context) => ListTile(
      leading: Icon(Partnerum.city, color: Theme.of(context).primaryColor),
      title: Text(Strings.pages.ordersPage.homeOrderDetailPage.city.l(context)),
      subtitle: Text(
          /*
          '${document.info.city}'

       */
          'info city'));

  Widget _buildMetro(Request document, BuildContext context) => ExpansionTile(
          leading: Icon(Partnerum.metro, color: Theme.of(context).primaryColor),
          /*
      title: Text('Станции метро (${document.info.stations.length})'),

       */
          title: Text(Strings.pages.ordersPage.homeOrderDetailPage.metroStations.l(context)),
          children: <Widget>[
            /*
            Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Align(
                    alignment: Alignment.topLeft,
                    child: ListView.builder(
                        shrinkWrap: true,
                        physics: ClampingScrollPhysics(),
                        itemCount: document.info.stations.length,
                        itemBuilder: (context, index) {
                          return Text(
                            '${document.info.stations[index]}',
                            style: TextStyle(fontSize: 14, color: Colors.grey),
                          );
                        }))
            )

             */
          ]);

  Widget _buildDistance(Request document, BuildContext context) => ListTile(
      leading: Icon(Partnerum.walk, color: Theme.of(context).primaryColor),
      title: Text(Strings.pages.ordersPage.homeOrderDetailPage.timeToMetro.l(context)),
      /*
      subtitle: Text('${document.info.minutesToMetro}'));

       */
      subtitle: Text('min to metro'));
}
