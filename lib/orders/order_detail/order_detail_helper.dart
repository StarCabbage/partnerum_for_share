import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';
import 'package:micro_utils/data_helper.dart';
import 'package:db_partnerum/db_partnerum.dart';
import 'package:partnerum/head_module/gen_extensions/generated_str_extension.dart';
import 'package:partnerum/utils/brain_module.dart';

@DataHelper()
class OrderDetailHelper {
  RequestInformation document;
  var notStated = 'N/A';

  Future<Request> requestByID(String requestID) =>
      root.userData[mindClass.user.uid].requests[requestID].future;

  String formatDate(int timestamp) {
    return DateFormat.yMMMd('ru')
        .format(DateTime.fromMillisecondsSinceEpoch(timestamp));
  }

  String requestDatesRange(BuildContext context) {
    var firstDate = document?.datesRange?.first?.value;
    var lastDate = document?.datesRange?.last?.value;

    var title =
        '${(firstDate == null || lastDate == null || firstDate?.inMilliseconds == 0 || lastDate?.inMilliseconds == 0) ? Strings.pages.ordersPage.orderDetail.orderDetailHelper.newRequest.l(context) : formatDate(lastDate.inMilliseconds)}';

    // var title = '${(firstDate == 'null' || firstDate == null || lastDate == null) ? Strings.pages.ordersPage.orderDetail.orderDetailHelper.newRequest.l(context) : formatDate(firstDate.inMilliseconds)}' ' - ' + '${(lastDate == 'null' || lastDate == null) ? notStated : formatDate(lastDate.inMilliseconds)}';
    return title;
  }

  Text requestDescription(BuildContext context) {
    var descr = document?.description;
    return ((descr.value == '') || (descr?.value == null))
        ? Text(
            Strings.pages.ordersPage.orderDetail.orderDetailHelper.notStated
                .l(context),
            style: TextStyle(fontSize: 14, color: Colors.grey),
          )
        : Text(
            descr.value,
            style: TextStyle(fontSize: 14, color: Colors.grey),
          );
  }

  Text requestCommission(BuildContext context) {
    var commission = document?.commision;
    // return Text('%');
    return ((commission == null) || (commission.value == null))
        ? Text(Strings.pages.ordersPage.orderDetail.orderDetailHelper.notStated
            .l(context))
        : Text('${commission.value} %');
  }

  Text requestGuests(BuildContext context) {
    var guests = document?.guests;
    // return Text('%');
    return ((guests == null) || (guests.value == null))
        ? Text(Strings.pages.ordersPage.orderDetail.orderDetailHelper.notStated
            .l(context))
        : Text('${guests.value}');
  }

  Text requestCity(BuildContext context) {
    var city = document?.city;
    return ((city == null) || (city.value == null))
        ? Text(Strings.pages.ordersPage.orderDetail.orderDetailHelper.notStated
            .l(context))
        : Text('${city.value}');
  }

  Text requestRooms(BuildContext context) {
    var rooms = document?.rooms;
    return ((rooms == null) || (rooms?.value == null))
        ? Text(Strings.pages.ordersPage.orderDetail.orderDetailHelper.notStated
            .l(context))
        : Text('${rooms.value}');
  }

  Text requestTimeToMetro(BuildContext context) {
    var minutes = document?.timeToMetro;
    return ((minutes == null) ||
            (minutes?.value == null) ||
            (minutes.value.inMilliseconds == 0))
        ? Text(Strings.pages.ordersPage.orderDetail.orderDetailHelper.notStated
            .l(context))
        : Text(
            '${minutes.value.inMinutes} ${Strings.pages.ordersPage.orderDetail.orderDetailHelper.timeMeasure.l(context)}');
  }

  Text requestPrices(BuildContext context) {
    var startPrice = document?.priceRange?.startPosition;
    var endPrice = document?.priceRange?.endPosition;
    return ((startPrice == null) ||
            (startPrice.value == null) ||
            (endPrice == null) ||
            (endPrice.value == null))
        ? Text(Strings.pages.ordersPage.orderDetail.orderDetailHelper.noPrice
            .l(context))
        : Text(
            '${Strings.pages.ordersPage.orderDetail.orderDetailHelper.priceFrom.l(context)} ${startPrice.value} ${Strings.pages.ordersPage.orderDetail.orderDetailHelper.priceTo.l(context)} ${endPrice.value} ${Strings.pages.ordersPage.orderDetail.orderDetailHelper.priceMeasure.l(context)}',
          );
  }

  List requestStations() {
    var stations = document?.stations;
    var stationsList = (stations?.values == null)
        ? []
        : stations.values.map((e) => e.key).toList();
    return stationsList;
  }

  Future<List> loadHouseInformation(House house) async {
    var housePhotos = [];
    var houseInformation = house?.houseInformation;
    for (var photo in houseInformation?.photos?.values) {
      housePhotos.add(await root.imagesContainer[photo.value].future);
    }
    return housePhotos;
  }
}
