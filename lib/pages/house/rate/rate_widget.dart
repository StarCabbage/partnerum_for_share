import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:db_partnerum/db_partnerum.dart';
import 'package:flutter/material.dart' hide ExpansionTile;
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:partnerum/head_module/gen_extensions/generated_str_extension.dart';

// import 'package:partnerum/models/houses_model.dart';
import 'package:partnerum/models/user_info_model.dart';
import 'package:partnerum/utils/brain_module.dart';
import 'package:partnerum/utils/theme_constants.dart';
import 'package:partnerum/widgets/expansion_tile.dart';
import 'package:shimmer/shimmer.dart';

import 'rate_helper.dart';

class RateChip extends StatefulWidget {
  final String messageInstanceId;
  final bool isOwner;

  const RateChip({Key key, this.messageInstanceId, this.isOwner})
      : super(key: key);

  @override
  _RateChipState createState() => _RateChipState();
}

class _RateChipState extends State<RateChip> with RateHelper {
  StreamController streamController = StreamController();
  Stream reviewFlow;
  StreamSubscription reviewFlowSubscription;
  Review review;

  @override
  void initState() {
    reviewFlow = streamController.stream.asBroadcastStream();
    reviewFlowSubscription = reviewFlow.listen((event) {
      if (event is Review) review = event;
    });
    putData();
    super.initState();
  }

  @override
  void dispose() {
    reviewFlowSubscription.cancel();
    streamController.close();
    super.dispose();
  }

  putData() async {
    streamController.add('loading');
    // streamController.add(await Review.loadReview(widget.messageInstanceId));
    streamController.add(await offerReviewFuture(widget.messageInstanceId));
  }

  @override
  Widget build(BuildContext context) => StreamBuilder(
      stream: reviewFlow,
      builder: (context, snapshot) {
        return Stack(children: [
          if (snapshot.data == 'loading')
            Shimmer.fromColors(
                highlightColor: Colors.orange,
                baseColor: Colors.orangeAccent,
                child: ActionChip(
                    backgroundColor: Colors.orange,
                    padding: EdgeInsets.all(5),
                    onPressed: () async {},
                    label: Text(
                        Strings.pages.chatsPage.rateWidget.giveFeedback
                            .l(context),
                        style: TextStyle(color: Colors.white)))),
          Theme(
              data: Theme.of(context).copyWith(canvasColor: Colors.transparent),
              child: ActionChip(
                  backgroundColor: snapshot.data == 'loading'
                      ? Colors.transparent
                      : Colors.orange,
                  elevation: 0,
                  padding: EdgeInsets.all(5),
                  onPressed: () async {
                    if (snapshot.data == 'loading') {
                      final snackBar = SnackBar(
                          content: Text(Strings
                              .pages.chatsPage.rateWidget.waitForLoading
                              .l(context)));
                      Scaffold.of(context).showSnackBar(snackBar);
                      return;
                    }
                    var reload = await RateWidget.showRateWidgetDialog(
                        context,
                        RateWidget(
                          messageInstanceId: widget.messageInstanceId,
                          // review: review.myReview
                          isOwner: widget.isOwner,
                          review: widget.isOwner
                              ? review.ownerReview
                              : review.requestUserReview,
                          isCreateReview: true,
                        ));

                    if (reload) putData();
                  },
                  label: Text(
                      Strings.pages.chatsPage.rateWidget.giveFeedback
                          .l(context),
                      style: TextStyle(color: Colors.white))))
        ]);
      });
}

class RatedChip extends StatefulWidget {
  final String messageInstanceId;
  final bool isOwner;

  const RatedChip({Key key, this.messageInstanceId, this.isOwner})
      : super(key: key);

  @override
  _RatedChipState createState() => _RatedChipState();
}

class _RatedChipState extends State<RatedChip> with RateHelper {
  StreamController streamController = StreamController();
  Stream reviewFlow;
  StreamSubscription reviewFlowSubscription;
  Review review;

  @override
  void initState() {
    reviewFlow = streamController.stream.asBroadcastStream();
    reviewFlowSubscription = reviewFlow.listen((event) {
      if (event is Review) review = event;
    });
    putData();

    super.initState();
  }

  @override
  void dispose() {
    reviewFlowSubscription.cancel();
    streamController.close();
    super.dispose();
  }

  putData() async {
    streamController.add('loading');
    streamController.add(await offerReviewFuture(widget.messageInstanceId));
  }

  @override
  Widget build(BuildContext context) => StreamBuilder(
      stream: reviewFlow,
      builder: (context, snapshot) {
        // if ((review?.reviewToMe?.rating ?? -1) == -1) {
        if ((splitRatingAndReview(review?.ratingAndReview?.value) ?? -1) ==
            -1) {
          return Container(width: 0, height: 0);
        }
        return Stack(children: [
          if (snapshot.data == 'loading')
            Shimmer.fromColors(
                highlightColor: Colors.yellow,
                baseColor: Colors.yellowAccent,
                child: ActionChip(
                    backgroundColor: Colors.yellow,
                    padding: EdgeInsets.all(5),
                    onPressed: () async {},
                    label: Text(
                        '${Strings.pages.chatsPage.rateWidget.checkReview.l(context)}',
                        style: TextStyle(color: Colors.white)))),
          ActionChip(
              backgroundColor: snapshot.data == 'loading'
                  ? Colors.transparent
                  : Colors.yellow,
              elevation: 0,
              padding: EdgeInsets.all(5),
              onPressed: () async {
                if (snapshot.data == 'loading') {
                  final snackBar = SnackBar(
                      content: Text(Strings
                          .pages.chatsPage.rateWidget.waitForLoading
                          .l(context)));
                  Scaffold.of(context).showSnackBar(snackBar);
                  return;
                }
                var reload = await RateWidget.showRateWidgetDialog(
                    context,
                    RateWidget(
                      messageInstanceId: widget.messageInstanceId,
                      // review: review.reviewToMe
                      isOwner: widget.isOwner,
                      review: widget.isOwner
                          ? review.requestUserReview
                          : review.ownerReview,
                      isCreateReview: false,
                    ));
                if (reload) putData();
              },
              label: Text(
                  Strings.pages.chatsPage.rateWidget.checkReview.l(context),
                  style: TextStyle(color: Colors.black)))
        ]);
      });
}

class RateWidget extends StatefulWidget {
  final String messageInstanceId;
  final Review review;
  final bool isOwner;
  final bool isCreateReview;

  RateWidget(
      {Key key,
      this.isDialog = false,
      @required this.messageInstanceId,
      this.review,
      this.isOwner,
      this.isCreateReview})
      : super(key: key);

  static Widget showRateChip(String messageInstanceId, bool isOwner) {
    // return Container(width: 100, height: 200, color: Colors.green,);

    return RateChip(messageInstanceId: messageInstanceId, isOwner: isOwner);
  }

  static Widget showRatedChip(String messageInstanceId, bool isOwner) =>
      RatedChip(
        messageInstanceId: messageInstanceId,
        isOwner: isOwner,
      );

  static Future<bool> showRateWidgetDialog(
      BuildContext context, RateWidget rateWidget) async {
    return showDialog<bool>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        FlutterStatusbarcolor.setStatusBarColor(Colors.transparent);
        rateWidget.isDialog = true;
        return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(32.0)),
            child: rateWidget);
      },
    ).then((value) {
      FlutterStatusbarcolor.setStatusBarColor(Colors.white);
      print(value);
      return value;
    });
  }

  bool isDialog;

  @override
  _RateWidgetState createState() => _RateWidgetState();
}

class _RateWidgetState extends State<RateWidget>
    with TickerProviderStateMixin<RateWidget>, RateHelper {
  WidgetController<ExpansionTileState> _controller;
  FocusNode _reviewFocusNode;
  FocusNode _commentFocusNode;
  final bool _isUploading = false;

  @override
  void initState() {
    reviewEditingController = TextEditingController(
        text:
            (splitRatingAndReview(widget?.review?.ratingAndReview?.value)[1]) ??
                '');
    commentEditingController = TextEditingController(
        text: (widget?.review?.reviewAnswer?.value) ?? '');
    // _rating = widget?.review?.rating ?? 3;
    rating = 3;
    _controller = WidgetController<ExpansionTileState>();
    _reviewFocusNode = FocusNode();
    _commentFocusNode = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    reviewEditingController.dispose();
    _controller.dispose();
    _reviewFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // var date = DateTime.fromMillisecondsSinceEpoch(widget.review.timestamp.value.inMilliseconds);
    var minutesSinceCreation =
        Duration(milliseconds: DateTime.now().millisecondsSinceEpoch)
                .inMinutes -
            widget.review.timestamp.value.inMinutes;
    return Padding(
        padding: widget.isDialog
            ? const EdgeInsets.all(0)
            : const EdgeInsets.symmetric(vertical: 8.0),
        child: Card(
            margin: widget.isDialog ? const EdgeInsets.all(0) : null,
            clipBehavior: Clip.antiAlias,
            shape: widget.isDialog
                ? null
                : RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(16))),
            child: ExpansionTile(
                backgroundColor: Colors.transparent,
                tilePadding: EdgeInsets.zero,
                expansionController: _controller,
                initiallyExpanded:
                    // widget?.review?.reviewText?.isNotEmpty ?? false,
                    splitRatingAndReview(
                                widget?.review?.ratingAndReview?.value)[1]
                            ?.isNotEmpty ??
                        false,
                title: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Builder(
                            builder: (BuildContext context) {
                              return CircleAvatar(backgroundColor: Colors.grey);
                            },
                          )
                          /*
                          FutureBuilder(
                              future: mindClass.userInfo(widget.review.userId),
                              builder:
                                  (context, AsyncSnapshot<UserModel> snapshot) {
                                if (snapshot.hasData) {
                                  return CircleAvatar(
                                      backgroundColor: Colors.orange.shade100,
                                      backgroundImage:
                                          CachedNetworkImageProvider(
                                              snapshot.data.avatar));
                                }
                                return CircleAvatar(
                                    backgroundColor: Colors.grey);
                              })
*/
                          ),
                      Padding(
                          padding: const EdgeInsets.all(8.0).copyWith(top: 0),
                          child: Text(
                              // (widget?.review?.isMyReview ?? false)
                              ((!widget.isCreateReview) ||
                                      commentEditingController.text.isNotEmpty)
                                  ? Strings.pages.chatsPage.rateWidget.newReview
                                      .l(context)
                                  : '${Strings.pages.chatsPage.rateWidget.mark.l(context)} & ${Strings.pages.chatsPage.rateWidget.review.l(context)}',
                              //Rate & review
                              style: Theme.of(context).textTheme.headline6)),
                      Padding(
                          padding: const EdgeInsets.all(8.0).copyWith(top: 0),
                          child: Text(
                              // (widget?.review?.isMyReview ?? false)
                              (widget?.review?.authorId?.value ==
                                      mindClass.user.uid)
                                  ? Strings
                                      .pages.chatsPage.rateWidget.askForReview
                                      .l(context)
                                  : Strings
                                      .pages.chatsPage.rateWidget.askForAnswer
                                      .l(context),
                              //Share your experience to help others
                              style: Theme.of(context).textTheme.bodyText2)),
                      Padding(
                          padding:
                              const EdgeInsets.all(8.0).copyWith(bottom: 16),
                          child: RatingBar.builder(
                              ignoreGestures: (!widget.isCreateReview) ||
                                  commentEditingController.text.isNotEmpty ||
                                  (widget.review?.ratingAndReview?.value ?? '').isNotEmpty,
                              initialRating: rating,
                              minRating: 0,
                              direction: Axis.horizontal,
                              allowHalfRating: true,
                              itemCount: 5,
                              itemPadding:
                                  EdgeInsets.symmetric(horizontal: 4.0),
                              itemBuilder: (context, _) =>
                                  Icon(Icons.star, color: Colors.amber),
                              onRatingUpdate: (rate) {
                                rating = rate;
                                if (!_controller.r.isExpanded)
                                  _controller.r.handleTap();
                              },
                              glow: false))
                    ]),
                trailing: Container(width: 0, height: 0),
                afterChildren:
                    Column(mainAxisSize: MainAxisSize.min, children: [
                  AnimatedSize(
                      vsync: this,
                      duration: kThemeAnimationDuration,
                      child: SizedBox(
                          height: _isUploading ? 0 : null,
                          child: Column(
                            children: [
                                  Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    FlatButton(
                                        onPressed: () async {
                                          if (minutesSinceCreation < 180) {
                                            denyReview(widget.review);
                                          } else {
                                            Fluttertoast.showToast(
                                              textColor: Colors.white,
                                              backgroundColor: Colors.black54,
                                              msg: Strings.pages.chatsPage
                                                  .rateWidget.timeOutMessage
                                                  .l(context),
                                              toastLength: Toast.LENGTH_SHORT,
                                              gravity: ToastGravity.BOTTOM,
                                              timeInSecForIosWeb: 2,
                                            );
                                          }
                                          Navigator.pop(context, true);
                                        },
                                        // child: Text(minutesSinceCreation.toString())),
                                        child: Text(
                                          Strings.pages.chatsPage.rateWidget.argue
                                              .l(context),
                                          style: TextStyle(fontSize: 12),
                                        )),
                                    IconButton(
                                        onPressed: () {
                                          _controller.r.handleTap();
                                          _reviewFocusNode.requestFocus();
                                        },
                                        icon: Icon(Icons.create, size: 20)),
                                    FlatButton(
                                        onPressed: () async {
                                          // if (!widget.review.isMyReview) {
                                          //   await mindClass.makeReviewAnswer(
                                          //       widget.messageInstanceId,
                                          //       widget.review.userId,
                                          //       _commentEditingController.text);
                                          // } else {
                                          //   await mindClass.makeReview(
                                          //       widget.messageInstanceId,
                                          //       _reviewEditingController.text,
                                          //       _rating);
                                          // }
                                          // print('IsCreateReview: ${widget?.isCreateReview}');
                                          if (widget.isCreateReview) {
                                            uploadReview(widget.review);
                                          } else {
                                            answerToReview(widget.review);
                                          }
                                          Navigator.pop(context, true);
                                        },
                                        child: Text(
                                          Strings.pages.chatsPage.rateWidget.send
                                              .l(context),
                                          style: TextStyle(fontSize: 12),
                                        ))
                                  ]),
                              if (widget.isDialog)
                                Align(
                                  alignment: Alignment.centerRight,
                                  child: FlatButton(
                                      onPressed: () {
                                        Navigator.pop(context, false);
                                      },
                                      child: Text(
                                        Strings
                                            .pages.chatsPage.rateWidget.cancel
                                            .l(context),
                                        style: TextStyle(fontSize: 12),
                                      )),
                                )
                                ],
                          ))),
                  AnimatedSize(
                      vsync: this,
                      duration: kThemeAnimationDuration,
                      child: SizedBox(
                          height: _isUploading ? 3 : 0,
                          child: linearProgressIndicator))
                ]),
                children: [
                  TextFormField(
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText:
                            '${Strings.pages.chatsPage.rateWidget.reviewHintText.l(context)} ${!widget.isCreateReview} ${commentEditingController.text.isEmpty}'),
                    // hintText: widget?.review?.authorId?.value),
                    textAlign: TextAlign.center,
                    minLines: 1,
                    maxLines: null,
                    // readOnly: !(widget?.review?.isMyReview ?? false) ,
                    // readOnly: (widget?.review?.authorId?.value != mindClass.user.uid),
                    readOnly: (!widget.isCreateReview) ||
                        commentEditingController.text.isNotEmpty ||
                        (widget.review?.ratingAndReview?.value ?? '').isNotEmpty,
                    keyboardAppearance: Brightness.light,
                    controller: reviewEditingController,
                    focusNode: _reviewFocusNode,
                  ),
                  Divider(),
                  // if (commentEditingController.text.isNotEmpty ||
                  //     // !(widget?.review?.isMyReview ?? false)
                  //     !(widget?.review?.authorId?.value == mindClass.user.uid))
                  if (!((widget?.review?.reviewAnswer?.value ?? '') == '') || (!widget.isCreateReview))
                    TextFormField(
                        // readOnly: !(widget?.review?.isMyReview ?? false),
                        // readOnly: !(widget?.review?.authorId?.value == mindClass.user.uid),
                        readOnly: (widget.isCreateReview) ||
                            commentEditingController.text.isNotEmpty,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: Strings
                                .pages.chatsPage.rateWidget.answerHintText
                                .l(context)),
                        textAlign: TextAlign.center,
                        minLines: 1,
                        maxLines: null,
                        keyboardAppearance: Brightness.light,
                        controller: commentEditingController,
                        focusNode: _commentFocusNode)
                ])));
  }
}
