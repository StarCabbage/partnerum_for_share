import 'dart:async';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:db_partnerum/db_partnerum.dart' hide Position;
import 'package:partnerum/head_module/gen_extensions/generated_str_extension.dart';
import 'package:partnerum/pages/extra/get_city_name.dart';
import 'package:partnerum/pages/metro/choose_metro/choose_metro_page.dart';

import 'package:partnerum/utils/brain_module.dart';
import 'package:partnerum/utils/theme_constants.dart';
import 'package:partnerum/widgets/partnerum_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:partnerum/tools/app_tools.dart';
import 'house_create_helper.dart';

import 'package:geocoding/geocoding.dart' as Geocoding;
import 'package:location/location.dart' as GeoLocation;
import 'package:geolocator/geolocator.dart';

const kGoogleApiKey = '';

GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: kGoogleApiKey);

class HouseCreatePage extends StatefulWidget {
  final String houseId;
  final HouseInformation houseInformation;
  final List photosList;

  const HouseCreatePage(
      {Key key, this.houseId, this.houseInformation, this.photosList})
      : super(key: key);

  @override
  _HouseCreatePageState createState() => _HouseCreatePageState();
}

class _HouseCreatePageState extends State<HouseCreatePage>
    with HouseCreateHelper {
  Map<int, File> imagesMap = Map();
  bool circleLoading;

  String query = '';
  List<dynamic> metroStantionList = <dynamic>[];
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final addressScaffoldKey = GlobalKey<ScaffoldState>();
  Prediction p;
  var startPrice = 0;
  var endPrice = 0;
  List<String> stations = [];

  var fieldsEnable = true;

  // String roomsCount = '1';
  // int roomsValue = 1;

  @override
  void initState() {
    circleLoading = false;
    initHouse(widget.houseId, widget.houseInformation);
    // TODO: implement initState
    super.initState();
  }

  bool firstLoad = true;

  // List urlImagesHeaders = [];

  Widget MultiImagePickerList(
      {List<File> imageList = const [],
      VoidCallback Function(int position) removeNewImage}) {
    return Padding(
      padding: const EdgeInsets.only(left: 15.0, right: 15.0),
      child: ((imageList?.length ?? 0) == 0) && ((urlImages?.length ?? 0) == 0)
          ? Container()
          : SizedBox(
              height: 150.0,
              child: ListView.builder(
                  itemCount:
                      (imageList?.length ?? 0) + (urlImages?.length ?? 0),
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    if (index + 1 > imageList.length) {
                      return Padding(
                        padding: EdgeInsets.only(left: 3.0, right: 3.0),
                        child: Stack(
                          children: <Widget>[
                            Container(
                              width: 150.0,
                              height: 150.0,
                              decoration: BoxDecoration(
                                color: Colors.grey,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(15.0)),
                              ),
                            ),
                            Opacity(
                              opacity: .95,
                              child: Container(
                                width: 150.0,
                                height: 150.0,
                                decoration: BoxDecoration(
                                  color: Colors.grey.withAlpha(100),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15.0)),
                                ),
                                child: CachedNetworkImage(
                                  imageUrl: urlImages[index - imageList.length]
                                      .smallImg
                                      .url,
                                  httpHeaders:
                                      urlImages[index - imageList.length]
                                          .smallImg
                                          .syncHttpHeaders,
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: CircleAvatar(
                                backgroundColor: Colors.red[600],
                                child: IconButton(
                                  icon: Icon(
                                    Icons.clear,
                                    color: Colors.white,
                                  ),
                                  onPressed: () async {
                                    // await urlImages[index - imageList.length].usagePlacesAmount.set(2);
                                    downgradeUsagePlaces(
                                        urlImages[index - imageList.length],
                                        widget.houseId);
                                    setState(() {
                                      urlImages
                                          .removeAt(index - imageList.length);
                                    });
                                    //removeOldImage(index);
                                  },
                                ),
                              ),
                            )
                          ],
                        ),
                      );
                    }
                    return Padding(
                      padding: EdgeInsets.only(left: 3.0, right: 3.0),
                      child: Stack(
                        children: <Widget>[
                          Container(
                            width: 150.0,
                            height: 150.0,
                            decoration: BoxDecoration(
                              color: Colors.grey,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15.0)),
                            ),
                          ),
                          Opacity(
                            child: Container(
                              width: 150.0,
                              height: 150.0,
                              decoration: BoxDecoration(
                                  color: Colors.grey.withAlpha(100),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15.0)),
                                  image: DecorationImage(
                                      fit: BoxFit.cover,
                                      image: FileImage(imageList[index]))),
                            ),
                            opacity: .95,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: CircleAvatar(
                              backgroundColor: Colors.red[600],
                              child: IconButton(
                                icon: Icon(
                                  Icons.clear,
                                  color: Colors.white,
                                ),
                                onPressed: () {
                                  removeNewImage(index);
                                },
                              ),
                            ),
                          )
                        ],
                      ),
                    );
                  }),
            ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var document = widget.houseInformation;
    urlImages = widget?.photosList ?? [];
    return FutureBuilder(
        /*
        future: mindClass.userHouse(widget.houseId),

       */
        future: futureHouseInfo,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Container(
              color: Colors.white,
              alignment: FractionalOffset.center,
              child: Center(
                child: Text(
                    '${Strings.globalOnes.error.l(context)}: ${snapshot.error}'),
              ),
            );
          }
//          if (!snapshot.hasData)
//            return  Container(
//              color: Colors.white,
//              alignment: FractionalOffset.center,
//              child:  Center(
//                child: Theme.of(context).platform == TargetPlatform.iOS
//                    ?  CupertinoActivityIndicator()
//                    :  CircularProgressIndicator(),
//              ),
//            );
          if (snapshot.hasData) {
            document = snapshot.data;
          }

          if (document != null) {
            if (document != null) {
              if (firstLoad) {
                /*
                for (var photo in document?.photos?.values){
                  if (photo.showInRedactor.value != false){
                    urlImages.add(photo);
                  }
                }

                 */
                // urlImages = document?.photos?.values?.toList();
                // for (var e in document?.photos?.values){
                //   urlImages.add(e);
                // }
                // Uint8List tmpUint8List = Uint8List(2);
                // imageList = document?.photos?.values?.map((e) {
                //   urlImages.add(e?.smallImg?.url);
                //   // http.Response responseData = await http.get(U);
                //   // uint8list = responseData.bodyBytes;
                //   // return File.fromUri(tmpUint8List);
                //   return File.fromRawPath(e?.smallImg?.uint8list ?? tmpUint8List);
                // })?.toList();
                // imageList = [];
                offerCityNameController.text = document.city.value;
                offerAddressNameController.text = document.address.value;
                offerStartPriceController.text =
                    document.priceRange.startPosition.value;
                offerEndPriceController.text =
                    document.priceRange.endPosition.value;
                startPrice = int.parse(offerStartPriceController.text);
                endPrice = int.parse(offerEndPriceController.text);
                offerInfoController.text = document.description.value;
                offerMetroStationController.text = houseStations(document);
                roomsValue = document?.rooms?.value ?? 1;

                switch (roomsValue - 1) {
                  case 0:
                    roomsCount = '1';
                    break;
                  case 1:
                    roomsCount = '2';
                    break;
                  case 2:
                    roomsCount = '3';
                    break;
                  case 3:
                    roomsCount = '4';
                    break;
                  case 4:
                    roomsCount = Strings
                        .pages.housesPage.houseCreatePage.moreThan4
                        .l(context);
                    break;
                }
                //imageList = document.info.photoUrls;
                firstLoad = false;
              }
            }
          }
          stations = offerMetroStationController.text.split(', ');
          return Scaffold(
            key: _scaffoldKey,
            backgroundColor: Colors.white,
            body: ScrollConfiguration(
              behavior: noHighlightScrollBehavior(context),
              child: CustomScrollView(
                slivers: [
                  SliverAppBar(
                    pinned: true,
                    title: Text(widget.houseId.isEmpty
                        ? Strings.pages.housesPage.houseCreatePage.newFlat
                            .l(context)
                        : Strings.pages.housesPage.houseCreatePage.changeFlat
                            .l(context)),
                    centerTitle: false,
                    textTheme: TextTheme(
                        subtitle1: TextStyle(
                      color: Colors.black,
                      fontSize: 20.0,
                    )),
                    backgroundColor: Colors.white,
                  ),
                  SliverToBoxAdapter(
                    child: Form(
                      key: _formKey,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          if (circleLoading) loadingChats(context),
                          _buildAddBtn(),
                          Container(
                            margin: const EdgeInsets.symmetric(vertical: 10.0),
                            child: MultiImagePickerList(
                              imageList: imageList ?? [],
                              // urlImages: urlImages,
                              removeNewImage: (index) {
                                removeImage(index);
                              },
                            ),
                          ),
                          _buildCity(),
                          _buildMetro(),
                          _buildDistance(),
                          _buildAddress(context),
                          _buildPrice(),
                          _buildRooms(),
                          _buildInfo(),
                          SizedBox(
                            height: 80,
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            floatingActionButtonLocation:
                FloatingActionButtonLocation.centerFloat,
            floatingActionButton: FloatingActionButton.extended(
              //width: MediaQuery.of(context).size.width,

              backgroundColor: Theme.of(context).primaryColor,
              onPressed: () async {
                if (!offerAddressNameController.text
                    .contains(offerCityNameController.text)) {
                  _scaffoldKey.currentState.showSnackBar(SnackBar(
                      content: Text('Город адреса не совпадает с указанным')));
                } else {
                  if (startPrice > endPrice) {
                    _scaffoldKey.currentState.showSnackBar(SnackBar(
                        content: Text(Strings
                            .pages.housesPage.houseCreatePage.priceSnackBar
                            .l(context))));
                  } else {
                    // return;
                    if (_formKey.currentState.validate() &&
                        (imageList.isNotEmpty || urlImages.isNotEmpty)) {
                      setState(() {
                        circleLoading = true;
                      });
                      await uploadHouse();

                      /*
                  var result = await addNewOffers(document, context);
                  if (!result) {
                    return;
                  }

                   */
                      Navigator.of(context).pop();

                      Fluttertoast.showToast(
                        textColor: Colors.white,
                        backgroundColor: Colors.black54,
                        msg: Strings
                            .pages.housesPage.houseCreatePage.flatAddedMessage
                            .l(context),
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.BOTTOM,
                        timeInSecForIosWeb: 1,
                      );
                      // uploadHouse();
                      mindClass.updateEvent('updateHousesPage');
                    } else {
                      _scaffoldKey.currentState.showSnackBar(SnackBar(
                          content: Text(Strings
                              .pages.housesPage.houseCreatePage.notEnoughInfo
                              .l(context))));
                    }
                  }
                }
              },
              label: Text(widget.houseId.isEmpty
                  ? Strings.pages.housesPage.houseCreatePage.addFlat
                      .l(context)
                      .toUpperCase()
                  : Strings.pages.housesPage.houseCreatePage.changeFlat
                      .l(context)
                      .toUpperCase()),
            ),
          );
        });
  }

  Widget _buildInfo() {
    return ListTile(
      leading: Icon(
        Partnerum.info,
        color: blackColor,
      ),
      title: TextFormField(
        enabled: fieldsEnable,
        autofocus: false,
        onFieldSubmitted: (term) {
          SystemChannels.textInput.invokeMethod('TextInput.hide');
        },
        maxLines: null,
        decoration: InputDecoration(
          hintText: Strings
              .pages.housesPage.houseCreatePage.infoWidget.addExtraInfo
              .l(context),
          labelText: Strings
              .pages.housesPage.houseCreatePage.infoWidget.extraInfo
              .l(context),
        ),
        controller: offerInfoController,
        keyboardType: TextInputType.text,
        onChanged: (text) {},
      ),
    );
  }

  Widget _buildAddBtn() {
    return ListTile(
      leading: Container(
        width: 16,
        child: Icon(
          Icons.image,
          color: blackColor,
        ),
      ),
      title: RaisedButton(
          color: Colors.white,
          onPressed: !fieldsEnable ? null : () => pickImage(),
          child: Text(
            Strings.pages.housesPage.houseCreatePage.addPhoto.l(context),
            // style: TextStyle(color: Colors.black),
          )),
    );
  }

  pickImage() async {
    var file = await ImagePicker.pickImage(source: ImageSource.gallery);

    if (file != null) {
      //imagesMap[imagesMap.length] = file;
      var imageFile = <File>[];
      imageFile.add(file);
      //imageList =  List.from(imageFile);
      if (imageList == null) {
        imageList = List.from(imageFile, growable: true);
      } else {
        for (var s = 0, l = imageFile.length; s < l; s++) {
          imageList.add(file);
        }
      }
      setState(() {});
    }
  }

  removeImage(int index) async {
    //imagesMap.remove(index);
    imageList.removeAt(index);
    setState(() {});
  }

  Widget _buildRooms() {
    return IgnorePointer(
      ignoring: !fieldsEnable,
      child: ExpansionTile(
        leading: Icon(
          Partnerum.guest,
          color: blackColor,
        ),
        title: Text(
          '${Strings.pages.housesPage.houseCreatePage.roomsAmount.l(context)} - $roomsCount',
          style: TextStyle(fontSize: 16, color: Colors.black),
        ),
        trailing: Icon(
          Icons.arrow_drop_down,
          color: blackColor,
        ),
        children: <Widget>[
          Center(
            child: SizedBox(
              height: 80.0,
              child: ListView(
                physics: ClampingScrollPhysics(),
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Radio(
                        activeColor: Theme.of(context).primaryColor,
                        value: 1,
                        groupValue: roomsValue,
                        onChanged: _handleGuestsCountValueChange,
                      ),
                      Text('1'),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Radio(
                        activeColor: Theme.of(context).primaryColor,
                        value: 2,
                        groupValue: roomsValue,
                        onChanged: _handleGuestsCountValueChange,
                      ),
                      Text('2'),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Radio(
                        activeColor: Theme.of(context).primaryColor,
                        value: 3,
                        groupValue: roomsValue,
                        onChanged: _handleGuestsCountValueChange,
                      ),
                      Text('3'),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Radio(
                        activeColor: Theme.of(context).primaryColor,
                        value: 4,
                        groupValue: roomsValue,
                        onChanged: _handleGuestsCountValueChange,
                      ),
                      Text('4'),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Radio(
                        activeColor: Theme.of(context).primaryColor,
                        value: 5,
                        groupValue: roomsValue,
                        onChanged: _handleGuestsCountValueChange,
                      ),
                      Text(Strings.pages.housesPage.houseCreatePage.moreThan4
                          .l(context)),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _handleGuestsCountValueChange(int value) {
    setRoomsNumber(value);
    setState(() {
      roomsValue = value;

      switch (roomsValue - 1) {
        case 0:
          roomsCount = '1';
          break;
        case 1:
          roomsCount = '2';
          break;
        case 2:
          roomsCount = '3';
          break;
        case 3:
          roomsCount = '4';
          break;
        case 4:
          roomsCount =
              Strings.pages.housesPage.houseCreatePage.moreThan4.l(context);
          break;
      }
    });
  }

  Widget _buildPrice() {
    return ListTile(
      leading: Icon(
        Partnerum.ruble,
        color: blackColor,
      ),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Flexible(child: _buildStartPrice()),
          Flexible(child: _buildEndPrice()),
        ],
      ),
    );
  }

  Widget _buildStartPrice() {
    return TextFormField(
      enabled: fieldsEnable,
      autofocus: false,
      keyboardType: TextInputType.number,
      inputFormatters: [
        WhitelistingTextInputFormatter(
          RegExp(r'^[()\d -]{1,15}$'),
        )
      ],
      controller: offerStartPriceController,
      decoration: InputDecoration(
        labelText:
            Strings.pages.housesPage.houseCreatePage.priceFrom.l(context),
      ),
      validator: (value) {
        if (value.isEmpty) {
          return Strings.pages.housesPage.houseCreatePage.notEmptyPriceField
              .l(context);
        }
        return null;
      },
      onChanged: (val) {
        _formKey.currentState.validate();
        setState(() {
          startPrice = int.parse(val);
        });
        // setStartPrice(int.parse(val));
      },
      onSaved: (value) {
        setStartPrice(int.parse(value));
        setState(() {
          startPrice = int.parse(value);
        });
      },
    );
  }

  Widget _buildEndPrice() {
    return Padding(
      padding: const EdgeInsets.only(left: 16.0),
      child: TextFormField(
        enabled: fieldsEnable,
        autofocus: false,
        keyboardType: TextInputType.number,
        inputFormatters: [
          WhitelistingTextInputFormatter(
            RegExp(r'^[()\d -]{1,15}$'),
          )
        ],
        controller: offerEndPriceController,
        decoration: InputDecoration(
//       icon: Icon(MyIcons.rouble),
          labelText:
              Strings.pages.housesPage.houseCreatePage.priceTo.l(context),
        ),
        validator: (value) {
          if (value.isEmpty) {
            return Strings.pages.housesPage.houseCreatePage.notEmptyPriceField
                .l(context);
          }
          return null;
        },
        onChanged: (val) {
          _formKey.currentState.validate();
          // setEndPrice(int.parse(val));
          setState(() {
            endPrice = int.parse(val);
          });
        },
        onSaved: (value) {
          setEndPrice(int.parse(value));
          setState(() {
            endPrice = int.parse(value);
          });
        },
      ),
    );
  }

  Widget _buildCity() {
    return ListTile(
      leading: Icon(
        Partnerum.city,
        color: blackColor,
      ),
      title: InkWell(
        child: IgnorePointer(
          child: TextFormField(
            autofocus: false,
            onFieldSubmitted: (term) {
              SystemChannels.textInput.invokeMethod('TextInput.hide');
            },
            decoration: InputDecoration(
              hintText:
                  Strings.pages.housesPage.houseCreatePage.city.l(context),
            ),
            controller: offerCityNameController,
            validator: (value) {
              if (value.isEmpty) {
                return Strings.pages.housesPage.houseCreatePage.necessarily
                    .l(context);
              }
              return null;
            },
          ),
        ),
      ),
      onTap: !fieldsEnable
          ? null
          : () async {
              var city = await Navigator.push(
                context,
                CupertinoPageRoute(
                  builder: (context) => GetCityName(),
                ),
              );

              if (city != null && city.isNotEmpty) {
                if (city != offerCityNameController.text) {
                  offerMetroStationController.clear();
                  if (!offerAddressNameController.text.contains(city)) {
                    offerAddressNameController.clear();
                  }
                }
                offerCityNameController.text = city;
              }

              // setCity(city);
              _formKey.currentState.validate();
              setState(() {});
            },
    );
  }

  Widget _buildMetro() {
    if (offerMetroStationController.text.isEmpty) {
      if (offerCityNameController.text == '') return Container();
      if (!mindClass.cityMetro.containsKey(offerCityNameController.text)) {
        return Container();
      }
    }
    return ListTile(
      leading: Icon(
        Partnerum.metro,
        color: blackColor,
      ),
      title: InkWell(
        child: IgnorePointer(
          child: TextFormField(
            maxLines: null,
            autofocus: false,
            onFieldSubmitted: (term) {
              SystemChannels.textInput.invokeMethod('TextInput.hide');
            },
            decoration: InputDecoration(
              hintText:
                  Strings.pages.housesPage.houseCreatePage.metro.l(context),
            ),
            controller: offerMetroStationController,
            validator: (value) {
              if (value.isEmpty) {
                return Strings.pages.housesPage.houseCreatePage.necessarily
                    .l(context);
              }
              return null;
            },
          ),
        ),
      ),
      onTap: () async {
        var _stations = await Navigator.push(
          context,
          CupertinoPageRoute(
            // builder: (context) => GetMetroName(
            //   city: offerCityNameController.text,
            //   stations: stations.toList(),
            // ),
            builder: (context) => ChooseMetro(
              city: offerCityNameController.text,
              stations: stations.toSet(),
            ),
          ),
        );
        if (_stations == null) return;
        if (_stations.length > 0) stations = _stations;
/*
        offerMetroStationController.text = 'Метро (${stations.join(", ")})';

 */
        offerMetroStationController.text = stations.join(', ');

        // offerMetroStationController.text = stations.join(', ');
        _formKey.currentState.validate();
      },
    );
  }

  Widget _buildDistance() {
    return offerCityNameController.text.isEmpty
        ? Container()
        : ExpansionTile(
            leading: Icon(Partnerum.walk, color: blackColor),
            title: Text(
                '${Strings.pages.housesPage.houseCreatePage.timeToMetro.l(context)} - $timeValue ${Strings.pages.housesPage.houseCreatePage.timeMeasure.l(context)}',
                style: TextStyle(fontSize: 16, color: Colors.black)),
            trailing: Icon(Icons.arrow_drop_down, color: blackColor),
            children: <Widget>[
                Center(
                    child: SizedBox(
                        height: 80.0,
                        child: ListView(
                            physics: ClampingScrollPhysics(),
                            shrinkWrap: true,
                            scrollDirection: Axis.horizontal,
                            children: <Widget>[
                              Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Radio(
                                        activeColor:
                                            Theme.of(context).primaryColor,
                                        value: 5,
                                        groupValue: timeValue,
                                        onChanged: _handleTimeValueChange),
                                    Text('5')
                                  ]),
                              Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Radio(
                                        activeColor:
                                            Theme.of(context).primaryColor,
                                        value: 10,
                                        groupValue: timeValue,
                                        onChanged: _handleTimeValueChange),
                                    Text('10')
                                  ]),
                              Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Radio(
                                        activeColor:
                                            Theme.of(context).primaryColor,
                                        value: 15,
                                        groupValue: timeValue,
                                        onChanged: _handleTimeValueChange),
                                    Text('15')
                                  ]),
                              Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Radio(
                                        activeColor:
                                            Theme.of(context).primaryColor,
                                        value: 20,
                                        groupValue: timeValue,
                                        onChanged: _handleTimeValueChange),
                                    Text('20')
                                  ]),
                              Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Radio(
                                        activeColor:
                                            Theme.of(context).primaryColor,
                                        value: 25,
                                        groupValue: timeValue,
                                        onChanged: _handleTimeValueChange),
                                    Text('25')
                                  ])
                            ])))
              ]);
  }

  void _handleTimeValueChange(int value) {
    setState(() {
      timeValue = value;
      // _distanceTime = _timeValue.toString();
    });
  }

  Widget _buildAddress(BuildContext buildContext) {
    return ListTile(
        leading: Icon(
          Partnerum.address,
          color: blackColor,
        ),
        title: InkWell(
            onTap: !fieldsEnable
                ? null
                : () async {
                    await _getAddress(buildContext);
                    _formKey.currentState.validate();
                    setState(() {});
                  },
            child: IgnorePointer(
                child: TextFormField(
                    autofocus: false,
                    decoration: InputDecoration(
                      hintText: Strings.pages.housesPage.houseCreatePage.address
                          .l(context),
                    ),
                    controller: offerAddressNameController,
                    validator: (value) {
                      if (value.isEmpty) {
                        return Strings
                            .pages.housesPage.houseCreatePage.necessarily
                            .l(context);
                      }
                      return null;
                    },
                    onFieldSubmitted: (term) {
                      SystemChannels.textInput.invokeMethod('TextInput.hide');
                    }))));
  }

  @override
  Future<List<String>> uploadProductImages(
      {List<File> imageList, String docID}) async {
    // TODO: implement uploadProductImages
    var imagesUrl = <String>[];

    try {
      for (var s = 0; s < imageList.length; s++) {}
    } on PlatformException catch (e) {
      imagesUrl.add('error');
    }
    return imagesUrl;
  }

  Future<bool> addNewOffers(document, BuildContext context) async {
    if (imageList.isEmpty && urlImages.isEmpty) {
      showSnackBar(
          Strings.pages.housesPage.houseCreatePage.uploadPhotoSnackBar
              .l(context),
          _scaffoldKey);
      return false;
    }

    if (_formKey.currentState.validate()) {
      displayProgressDialog(context);

      var houseId = widget.houseId;
      /*
      if (houseId.isEmpty) houseId = mindClass.getUserHouseId();

       */
      var imagesURL =
          await uploadProductImages(docID: houseId, imageList: imageList);

      if (imagesURL.contains('error')) {
        closeProgressDialog(context);
        showSnackBar(Strings.globalOnes.upoadError.l(context), _scaffoldKey);
        return false;
      }

      //bool result =
      //    await appMethod.updateProductImages(docID: offerId, data: imagesURL);

      var imagesEndList = [];
      imagesEndList.addAll(imagesURL);
      imagesEndList.addAll(urlImages);
      /*
      await mindClass.writeUserHouse(
          homeId: houseId,
          city: _offerCityNameController.text,
          rooms: roomsValue.toString(),
          priceRange: [
            _offerStartPriceController.text,
            _offerEndPriceController.text
          ],
          latLon: latLon.isNotEmpty ? latLon : null,
          photoUrls: imagesEndList,
          //imagesURL,
          address: _offerAddressNameController.text,
          stations: stations,
          extraInformation: _offerInfoController.text,
          minutesToMetro: _timeValue.toString());
*/
      closeProgressDialog(context);
      //resetEverything();
      //showSnackBar('Квартира добавлена', _scaffoldKey);
      Fluttertoast.showToast(
        textColor: Colors.white,
        backgroundColor: Colors.black54,
        msg: Strings.pages.housesPage.houseCreatePage.flatAddedMessage
            .l(context),
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
      );
      mindClass.updateEvent('updateHousesPage');
      return true;
      //Navigator.of(context).pop();
//      } else {
//        closeProgressDialog(context);
//        showSnackBar('Ошибка. Мы уже работаем над ней', scaffoldKey);
//      }
    }
  }

  Future<void> _getAddress(BuildContext context) async {
    p = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (_) => PlacesAutocompleteCustomPage(
                  context: context,
                  apiKey: kGoogleApiKey,
                  onError: _onError,
                  // hint: 'Введите адрес',
                  hint: Strings.pages.housesPage.houseCreatePage.needAddress
                      .l(context),
                  mode: Mode.fullscreen,
                  logo: Container(),
                  language: 'ru',
                  components: [Component(Component.country, 'ru')],
                  types: [],
                  strictbounds: false,
                  startText: offerAddressNameController.text,
                )));

    await displayPrediction(p, addressScaffoldKey.currentState);
  }

  String latLon = '';

  Future addresses() async {
    final places = GoogleMapsPlaces(apiKey: kGoogleApiKey);
    PlacesSearchResponse response =
        await places.searchByText('123 Main Street');
  }

  Future displayPrediction(Prediction p, ScaffoldState scaffold) async {
    if (p != null) {
      PlacesDetailsResponse detail =
          await _places.getDetailsByPlaceId(p.placeId);
      final lat = detail.result.geometry.location.lat;
      final lng = detail.result.geometry.location.lng;
      latLon = '$lat,$lng';
      offerAddressNameController.text = p.description;
    }
  }

  void _onError(PlacesAutocompleteResponse response) {
    addressScaffoldKey.currentState.showSnackBar(
      SnackBar(content: Text(response.errorMessage)),
    );
  }

  Widget loadingChats(context) {
    return Padding(
      padding: const EdgeInsets.only(top: 12.0, bottom: 16),
      child: Container(
          alignment: FractionalOffset.center,
          child: Center(
              child: Theme.of(context).platform == TargetPlatform.iOS
                  ? CupertinoActivityIndicator()
                  : linearProgressIndicator)),
    );
  }
}

class PlacesAutocompleteCustomPage extends StatefulWidget {
  final BuildContext context;
  final String apiKey;
  final Mode mode;
  final String hint;
  final BorderRadius overlayBorderRadius;
  final num offset;
  final Location location;
  final num radius;
  final String language;
  final String sessionToken;
  final List<String> types;
  final List<Component> components;
  final bool strictbounds;
  final String region;
  final Widget logo;
  final ValueChanged<PlacesAutocompleteResponse> onError;
  final String proxyBaseUrl;
  final String startText;

  const PlacesAutocompleteCustomPage(
      {Key key,
      @required this.context,
      @required this.apiKey,
      this.mode,
      this.hint,
      this.overlayBorderRadius,
      this.offset,
      this.location,
      this.radius,
      this.language,
      this.sessionToken,
      this.types,
      this.components,
      this.strictbounds,
      this.region,
      this.logo,
      this.onError,
      this.proxyBaseUrl,
      this.startText})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _PlacesAutocompleteCustomPageState();
}

class _PlacesAutocompleteCustomPageState
    extends State<PlacesAutocompleteCustomPage> {
  String autoText;
  Position currentPosition;
  String currentAddress;

  @override
  initState() {
    autoText = widget.startText;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PlacesAutocompleteWidget(
        key: ValueKey(autoText),
        apiKey: widget.apiKey,
        mode: widget.mode,
        overlayBorderRadius: widget.overlayBorderRadius,
        language: widget.language,
        sessionToken: widget.sessionToken,
        components: widget.components,
        types: widget.types,
        location: widget.location,
        radius: widget.radius,
        strictbounds: widget.strictbounds,
        region: widget.region,
        offset: widget.offset,
        hint: widget.hint,
        // logo: logo,
        logo: widget.logo,
        onError: widget.onError,
        proxyBaseUrl: widget.proxyBaseUrl,
        startText: autoText,
        // startText: autoText ?? widget.startText,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          var address = await getUserLocation();

          currentAddress = addressCollector(address);

          setState(() {
            autoText = currentAddress;
          });
        },
        child: Icon(
          Icons.not_listed_location_sharp,
          size: 50,
          color: Colors.redAccent,
        ),
      ),
    );
  }

  Future<Geocoding.Placemark> getUserLocation() async {
    //call this async method from whereever you need
    GeoLocation.LocationData currentLocation;
    GeoLocation.LocationData myLocation;
    String error;
    var location = GeoLocation.Location();
    try {
      myLocation = await location.getLocation();
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        error = 'please grant permission';
        print(error);
      }
      if (e.code == 'PERMISSION_DENIED_NEVER_ASK') {
        error = 'permission denied- please enable it from app settings';
        print(error);
      }
      myLocation = null;
    }
    currentLocation = myLocation;
    final addresses = await Geocoding.placemarkFromCoordinates(
        myLocation.latitude, myLocation.longitude);
    // var addresses = await Geocoding.placemarkFromCoordinates(coordinates);
    var first = addresses.first;
    return first;
  }

  String addressCollector(Geocoding.Placemark address) {
    var stringAddress = '';
    var needComma = false;
    if (address.thoroughfare != '') {
      if (needComma) {
        stringAddress += ', ';
      }
      stringAddress += address.thoroughfare;
      needComma = true;
    }

    if (address.subThoroughfare != '') {
      if (needComma) {
        stringAddress += ', ';
      }
      stringAddress += address.subThoroughfare;
      needComma = true;
    }

    if (address.subAdministrativeArea != '') {
      if (needComma) {
        stringAddress += ', ';
      }
      stringAddress += address.subAdministrativeArea;
      needComma = true;
    }

    if (address.administrativeArea != '') {
      if (needComma) {
        stringAddress += ', ';
      }
      stringAddress += address.administrativeArea;
      needComma = true;
    }

    if (address.country != '') {
      if (needComma) {
        stringAddress += ', ';
      }
      stringAddress += address.country;
      needComma = true;
    }
    // stringAddress += (address.thoroughfare != '' ? ', ${address.thoroughfare}' : '');
    // stringAddress += (address.subThoroughfare != '' ? ', ${address.subThoroughfare}' : '');
    // stringAddress += (address.subAdministrativeArea != '' ? ', ${address.subAdministrativeArea}' : '');
    // stringAddress += (address.administrativeArea != '' ? ', ${address.administrativeArea}' : '');
    // stringAddress += (address.country != '' ? ', ${address.country}' : '');
    return stringAddress;
  }
}
