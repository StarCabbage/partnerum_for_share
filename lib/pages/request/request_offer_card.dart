import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';
import 'dart:math' as math;
import 'package:db_partnerum/db_partnerum.dart';
import 'package:intl/intl.dart';

import 'package:after_layout/after_layout.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:diffutil_sliverlist/diffutil_sliverlist.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart' hide ExpansionTile;
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:partnerum/head_module/gen_extensions/generated_str_extension.dart';
import 'package:partnerum/models/requests_model.dart';

import 'package:partnerum/models/user_info_model.dart';
import 'package:partnerum/pages/house/house_details/house_details_page.dart';
import 'package:partnerum/orders/orders/orders_page.dart';
import 'package:partnerum/pages/request/share.dart';
import 'package:partnerum/utils/brain_module.dart';
import 'package:partnerum/utils/theme_constants.dart';
import 'package:partnerum/widgets/user_title/user_tile.dart';

import 'package:scroll_to_index/scroll_to_index.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import '../chat/messages/messages_page.dart';
import 'request_create/request_create_page.dart';
import 'suitable_apartments/suitable_apartments_page.dart';
import 'request_offer_card_new.dart';

class RequestOfferCard extends StatelessWidget {
  final String id;
  final bool isReady;
  final list;
  final Function show;
  final Future future;
  final HouseInformation houseInformation;
  final String messId;
  final String rType;
  final Function close;
  final bool newStyle;

  const RequestOfferCard({Key key,
    this.id,
    this.isReady = false,
    this.list = const [],
    this.show,
    this.future,
    this.messId,
    this.rType,
    this.houseInformation,
    this.close,
    this.newStyle = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (newStyle) {
      return RequestOfferCardNew(
          id: id,
          isReady: isReady,
          list: list,
          show: show,
          future: future,
          messId: messId,
          rType: rType,
          houseInformation: houseInformation,
          close: close);
    }
    var idSplit;
    String type;
    String messageInstanceId;
    if (future == null) {
      idSplit = id?.split(';');
      type = '';
      messageInstanceId = '';
      if (list.length > 0) {
        type = list[3];
        messageInstanceId = list[2];
      }
    } else {
      messageInstanceId = messId;
      type = rType;
    }

    //if(idSplit[0] == mindClass.user.uid) return Container();
    /*
    return FutureBuilder(
        future: future ??
            (isReady
                ? mindClass.userHouseByMessageInstance(list[2])
                : mindClass.userHouse(idSplit[1], idSplit[0])),
        builder: (BuildContext context, snapShot) {
          if (snapShot.hasData) {
            House house = snapShot.data;
            //if(house.houseId.isEmpty) return Container();
            return Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20))),
                    child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              CupertinoPageRoute(
                                  builder: (context) =>
                                      OfferDetailsPage(
                                          edit: false, house: house)
                              )
                          );
                        },
                        child: AnimatedSwitcher(
                            duration: Duration(milliseconds: 300),
                            child: Padding(
                                key: Key(id),
                                padding: const EdgeInsets.only(
                                    left: 16.0, top: 8, right: 8, bottom: 8),
                                child: Column(
                                    crossAxisAlignment: CrossAxisAlignment
                                        .start,
                                    children: <Widget>[
                                      Text(house.info.address,
                                          style:
                                          TextStyle(color: Colors.black,
                                              fontSize: 22)),
                                      Padding(
                                          padding: const EdgeInsets.only(
                                              top: 12.0),
                                          child: Column(
                                              children: <Widget>[
                                                Align(
                                                    alignment: Alignment
                                                        .centerLeft,
                                                    child: Text(
                                                        house.info.stations
                                                            .join(', '),
                                                        maxLines: 3,
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        style: TextStyle(
                                                            color: Colors.grey))
                                                ),
                                                Align(
                                                    alignment: Alignment
                                                        .centerLeft,
                                                    child: Text(
                                                        'От ${house.info
                                                            .priceRange[0]} до ${house
                                                            .info
                                                            .priceRange[1]} руб/сут',
                                                        style: TextStyle(
                                                            color: Colors.grey))
                                                )
                                              ]
                                          )
                                      ),
                                      house.info.extraInformation == 'yours'
                                          ? Container()
                                          : Padding(
                                          padding: const EdgeInsets.only(
                                              top: 16.0),
                                          child: Container(
                                              width: double.maxFinite,
                                              child: Wrap(
                                                  alignment: WrapAlignment.end,
                                                  children: <Widget>[
                                                    true //TODO
                                                        ? FlatButton(
                                                        padding: EdgeInsets.all(
                                                            5),
                                                        onPressed: () async {
                                                          await share(
                                                              house.info
                                                                  .toExportableJson(),
                                                              house.info
                                                                  .photoUrls,
                                                              context);
                                                        },
                                                        child: Icon(Icons.share)
                                                    )
                                                        : Container(),
                                                    true //TODO
                                                        ? FlatButton(
                                                        padding: EdgeInsets.all(
                                                            5),
                                                        onPressed: () {
                                                          Navigator.push(
                                                              context,
                                                              CupertinoPageRoute(
                                                                  builder: (
                                                                      context) =>
                                                                      Messages(
                                                                          messageInstanceId:
                                                                          messageInstanceId,
                                                                          noInfo: true
                                                                      )
                                                              )
                                                          );
                                                        },
                                                        child: Text(
                                                            'Чат',
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .black,
                                                                fontSize: 16)
                                                        )
                                                    )
                                                        : Container(),
                                                    ListTile(
                                                        title: Align(
                                                            alignment: Alignment
                                                                .centerRight,
                                                            child: show == null
                                                                ? Text(
                                                                'Закрыть окно',
                                                                style: Theme
                                                                    .of(context)
                                                                    .textTheme
                                                                    .button
                                                                    .merge(
                                                                    TextStyle(
                                                                        color:
                                                                        Colors
                                                                            .orange,
                                                                        fontSize: 16)))
                                                                : Text(
                                                                isReady
                                                                    ? 'Отменить заявку'
                                                                    : Strings
                                                                    .pages
                                                                    .suitableApartmentsPage
                                                                    .makeOfferText
                                                                    .l(context),
                                                                style: Theme
                                                                    .of(context)
                                                                    .textTheme
                                                                    .button
                                                                    .merge(
                                                                    TextStyle(
                                                                        color: Colors
                                                                            .orange,
                                                                        fontSize: 16)))
                                                        ),
                                                        onTap: () {
                                                          if (show != null) {
                                                            show(idSplit);
                                                          } else {
                                                            close();
                                                          }
                                                        }
                                                    )
                                                  ]
                                              )
                                          )
                                      )
                                    ]
                                )
                            )
                        )
                    )
                )
            );
          }
          return Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                  elevation: 3,
                  child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                      child: AnimatedSwitcher(
                          duration: Duration(milliseconds: 1000),
                          child: Container(
                              key: Key(messageInstanceId + 'shimmer'),
                              child: getShimmer(context)
                          )
                      )
                  )
              )
          );
        });

     */
  }
}