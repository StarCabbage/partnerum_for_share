import 'dart:async';
import 'package:micro_utils/data_helper.dart';
import 'package:db_partnerum/db_partnerum.dart';
import 'package:partnerum/utils/brain_module.dart';

@DataHelper()
class RequestsHelper {
  StreamController manualUpdate = StreamController();
  Stream manualUpdateStream;
  Stream houses;
  List<Request> lastHousesList;
  // bool archive = false;
  List<Request> requests;

  initStreams() {
    manualUpdateStream = manualUpdate.stream.asBroadcastStream();
  }

  Stream requestsLoader() {
    // return root.userData[mindClass.user.uid].requests.stream.map((event) => event?.values?.toList() ?? []);
    return root.userData[mindClass.user.uid].requests.stream.map((event) => event.values.toList());
  }

  String requestIdByIndex(int index) {
    return requests[index].key;
  }

  Future<void> archiveRequest(int index) async {
    var isArchived = requests[index].requestInformation.isArchived.value;
    if ((isArchived != false) && (isArchived != true)) {
      isArchived = false;
    }
    await requests[index].requestInformation.isArchived.set(!isArchived);
  }

  // Future<void> archiveRequestByObject(Request request) async {
  //   var isArchived = request.requestInformation.isArchived.value;
  //   if ((isArchived != false) && (isArchived != true)) {
  //     isArchived = false;
  //   }
  //   await request.requestInformation.isArchived.set(!isArchived);
  // }

  bool isArchived(int index) {
    return requests[index].requestInformation.isArchived.value;
  }
}
