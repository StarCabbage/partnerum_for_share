import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';
import 'dart:math' as math;
import 'package:db_partnerum/db_partnerum.dart';
import 'package:intl/intl.dart';

import 'package:after_layout/after_layout.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:diffutil_sliverlist/diffutil_sliverlist.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart' hide ExpansionTile;
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:partnerum/head_module/gen_extensions/generated_str_extension.dart';
import 'package:partnerum/pages/house/house_details/house_details_page.dart';
import 'package:partnerum/orders/orders/orders_page.dart';
import 'package:partnerum/pages/request/share.dart';
import 'package:partnerum/utils/brain_module.dart';
import 'package:partnerum/utils/theme_constants.dart';
import 'package:partnerum/widgets/user_title/user_tile.dart';

import 'package:scroll_to_index/scroll_to_index.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import '../../chat/messages/messages_page.dart';
import '../request_create/request_create_page.dart';
import '../request_offer_card_new.dart';
import '../request_offer_card.dart';
import '../request_offer/request_offer_page.dart';
import 'suitable_apartments_helper.dart';

class FilterRequestOffers {
  bool onlyNew = false;
}

class CurrentRequestWidget extends InheritedWidget {
  final Request request;

  CurrentRequestWidget(this.request, {child}) : super(child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => false;

  static CurrentRequestWidget of(BuildContext context) => context.dependOnInheritedWidgetOfExactType<CurrentRequestWidget>();
}

class SearchingOfSuitableApartments extends StatefulWidget {
  final Request request;

  const SearchingOfSuitableApartments({Key key, this.request}) : super(key: key);

  @override
  _SearchingOfSuitableApartmentsState createState() => _SearchingOfSuitableApartmentsState();
}

class _SearchingOfSuitableApartmentsState extends State<SearchingOfSuitableApartments>
    with
        SingleTickerProviderStateMixin<SearchingOfSuitableApartments>,
        AfterLayoutMixin<SearchingOfSuitableApartments>,
        SearchingOfSuitableApartmentsHelper {
  // final ValueNotifier<bool> blockFilterOpen = ValueNotifier<bool>(true);

  final ValueNotifier<bool> _filterOpened = ValueNotifier<bool>(false);

  final ValueNotifier<FilterRequestOffers> _filterList = ValueNotifier<FilterRequestOffers>(FilterRequestOffers());

  final ValueNotifier<bool> _loaderVisibility = ValueNotifier<bool>(true);

  _scrollListener() {
    _filterOpened.value = false;
    if (_clicked) mindClass.updateEvent('removeRoundClick');
    if (panelController.isPanelClosed) panelController.show();
  }

  bool blockFilterOpen = true;
  bool _clicked = false;
  bool _filtered = false;

  StreamSubscription _subscription;

  List<PhotoImage> photosList = <PhotoImage>[];
  @override
  void initState() {
    super.initState();
    _autoScrollController.addListener(_scrollListener);
    _subscription = mindClass.eventSteam.outStream.listen((value) async {
      print('outStream.listen: ${value}');
      if (value is String && value.contains('scrollToElement')) {
        var scrollIndex = value.replaceFirst('scrollToElement', '');
        // await _autoScrollController.scrollToIndex(scrollIndex.hashCode, preferPosition: AutoScrollPosition.begin);
        print('scrollIndex.hashCode: ${scrollIndex.hashCode}');
        await _autoScrollController.scrollToIndex(scrollIndex.hashCode, preferPosition: AutoScrollPosition.begin);
        _clicked = true;
      }
      if (value == 'updateHouse') _filterList.notifyListeners();
      if (value == 'updateView') _loaderVisibility.value = false;
      if (value == 'update_suitable_apartments_page') setState(() {});
      if (value.toString().contains('hideBottomSheet')) {
        value == 'hideBottomSheettrue' ? panelController.hide() : panelController.show();
      }
    });
  }

  @override
  void dispose() {
    _subscription.cancel();
    _autoScrollController.removeListener(_scrollListener);
    super.dispose();
  }

  PanelController panelController = PanelController();

  @override
  Widget build(BuildContext context) {
    return CurrentRequestWidget(widget.request,
        child: Scaffold(
            backgroundColor: Colors.white,
            body: SlidingUpPanel(
                color: Colors.transparent,
                controller: panelController,
                maxHeight: kToolbarHeight * 4,
                panelBuilder: (ScrollController sc) => Container(
                    decoration: BoxDecoration(color: Colors.white, borderRadius: sheetRadius),
                    clipBehavior: Clip.antiAlias,
                    child: SingleChildScrollView(
                        controller: sc,
                        physics: BouncingScrollPhysics(),
                        child: Column(mainAxisSize: MainAxisSize.min, children: [
                          Padding(
                              padding: const EdgeInsets.all(16).copyWith(bottom: 8, top: 8),
                              child: Text(Strings.pages.suitableApartmentsPage.yoursRequest.l(context),
                                  textAlign: TextAlign.start, style: TextStyle(fontSize: 26, color: Colors.black))),
                          RequestCard(request: widget.request, hideActions: true, cardForm: false)
                        ]))),
                body: RefreshIndicator(
                    displacement: 80,
                    onRefresh: () async {
                      /*
                      _loaderVisibility.value = true;
                      return await mindClass.updateUserSearch(
                          widget.request.requestId);

                       */
                    },
                    child: ScrollConfiguration(
                        behavior: noHighlightScrollBehavior(context),
                        child: CustomScrollView(controller: _autoScrollController, slivers: [
                          buildAppBar(),
                          filterHeader(),
                          drawPage(),
                          SliverToBoxAdapter(child: SizedBox(height: kToolbarHeight * 2))
                        ]))))));
  }

  Widget drawPage() {
    var goody = Padding(
        padding: const EdgeInsets.only(left: 16.0, bottom: 16, top: 16, right: 16),
        child: Text(Strings.pages.suitableApartmentsPage.suitableApartmentsText.l(context), style: Theme.of(context).textTheme.headline6));
    var nGoody = Padding(
        padding: const EdgeInsets.all(16),
        child: Text(Strings.pages.suitableApartmentsPage.noMoreFlats.l(context),
            textAlign: TextAlign.start, style: TextStyle(fontSize: 26, color: Colors.black)));
    var requestsObject = requestsStream(widget.request.key);
    // requestsObject.map((event) => event?.values?.toList() ?? []);
    return StreamBuilder(
        stream: requestsObject,
        builder: (context, snapshot) {
          // print('Offers offers: ${snapshot.data}');
          var origin = snapshot.hasData && snapshot.data != null ? snapshot.data : <Offer>[];

          // if (snapshot.hasData) {
          //   Timer(kThemeChangeDuration, () => _loaderVisibility.value = false);
          // }

          return ValueListenableBuilder(
              valueListenable: _filterList,
              builder: (BuildContext context, value, Widget child) {
                var filtered = List<Offer>.from(origin).reversed.toList();

                if (filtered.isEmpty) {
                  _loaderVisibility.value = false;
                  return SliverToBoxAdapter(
                      child: SizedBox(
                          height: MediaQuery.of(context).size.height - AppBar().preferredSize.height * 3,
                          child: Center(child: Text(Strings.pages.suitableApartmentsPage.noVariants.l(context)))));
                }
                /*
                if ((value as FilterRequestOffers).onlyNew) {
                  filtered = filtered.where((house) => !house.status.denyRequest).toList();
                }
                 */
                return DiffUtilSliverList<Offer>(
                    removeAnimationDuration: const Duration(milliseconds: 300),
                    insertAnimationDuration: const Duration(milliseconds: 300),
                    items: filtered,
                    equalityChecker: (offer1, offer2) => offer1.key == offer2.key,
                    builder: (context, offer) {
                      if (offer?.userId?.value == null || offer?.houseId?.value == null) {
                        return SizedBox.shrink();
                      }
                      var userInformationObject = loadUserInformation(offer.userId.value);
                      var bookedDates = loadHouseBookingDates(offer);
                      var houseInformationObject = loadHouseInformation(offer);
                      var conversationUsersObject = conversationUsers(offer?.offerId?.value);

                      return AutoScrollTag(
                          key: Key(offer.userId.value + ';' + offer.houseId.value),
                          // index: offer.key.hashCode,
                          index: (offer.userId.value + ';' + offer.houseId.value).hashCode,
                          controller: _autoScrollController,
                          /*    child: RequestOffer(house: house, messageInstanceId: house.messageInstanceId, user: house.userModel) */
                          child: StreamBuilder(
                            stream: userInformationObject.stream.asBroadcastStream(),
                            initialData: userInformationObject,
                            builder: (BuildContext context, AsyncSnapshot<dynamic> userSnapshot) {


                              // var houseObjectPhotoValues = houseInformationObject.photos.values;
                              // if (houseObjectPhotoValues != null && houseObjectPhotoValues.toList().isNotEmpty) {
                              //   for (var photoId in houseObjectPhotoValues.toList()) {
                              //     photosList.add(root.imagesContainer[photoId.value]);
                              //   }
                              // }

                              if (userSnapshot.hasData) {
                                if (!_filtered ||
                                    (conversationUsersObject == null) ||
                                    (conversationUsersObject?.usersStatuses?.requestUserStatus?.value == 'house_request_canceled')) {
                                  return FutureBuilder<List<BookingPeriod>>(
                                      future: bookedDates.future.then((value) => value?.values?.toList() ?? []),
                                      initialData: bookedDates?.values?.toList() ?? [],
                                      builder: (context, bookedDatesSnapshot) {
                                        return FutureBuilder(
                                            future: houseInformationObject?.future,
                                            initialData: houseInformationObject,
                                            builder: (context, houseInformationSnapshot) {
                                              if (houseInformationSnapshot.hasData) {
                                                // return Container(color: Colors.purple, width: 200, height: 100,);
                                                if (conversationUsersObject == null) {
                                                  return RequestOffer(
                                                    offer: offer,
                                                    houseInformation: houseInformationSnapshot.data,
                                                    userInformation: userSnapshot.data,
                                                    requestInformation: widget.request.requestInformation,
                                                    bookingIntersectionMessage: null,
                                                    conversationUsers: conversationUsersObject,
                                                    requestId: widget.request.key,
                                                    photosList: photosList,
                                                    bookedDates: bookedDatesSnapshot.data,
                                                  );
                                                }
                                                return FutureBuilder(
                                                  future: loadHouseImages(houseInformationObject),
                                                  builder: (BuildContext context, AsyncSnapshot<List<PhotoImage>> photosSnaphot) {
                                                    photosList = photosSnaphot.data ?? [];
                                                    return  StreamBuilder(
                                                        stream: conversationUsersObject.stream.asBroadcastStream(),
                                                        initialData: conversationUsersObject,
                                                        builder: (context, conversationSnapshot) {
                                                          Timer(kThemeChangeDuration, () => _loaderVisibility.value = false);
                                                          return RequestOffer(
                                                            offer: offer,
                                                            houseInformation: houseInformationSnapshot.data,
                                                            userInformation: userSnapshot.data,
                                                            requestInformation: widget.request.requestInformation,
                                                            bookingIntersectionMessage: null,
                                                            conversationUsers: conversationSnapshot?.data,
                                                            requestId: widget.request.key,
                                                            photosList: photosList,
                                                            bookedDates: bookedDatesSnapshot.data,
                                                          );
                                                        });
                                                  },

                                                );
                                              }
                                              // FutureBuilder(future: , builder: (context, photos),)
                                              // photosList = houseInformationSnapshot.data.photos.values.toList();

                                              // return Container(height: 200, width: 200, color: Colors.orange);
                                              return Container();
                                            });
                                      });
                                }
                                // return Container(height: 200, width: 200, color: Colors.blue);
                                return Container();
                              }
                              // return Container(height: 200, width: 200, color: Colors.red);
                              return Container();
                            },
                          )
                          /*child: Container(color: Colors.green, width: 100, height: 200)*/
                          );
                    },
                    insertAnimationBuilder: (context, animation, child) => FadeTransition(opacity: animation, child: child),
                    removeAnimationBuilder: (context, animation, child) =>
                        SizeTransition(sizeFactor: animation, child: FadeTransition(opacity: animation, child: child)));
              });
        });
    /*
    return FutureBuilder(
        future: requestFuture(widget.request.key),
        builder: (context, snapshot) {
          var origin = snapshot.hasData && snapshot.data != null ? snapshot.data : <House>[];
          if (snapshot.hasData) {
            Timer(kThemeChangeDuration, () => _loaderVisibility.value = false);
          }

          return ValueListenableBuilder(
              valueListenable: _filterList,
              builder: (BuildContext context, value, Widget child) {
                var filtered = List<House>.from(origin);
                /*
                if ((value as FilterRequestOffers).onlyNew) {
                  filtered = filtered.where((house) => !house.status.denyRequest).toList();
                }

                 */
                return DiffUtilSliverList<House>(
                    removeAnimationDuration: const Duration(milliseconds: 300),
                    insertAnimationDuration: const Duration(milliseconds: 300),
                    items: filtered,
                    equalityChecker: (house1, house2) => house1.key == house2.key,
                    builder: (context, house) => AutoScrollTag(
                        key: Key(house.key),
                        index: house.key.hashCode,
                        controller: _autoScrollController,
                        /*    child: RequestOffer(house: house, messageInstanceId: house.messageInstanceId, user: house.userModel) */
                        child: Container(color: Colors.green, width: 100, height: 200)
                    ),

                    insertAnimationBuilder: (context, animation, child) => FadeTransition(opacity: animation, child: child),
                    removeAnimationBuilder: (context, animation, child) =>
                        SizeTransition(sizeFactor: animation, child: FadeTransition(opacity: animation, child: child)));
              });
        });
      */
  }

  final AutoScrollController _autoScrollController = AutoScrollController();

  Column buildBottomOffer() {
    return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      Icon(
        Icons.minimize,
        color: Colors.grey,
      ),
      Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
        Padding(
            padding: const EdgeInsets.only(top: 12.0, left: 24),
            child: Align(
                alignment: Alignment.centerLeft,
                child: Text(Strings.pages.suitableApartmentsPage.request.l(context), style: TextStyle(color: Colors.black, fontSize: 32)))),
        Padding(
            padding: const EdgeInsets.only(right: 16.0),
            child: Text(Strings.pages.suitableApartmentsPage.more.l(context), style: TextStyle(color: Colors.grey, fontSize: 20)))
      ]),
      Padding(
          padding: const EdgeInsets.only(left: 24, bottom: 8),
          child: Align(alignment: Alignment.centerLeft, child: Text('Голицино', style: TextStyle(color: Colors.black, fontSize: 20)))),
      Padding(
          padding: const EdgeInsets.only(left: 24.0),
          child: Align(alignment: Alignment.centerLeft, child: Text('Местоположение Москва, Россия', style: TextStyle(color: Colors.grey)))),
      Padding(
          padding: const EdgeInsets.only(left: 24.0),
          child: Align(alignment: Alignment.centerLeft, child: Text('Аренда от 3000 до 5000 руб/сут', style: TextStyle(color: Colors.grey)))),
      Padding(
          padding: const EdgeInsets.only(left: 24.0),
          child: Align(alignment: Alignment.centerLeft, child: Text('4 или более комнат', style: TextStyle(color: Colors.grey)))),
      SizedBox(height: 30),
      Align(
          alignment: Alignment.bottomCenter,
          child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
            FlatButton(
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                onPressed: () {},
                child: Text('Одобрить', style: TextStyle(color: Colors.orange, fontSize: 24))),
            FlatButton(
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                onPressed: () {},
                child: Text('Отклонить', style: TextStyle(color: Colors.orange, fontSize: 24)))
          ]))
    ]);
  }

  Column buildBottomReasonOfRefusing() {
    return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      Icon(Icons.minimize, color: Colors.grey),
      Padding(
          padding: const EdgeInsets.only(top: 12.0, left: 24, bottom: 16),
          child: Align(alignment: Alignment.centerLeft, child: Text('Московская', style: TextStyle(color: Colors.black, fontSize: 22)))),
      Padding(
          padding: const EdgeInsets.only(left: 24.0, top: 12),
          child: Align(alignment: Alignment.centerLeft, child: Text('Причина отклонения заявки:', style: TextStyle(color: Colors.grey)))),
      Padding(
          padding: const EdgeInsets.only(left: 24.0),
          child: Align(alignment: Alignment.centerLeft, child: Text('Мы не принимаем гостей с животными', style: TextStyle(color: Colors.grey)))),
      SizedBox(height: 30),
      Align(
          alignment: Alignment.bottomCenter,
          child: FlatButton(
              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
              onPressed: () {},
              child: Text('Отправить апелляцию', style: TextStyle(color: Colors.orange, fontSize: 24))))
    ]);
  }

  String _selectedItem = '';

  Widget buildAppBar() => SliverAppBar(
      pinned: true,
      bottom: PreferredSize(
          child: ValueListenableBuilder(
              valueListenable: _loaderVisibility,
              builder: (BuildContext context, bool value, Widget child) =>
                  AnimatedOpacity(child: linearProgressIndicator, duration: Duration(milliseconds: 300), opacity: value ? 1 : 0)),
          preferredSize: Size.fromHeight(2)),
      actions: [
        IconButton(
            icon: Icon(Icons.filter_list),
            onPressed: () {
              blockFilterOpen = false;
              _filterOpened.value = !_filterOpened.value;
            })
      ],
      backgroundColor: Colors.white,
      iconTheme: IconThemeData(color: Colors.black),
      title: Text(Strings.pages.suitableApartmentsPage.titleText.l(context), style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal)));

  void _selectItem(String name) {
    Navigator.pop(context);
    setState(() {
      _selectedItem = name;
    });
  }

  filterHeader() {
    return ValueListenableBuilder(
      builder: (BuildContext context, value, Widget child) {
        const max = kToolbarHeight + 20;
        return TweenAnimationBuilder(
            builder: (context, _value, child) {
              return SliverPersistentHeader(pinned: false, delegate: SliverAppBarHidenDelegate(minHeight: _value, maxHeight: _value, child: child));
            },
            tween: blockFilterOpen
                ? Tween<double>(begin: 0, end: 0)
                : !value
                    ? Tween<double>(begin: max, end: 0)
                    : Tween<double>(begin: 0, end: max),
            duration: kThemeChangeDuration,
            child: SingleChildScrollView(
                child: AnimatedContainer(
                    duration: kThemeAnimationDuration,
                    decoration: BoxDecoration(color: Colors.white, boxShadow: [
                      BoxShadow(
                          color: Colors.grey.withOpacity(0.2), spreadRadius: 1, blurRadius: 1, offset: Offset(0, 3) // changes position of shadow
                          )
                    ]),
                    child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(mainAxisSize: MainAxisSize.min, children: [
                          SwitchListTile(
                              title: Text(Strings.pages.suitableApartmentsPage.hideSendedRequests.l(context)),
                              value: _filterList.value.onlyNew,
                              onChanged: (value) {
                                _filtered = !_filtered;
                                _filterList.value.onlyNew = value;
                                _filterOpened.value = _filterOpened.value;
                                _filterOpened.notifyListeners();
                                _filterList.notifyListeners();
                              })
                        ])))));
      },
      valueListenable: _filterOpened,
    );
  }

  getCard(BuildContext buildContext, String id, [isReady = false, list = const []]) {
    return RequestOfferCard(
        id: id,
        isReady: isReady,
        list: list,
        newStyle: true,
        show: (idSplit) async {
          setState(() {
            _loaderVisibility.value = true;
          });
          /*
          if (isReady) {
            await mindClass.offerRequest(
                idSplit[0], idSplit[1], widget.request.requestId);
            //updateView();
            return;
          }
          await mindClass.offerRequest(
              idSplit[0], idSplit[1], widget.request.requestId);

           */
        });
  }

  @override
  void afterFirstLayout(BuildContext context) {
    //_loaderVisibility.value = false;
  }
}

class SliverAppBarHidenDelegate extends SliverPersistentHeaderDelegate {
  SliverAppBarHidenDelegate({
    @required this.minHeight,
    @required this.maxHeight,
    @required this.child,
  });

  final double minHeight;
  final double maxHeight;
  final Widget child;

  @override
  double get minExtent => minHeight;

  @override
  double get maxExtent => math.max(maxHeight, minHeight);

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    return SizedBox.expand(child: child);
  }

  @override
  bool shouldRebuild(SliverAppBarHidenDelegate oldDelegate) {
    return maxHeight != oldDelegate.maxHeight || minHeight != oldDelegate.minHeight || child != oldDelegate.child;
  }
}
