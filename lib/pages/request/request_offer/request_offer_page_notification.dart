import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';
import 'dart:math' as math;
import 'package:db_partnerum/db_partnerum.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';

import 'package:after_layout/after_layout.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:diffutil_sliverlist/diffutil_sliverlist.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart' hide ExpansionTile;
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:partnerum/head_module/gen_extensions/generated_str_extension.dart';
import 'package:partnerum/models/requests_model.dart';

import 'package:partnerum/models/user_info_model.dart';
import 'package:partnerum/pages/house/house_details/house_details_page.dart';
import 'package:partnerum/orders/orders/orders_page.dart';
import 'package:partnerum/pages/request/share.dart';
import 'package:partnerum/utils/brain_module.dart';
import 'package:partnerum/utils/theme_constants.dart';
import 'package:partnerum/widgets/partnerum_icons.dart';
import 'package:partnerum/widgets/user_title/user_tile.dart';

import 'package:scroll_to_index/scroll_to_index.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import '../../chat/messages/messages_page.dart';
import '../request_create/request_create_page.dart';
import '../request_offer_card_new.dart';
import '../request_offer_card.dart';
import '../suitable_apartments/suitable_apartments_page.dart';
import 'request_offer_helper.dart';

class RequestOfferNotification extends StatefulWidget {
  final User user;
  final OfferObject offerObject;

  const RequestOfferNotification({Key key, this.user, this.offerObject}) : super(key: key);

  @override
  _RequestOfferState createState() => _RequestOfferState();
}

class _RequestOfferState extends State<RequestOfferNotification> with RequestOfferHelper {
  bool _fullScreen = false;

  @override
  Widget build(BuildContext context) {
    Text title(text) => Text(text, style: Theme.of(context).textTheme.headline6);
    Padding divider() => Padding(padding: const EdgeInsets.symmetric(horizontal: 8.0), child: Divider());
    Column miniInfo() {
      var stations = widget.offerObject.houseCopy.stations.values.toList();
      if (stations.length > 8) stations = stations.sublist(0, 8);
      return Column(key: ValueKey(_fullScreen), mainAxisSize: MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
        Wrap(spacing: 4, children: [
          for (var station in stations)
            Chip(
                label: Text(station.key, style: Theme.of(context).chipTheme.labelStyle.copyWith(color: Colors.orangeAccent)),
                padding: EdgeInsets.zero,
                shape: StadiumBorder(side: BorderSide(color: Colors.orangeAccent)),
                backgroundColor: Colors.white,
                elevation: 0),
          if (widget.offerObject.houseCopy.stations.length > 8)
            ActionChip(
                pressElevation: 0,
                label: Text('...', style: Theme.of(context).chipTheme.labelStyle.copyWith(color: Colors.orangeAccent)),
                padding: EdgeInsets.zero,
                shape: StadiumBorder(side: BorderSide(color: Colors.orangeAccent)),
                backgroundColor: Colors.white,
                onPressed: () => setState(() {
                      _fullScreen = !_fullScreen;
                      mindClass.updateEvent('hideBottomSheet$_fullScreen');
                    }),
                elevation: 0),
          for (String addressPart in widget.offerObject.houseCopy.address.value.split(','))
            Chip(
                label: Text(addressPart.trim(), style: Theme.of(context).chipTheme.labelStyle.copyWith(color: Colors.green)),
                padding: EdgeInsets.zero,
                shape: StadiumBorder(side: BorderSide(color: Colors.green)),
                backgroundColor: Colors.white,
                elevation: 0),
          Chip(
              label: Text(
                  '${Strings.pages.requestOfferPage.requestOfferPageNotification.priceFrom.l(context)} ${widget.offerObject.houseCopy.priceRange.startPosition.value} ${Strings.pages.requestOfferPage.requestOfferPageNotification.priceMeasure.l(context)}',
                  style: Theme.of(context).chipTheme.labelStyle.copyWith(color: Colors.white)),
              padding: EdgeInsets.zero,
              shape: StadiumBorder(side: BorderSide(color: Colors.orangeAccent)),
              backgroundColor: Colors.orangeAccent,
              elevation: 0),
          Chip(
              label: Text(
                  '${Strings.pages.requestOfferPage.requestOfferPageNotification.priceTo.l(context)} ${widget.offerObject.houseCopy.priceRange.endPosition.value} ${Strings.pages.requestOfferPage.requestOfferPageNotification.priceMeasure.l(context)}',
                  style: Theme.of(context).chipTheme.labelStyle.copyWith(color: Colors.white)),
              padding: EdgeInsets.zero,
              shape: StadiumBorder(side: BorderSide(color: Colors.orangeAccent)),
              backgroundColor: Colors.orangeAccent,
              elevation: 0)
        ])
      ]);
    }

    maxInfo() => Column(key: ValueKey(_fullScreen), mainAxisSize: MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
          spacer(0, 8),
          title(Strings.pages.requestOfferPage.requestOfferPageNotification.metroStations.l(context)),
          spacer(0, 8),
          Wrap(spacing: 4, children: [
            for (MapEntry station in widget.offerObject.houseCopy.stations.entries)
              Chip(
                  label: Text(station.value, style: Theme.of(context).chipTheme.labelStyle.copyWith(color: Colors.orangeAccent)),
                  padding: EdgeInsets.zero,
                  shape: StadiumBorder(side: BorderSide(color: Colors.orangeAccent)),
                  backgroundColor: Colors.white,
                  elevation: 0)
          ]),
          spacer(0, 8),
          divider(),
          spacer(0, 8),
          title(Strings.pages.requestOfferPage.requestOfferPageNotification.address.l(context)),
          spacer(0, 8),
          Wrap(spacing: 4, children: [
            for (String addressPart in widget.offerObject.houseCopy.address.value.split(","))
              Chip(
                  label: Text(addressPart.trim(), style: Theme.of(context).chipTheme.labelStyle.copyWith(color: Colors.green)),
                  padding: EdgeInsets.zero,
                  shape: StadiumBorder(side: BorderSide(color: Colors.green)),
                  backgroundColor: Colors.white,
                  elevation: 0)
          ]),
          spacer(0, 8),
          divider(),
          title(Strings.pages.requestOfferPage.requestOfferPageNotification.price.l(context)),
          spacer(0, 8),
          Wrap(spacing: 4, children: [
            Chip(
                label: Text(
                    '${Strings.pages.requestOfferPage.requestOfferPageNotification.priceFrom.l(context)} ${widget.offerObject.houseCopy.priceRange.startPosition} ${Strings.pages.requestOfferPage.requestOfferPageNotification.priceMeasure.l(context)}',
                    style: Theme.of(context).chipTheme.labelStyle.copyWith(color: Colors.white)),
                padding: EdgeInsets.zero,
                shape: StadiumBorder(side: BorderSide(color: Colors.orange)),
                backgroundColor: Colors.orangeAccent,
                elevation: 0),
            Chip(
                label: Text(
                    '${Strings.pages.requestOfferPage.requestOfferPageNotification.priceTo.l(context)} ${widget.offerObject.houseCopy.priceRange.endPosition} ${Strings.pages.requestOfferPage.requestOfferPageNotification.priceMeasure.l(context)}',
                    style: Theme.of(context).chipTheme.labelStyle.copyWith(color: Colors.white)),
                padding: EdgeInsets.zero,
                shape: StadiumBorder(side: BorderSide(color: Colors.orange)),
                backgroundColor: Colors.orangeAccent,
                elevation: 0)
          ]),
          divider()
        ]);

    houseElement([special = false]) {
      // print('Offer obj: ${widget.offerObject.houseCopy.city.value}');
      /*if (special) {*/
      final _counter = ValueNotifier<int>(0);
      return Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: Column(mainAxisSize: MainAxisSize.min, children: [
            GestureDetector(
                onTap: () {
                  // Navigator.push(
                  //     context,
                  //     CupertinoPageRoute(
                  //         builder: (context) =>
                  //             OfferDetailsPage(edit: false, house: house)));
                },
                child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 15),
                    child: Card(
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16))),
                        color: Colors.white,
                        clipBehavior: Clip.antiAlias,
                        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                          ListTile(
                              // onTap: () => openUserInfo(widget.user.key, widget.user, context),
                              onTap: () => openUserInfo(context: context),
                              contentPadding: EdgeInsets.symmetric(vertical: 5).copyWith(left: 16),
                              title: Text.rich(TextSpan(children: <InlineSpan>[
                                TextSpan(text: widget.user.userInformation.username.value),
                                /*if (DateTime.now().millisecondsSinceEpoch - widget.user.lastOnline <= 60000)*/
                                if (DateTime.now().millisecondsSinceEpoch - widget.user.userInformation.lastOnline.value.inMilliseconds <= 60000)
                                  WidgetSpan(
                                      alignment: PlaceholderAlignment.top,
                                      child: ClipOval(child: Container(width: 8, height: 8, color: Colors.greenAccent))),
                                /*TextSpan(text: ' • ${widget.user?.isConfirmedHumanReadable()}')

                                 */
                              ])),
                              subtitle: Text(Strings.pages.requestOfferPage.requestOfferPageNotification.rating.l(context)),
/*                              leading: CircleAvatar(backgroundColor: Colors.grey, backgroundImage: CachedNetworkImageProvider(widget.user.avatar))),*/
                              leading: CircleAvatar(
                                backgroundColor: Colors.grey,
                                // backgroundImage: CachedNetworkImageProvider(widget.user.userInformation.avatar.value)
                              )),
                          LayoutBuilder(builder: (context, constraints) {
                            return SizedBox(
                                height: MediaQuery.of(context).size.height * 1 / 6,
                                child: Stack(overflow: Overflow.visible, children: [
                                  Positioned(
                                    top: -11,
                                    bottom: 0,
                                    left: 0,
                                    right: 0,
                                    child: LayoutBuilder(builder: (context, constraints) {
                                      return SizedBox(
                                          height: MediaQuery.of(context).size.height * 1 / 6,
                                          child: ListView(
                                              physics: BouncingScrollPhysics(),
                                              scrollDirection: Axis.horizontal,
                                              shrinkWrap: true,
                                              children: [
                                                if (widget?.offerObject?.houseCopy?.photos?.values != null)
                                                  for (var url in widget.offerObject.houseCopy.photos.values.toList())
                                                    Padding(
                                                        padding: EdgeInsets.only(right: 8),
                                                        child: ClipRRect(
                                                            //borderRadius: BorderRadius.circular(16),
                                                            child: Stack(children: <Widget>[
                                                          /*
                                                              CachedNetworkImage(
                                                              fit: BoxFit.cover, width: constraints.maxWidth, imageUrl: url.smallImg.url

                                                          ),
                                                          */
                                                          Positioned.fill(child: Container(color: Colors.black.withOpacity(0.05)))
                                                        ])))
                                              ]));
                                    }),
                                    /*
                                      child: ListView(physics: BouncingScrollPhysics(), scrollDirection: Axis.horizontal, shrinkWrap: true, children: [
                                        /*
                                            for (var url in offerObject.houseCopy.photos.list)
                                          Padding(
                                              padding: EdgeInsets.only(right: 8),
                                              child: ClipRRect(
                                                  borderRadius: BorderRadius.circular(16),
                                                  child: Stack(children: <Widget>[
                                                    CachedNetworkImage(fit: BoxFit.cover, width: constraints.maxWidth, imageUrl: url),
                                                    Positioned.fill(child: Container(color: Colors.black.withOpacity(0.05)))
                                                  ]))),


                                         */

                                                    ///TODO добавить, если будет надо
//                                    if (house.info.photoUrls.length < 2)
//                                      Container(
//                                        height:
//                                            MediaQuery.of(context).size.height *
//                                                1 /
//                                                6,
//                                        width:
//                                            MediaQuery.of(context).size.width *
//                                                    2 /
//                                                    3 -
//                                                80,
//                                        child: Column(
//                                          mainAxisAlignment:
//                                              MainAxisAlignment.center,
//                                          crossAxisAlignment:
//                                              CrossAxisAlignment.center,
//                                          children: <Widget>[
//                                            Icon(
//                                              Icons.add_a_photo,
//                                              color: Theme.of(context)
//                                                  .primaryColor,
//                                            ),
//                                            Text("Запросить больше фото"),
//                                            Text(
//                                                "Попросите владельца добавить больше фото"),
//                                          ],
//                                        ),
//                                      )
                                      ])

                                       */
                                  )
                                ]));
                          }),
                          Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Theme(
                                  data: ThemeData(iconTheme: IconThemeData(color: Colors.grey.shade500)),
                                  child: Column(mainAxisSize: MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                    Text(widget.offerObject.houseCopy.stations.values.map((e) => e.key).toList().join(', '),
                                        style: Theme.of(context).textTheme.subtitle1, maxLines: 3, overflow: TextOverflow.ellipsis),
                                    // Text('stations', style: Theme.of(context).textTheme.subtitle1, maxLines: 3, overflow: TextOverflow.ellipsis),
                                    spacer(0, 8),
                                    if (widget.offerObject.houseCopy.address.value.isNotEmpty)
                                      Text(widget.offerObject.houseCopy.address.value, style: Theme.of(context).textTheme.subtitle2),
                                    spacer(0, 4),
                                    /*
                                                if (widget.offerObject.houseCopy.rating != null)
                                                  Row(
                                                      mainAxisSize: MainAxisSize
                                                          .min,
                                                      children: <Widget>[
                                                        Text(house.info.rating
                                                            .toString()),
                                                        Text(house.info.reviews
                                                            .length.toString())
                                                      ]
                                                  )
                                                else
                                                  Text(
                                                      'Нет ни оценок, не отзывов'),

                                                 */
                                    Text(
                                        '${Strings.pages.requestOfferPage.requestOfferPageNotification.priceFrom.l(context)} ${widget.offerObject.houseCopy.priceRange.startPosition.value} ${Strings.pages.requestOfferPage.requestOfferPageNotification.priceTo.l(context)} ${widget.offerObject.houseCopy.priceRange.endPosition.value} ${Strings.pages.requestOfferPage.requestOfferPageNotification.priceMeasure.l(context)}',
                                        style: Theme.of(context).textTheme.caption)
                                  ]))),
                          // if (widget.offerObject.houseCopy.description != 'yours')
                          if (true)
                            Container(
                                width: double.maxFinite,
                                child: Wrap(alignment: WrapAlignment.end, children: <Widget>[
                                  true //TODO
                                      ? FlatButton(
                                          padding: EdgeInsets.all(5),
                                          /*
                                                  onPressed: () async =>
                                                  await share(
                                                      house.info
                                                          .toExportableJson(),
                                                      house.info.photoUrls,
                                                      context),

                                                   */
                                          child: Icon(Icons.share))
                                      : Container(),
                                  (widget?.offerObject?.key != null) //TODO
                                      ? FutureBuilder<ConversationUsers>(
                                          future: getOwnerStatus(widget?.offerObject?.key),
                                          builder: (context, snapshot) {
                                            return FlatButton(
                                              padding: EdgeInsets.all(5),
                                              onPressed: () {
                                                var msg = '';
                                                if (snapshot.data.usersStatuses.ownerUserStatus.value.split(';').first == 'request_accept' ||
                                                    snapshot.data.usersStatuses.ownerUserStatus.value.split(';').first == 'request_booked') {
                                                  Navigator.push(
                                                      context,
                                                      CupertinoPageRoute(
                                                          builder: (context) => Messages(
                                                                messageInstanceId: snapshot.data.chatId.value,
                                                                // noInfo: true,
                                                              )));
                                                } else {
                                                  if (snapshot.data.usersStatuses.ownerUserStatus.value.split(';').first == 'request_denied') {
                                                    msg = Strings.pages.requestOfferPage.requestOfferPageNotification.ownerDeclinedMessage.l(context);
                                                  } else {
                                                    msg = Strings.pages.requestOfferPage.requestOfferPageNotification.waitForAnswerMessage.l(context);
                                                  }
                                                  Fluttertoast.showToast(
                                                    textColor: Colors.white,
                                                    backgroundColor: Colors.black54,
                                                    msg: msg,
                                                    toastLength: Toast.LENGTH_SHORT,
                                                    gravity: ToastGravity.BOTTOM,
                                                    timeInSecForIosWeb: 2,
                                                  );
                                                }
                                              },
                                              child: Icon(Partnerum.help, size: 25.0),
                                            );
                                          })
                                      : Container(),
                                  Divider(),
                                  ListTile(
                                      onTap: () {
                                        if (widget?.offerObject?.conversationUsers?.usersStatuses?.requestUserStatus?.value ==
                                            'house_request_canceled') {
                                          offerRequestByKey(widget.offerObject.key);
                                          // uploadRequestOffer(
                                          //     offer: widget.offer,
                                          //     ownerId: widget.user.key,
                                          //     houseId: widget.house.key,
                                          //     requestInformation: widget
                                          //         .requestInformation,
                                          //     houseInformation: widget
                                          //         .houseInformation);
                                        } else {
                                          declineRequestOfferByKey(widget.offerObject.key);
                                        }
                                      },
                                      title: Align(
                                          alignment: Alignment.centerRight,
                                          child: Text(
                                              (widget?.offerObject?.conversationUsers?.usersStatuses?.requestUserStatus?.value ==
                                                      'house_request_canceled')
                                                  ? Strings.pages.suitableApartmentsPage.makeOfferText.l(context)
                                                  : Strings.pages.suitableApartmentsPage.cancelOfferText.l(context),
                                              style: Theme.of(context).textTheme.button.merge(TextStyle(color: Colors.orange, fontSize: 16)))))
                                ])),
                          SizedBox(height: 8)
                        ])))),
            /*
                if (!house.info
                    .possibleToBook(CurrentRequestWidget
                    .of(context)
                    .request))
                  Padding(
                      padding: const EdgeInsets.only(top: 8.0).add(
                          EdgeInsets.symmetric(horizontal: 15)
                      ),
                      child: Card(
                          child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Text(
                                        'К сожалению в выбраннные Вами даты невозможно запросить'
                                            ' бронь, квартира забронирована в следующеи даты.',
                                        style: Theme
                                            .of(context)
                                            .textTheme
                                            .bodyText1
                                    ),
                                    Wrap(
                                        alignment: WrapAlignment.start,
                                        crossAxisAlignment: WrapCrossAlignment
                                            .start,
                                        children: [
                                          Chip(label: Text('23.05.23-23.06.23'))
                                        ]
                                    ),
                                    Text(
                                        'Хотите изменить даты у себя в заявке?',
                                        style: Theme
                                            .of(context)
                                            .textTheme
                                            .subtitle2,
                                        textAlign: TextAlign.start
                                    ),
                                    Align(
                                        alignment: Alignment.bottomRight,
                                        child: FlatButton(
                                            child: Text('ДА'),
                                            onPressed: () {
                                              Navigator.push(
                                                  context,
                                                  CupertinoPageRoute(
                                                      builder: (_) =>
                                                          RequestCreate(
                                                              requestId:
                                                              CurrentRequestWidget
                                                                  .of(context)
                                                                  .request
                                                                  .requestId
                                                          )
                                                  )
                                              ).then((v) {
                                                mindClass.updateEvent(
                                                    'update_suitable_apartments_page');
                                              });
                                            }
                                        ))
                                  ]
                              )
                          )
                      )
                  )

                 */
          ]));
    }

    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Card(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16))),
            clipBehavior: Clip.antiAlias,
            child: houseElement(true)));
  }
}
