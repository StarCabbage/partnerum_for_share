import 'dart:async';
import 'dart:ffi';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';
import 'dart:math' as math;
import 'package:db_partnerum/db_partnerum.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';

import 'package:after_layout/after_layout.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:diffutil_sliverlist/diffutil_sliverlist.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart' hide ExpansionTile;
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:partnerum/head_module/gen_extensions/generated_str_extension.dart';
import 'package:partnerum/models/houses_model.dart' as housesModel;
import 'package:partnerum/models/requests_model.dart';

import 'package:partnerum/models/user_info_model.dart';
import 'package:partnerum/pages/house/house_details/house_details_page.dart';
import 'package:partnerum/orders/orders/orders_page.dart';
import 'package:partnerum/pages/request/share.dart';
import 'package:partnerum/utils/brain_module.dart';
import 'package:partnerum/utils/theme_constants.dart';
import 'package:partnerum/widgets/partnerum_icons.dart';
import 'package:partnerum/widgets/user_title/user_tile.dart';

import 'package:scroll_to_index/scroll_to_index.dart';
import 'package:shimmer/shimmer.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import '../../chat/messages/messages_page.dart';
import '../request_create/request_create_page.dart';
import '../request_offer_card_new.dart';
import '../request_offer_card.dart';
import '../suitable_apartments/suitable_apartments_page.dart';
import 'request_offer_helper.dart';

class RequestOffer extends StatefulWidget {
  final Offer offer;
  final HouseInformation houseInformation;
  final UserInformation userInformation;
  final RequestInformation requestInformation;
  final String bookingIntersectionMessage;
  final ConversationUsers conversationUsers;
  final String requestId;
  final List photosList;

  final String userId;
  final String houseId;
  final String offerId;

  final List<BookingPeriod> bookedDates;

  const RequestOffer(
      {Key key,
      this.offer,
      this.houseInformation,
      this.userInformation,
      this.requestInformation,
      this.bookingIntersectionMessage,
      this.conversationUsers,
      this.requestId,
      this.photosList,
      this.userId,
      this.houseId,
      this.offerId,
      this.bookedDates})
      : super(key: key);

  @override
  _RequestOfferState createState() => _RequestOfferState();
}

class _RequestOfferState extends State<RequestOffer> with RequestOfferHelper {
  bool _fullScreen = false;

  @override
  Widget build(BuildContext context) {

    Text title(text) => Text(text, style: Theme.of(context).textTheme.headline6);
    Padding divider() => Padding(padding: const EdgeInsets.symmetric(horizontal: 8.0), child: Divider());
    Column miniInfo() {
      var stations = widget.houseInformation.stations.values.toList();
      if (stations.length > 8) stations = stations.sublist(0, 8);
      return Column(key: ValueKey(_fullScreen), mainAxisSize: MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
        Wrap(spacing: 4, children: [
          for (var station in stations)
            Chip(
                label: Text(station.key, style: Theme.of(context).chipTheme.labelStyle.copyWith(color: Colors.orangeAccent)),
                padding: EdgeInsets.zero,
                shape: StadiumBorder(side: BorderSide(color: Colors.orangeAccent)),
                backgroundColor: Colors.white,
                elevation: 0),
          if (widget.houseInformation.stations.length > 8)
            ActionChip(
                pressElevation: 0,
                label: Text('...', style: Theme.of(context).chipTheme.labelStyle.copyWith(color: Colors.orangeAccent)),
                padding: EdgeInsets.zero,
                shape: StadiumBorder(side: BorderSide(color: Colors.orangeAccent)),
                backgroundColor: Colors.white,
                onPressed: () => setState(() {
                      _fullScreen = !_fullScreen;
                      mindClass.updateEvent('hideBottomSheet$_fullScreen');
                    }),
                elevation: 0),
          for (String addressPart in widget.houseInformation.address.value.split(','))
            Chip(
                label: Text(addressPart.trim(), style: Theme.of(context).chipTheme.labelStyle.copyWith(color: Colors.green)),
                padding: EdgeInsets.zero,
                shape: StadiumBorder(side: BorderSide(color: Colors.green)),
                backgroundColor: Colors.white,
                elevation: 0),
          Chip(
              label: Text(
                  '${Strings.pages.requestOfferPage.priceFrom.l(context)} ${widget.houseInformation.priceRange.startPosition.value} ${Strings.pages.requestOfferPage.priceMeasure.l(context)}',
                  style: Theme.of(context).chipTheme.labelStyle.copyWith(color: Colors.white)),
              padding: EdgeInsets.zero,
              shape: StadiumBorder(side: BorderSide(color: Colors.orangeAccent)),
              backgroundColor: Colors.orangeAccent,
              elevation: 0),
          Chip(
              label: Text(
                  '${Strings.pages.requestOfferPage.priceTo.l(context)} ${widget.houseInformation.priceRange.endPosition.value} ${Strings.pages.requestOfferPage.priceTo.l(context)}',
                  style: Theme.of(context).chipTheme.labelStyle.copyWith(color: Colors.white)),
              padding: EdgeInsets.zero,
              shape: StadiumBorder(side: BorderSide(color: Colors.orangeAccent)),
              backgroundColor: Colors.orangeAccent,
              elevation: 0)
        ])
      ]);
    }

    maxInfo() => Column(key: ValueKey(_fullScreen), mainAxisSize: MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
          spacer(0, 8),
          title(Strings.pages.requestOfferPage.metroStations.l(context)),
          spacer(0, 8),
          Wrap(spacing: 4, children: [
            for (MapEntry station in widget.houseInformation.stations.entries)
              Chip(
                  label: Text(station.value, style: Theme.of(context).chipTheme.labelStyle.copyWith(color: Colors.orangeAccent)),
                  padding: EdgeInsets.zero,
                  shape: StadiumBorder(side: BorderSide(color: Colors.orangeAccent)),
                  backgroundColor: Colors.white,
                  elevation: 0)
          ]),
          spacer(0, 8),
          divider(),
          spacer(0, 8),
          title(Strings.pages.requestOfferPage.address.l(context)),
          spacer(0, 8),
          Wrap(spacing: 4, children: [
            for (String addressPart in widget.houseInformation.address.value.split(","))
              Chip(
                  label: Text(addressPart.trim(), style: Theme.of(context).chipTheme.labelStyle.copyWith(color: Colors.green)),
                  padding: EdgeInsets.zero,
                  shape: StadiumBorder(side: BorderSide(color: Colors.green)),
                  backgroundColor: Colors.white,
                  elevation: 0)
          ]),
          spacer(0, 8),
          divider(),
          title(Strings.pages.requestOfferPage.price.l(context)),
          spacer(0, 8),
          Wrap(spacing: 4, children: [
            Chip(
                label: Text(
                    '${Strings.pages.requestOfferPage.priceFrom.l(context)} ${widget.houseInformation.priceRange.startPosition} ${Strings.pages.requestOfferPage.priceMeasure.l(context)}',
                    style: Theme.of(context).chipTheme.labelStyle.copyWith(color: Colors.white)),
                padding: EdgeInsets.zero,
                shape: StadiumBorder(side: BorderSide(color: Colors.orange)),
                backgroundColor: Colors.orangeAccent,
                elevation: 0),
            Chip(
                label: Text(
                    '${Strings.pages.requestOfferPage.priceTo.l(context)} ${widget.houseInformation.priceRange.endPosition} ${Strings.pages.requestOfferPage.priceMeasure.l(context)}',
                    style: Theme.of(context).chipTheme.labelStyle.copyWith(color: Colors.white)),
                padding: EdgeInsets.zero,
                shape: StadiumBorder(side: BorderSide(color: Colors.orange)),
                backgroundColor: Colors.orangeAccent,
                elevation: 0)
          ]),
          divider()
        ]);

    bool ifDatesIntersection() {
      if (widget?.conversationUsers?.usersStatuses?.ownerUserStatus?.value != 'request_booked') {
        var startPeriod = widget.requestInformation.datesRange.first.value;
        var endPeriod = widget.requestInformation.datesRange.last.value;
        for (var period in widget.bookedDates) {
          if (!((endPeriod < period.checkIn.value) || (startPeriod > period.checkOut.value))) {
            return true;
          }
        }
        return false;
      } else {
        return false;
      }
    }

    houseElement(HouseInformation houseInformation, List<PhotoImage> photosList, [special = false]) {
      /*if (special) {*/
      /*
      if (false) {
        return GestureDetector(
            onTap: () {
              Navigator.push(context, CupertinoPageRoute(builder: (context) => OfferDetailsPage(edit: false, houseInformation: houseInformation)));
            },
            child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
              UserTile(uid: widget.offer.userId.value),
              LayoutBuilder(builder: (context, constraints) {
                return SizedBox(
                    height: MediaQuery.of(context).size.height * 1 / 6,
                    child: ListView(physics: BouncingScrollPhysics(), scrollDirection: Axis.horizontal, shrinkWrap: true, children: [
                      for (var url in houseInformation.photos.values.toList())
                        Padding(
                            padding: EdgeInsets.only(right: 8),
                            child: ClipRRect(
                                //borderRadius: BorderRadius.circular(16),
                                child: Stack(children: <Widget>[
                              CachedNetworkImage(fit: BoxFit.cover, width: constraints.maxWidth, imageUrl: url.smallImg.url),
                              Positioned.fill(child: Container(color: Colors.black.withOpacity(0.05)))
                            ])))
                    ]));
              }),
              /*
              if (house.status.currentRequestStatus.isNotEmpty)
                Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0)
                        .copyWith(top: 16, bottom: 4),
                    child: Text(house.status.currentRequestStatus,
                        style: Theme
                            .of(context)
                            .textTheme
                            .headline6
                            .copyWith(color: Colors.orange.shade800))
                ),

               */
              Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Theme(
                      data: ThemeData(iconTheme: IconThemeData(color: Colors.grey.shade500)),
                      child: AnimatedSwitcher(
                          duration: animationDuration,
                          switchInCurve: Curves.easeIn,
                          switchOutCurve: Curves.easeIn,
                          transitionBuilder: (Widget child, Animation<double> animation) {
                            return AnimatedSwitcher.defaultTransitionBuilder(SizeTransition(child: child, sizeFactor: animation), animation);
                          },
                          child: _fullScreen ? maxInfo() : miniInfo()))),
              /*
              if (house.info.extraInformation != 'yours')
                Container(
                    width: double.maxFinite,
                    child:
                    Wrap(alignment: WrapAlignment.start, children: <Widget>[
                      IconButton(
                          onPressed: () =>
                              setState(() {
                                _fullScreen = !_fullScreen;
                                mindClass
                                    .updateEvent('hideBottomSheet$_fullScreen');
                              }),
                          padding: EdgeInsets.zero,
                          splashColor: Colors.transparent,
                          icon: Icon(_fullScreen
                              ? Icons.expand_less
                              : Icons.expand_more)),
                      if (house.status.canShare)
                        IconButton(
                            onPressed: () async {
                              await share(house.info.toExportableJson(),
                                  house.info.photoUrls, context);
                            },
                            padding: EdgeInsets.zero,
                            splashColor: Colors.transparent,
                            icon: Icon(Icons.share)),
                      if (house.status.canChat)
                        IconButton(
                            onPressed: () =>
                                Navigator.push(
                                    context,
                                    CupertinoPageRoute(
                                        builder: (context) =>
                                            Messages(
                                                messageInstanceId:
                                                widget.messageInstanceId,
                                                noInfo: true))),
                            icon: Icon(Icons.chat)),
                      if (!house.info.possibleToBook(
                          CurrentRequestWidget
                              .of(context)
                              .request))
                        ListTile(
                            subtitle: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                      Strings.pages.requestOfferPage.datesIssueApologizing.l(context)
                                          Strings.pages.requestOfferPage.bookingMessage.l(context)),
                                  Wrap(
                                      alignment: WrapAlignment.start,
                                      crossAxisAlignment: WrapCrossAlignment
                                          .start,
                                      children: [
                                        for (DateTimeRange bookedDates in widget
                                            .house.info.bookedDates ??
                                            [
                                            ]) //[DateTimeRange(start: DateTime.now(),end: DateTime.now())]
                                          Chip(
                                              label: Text(
                                                  "${DateFormat('dd.MM.y', "ru")
                                                      .format(bookedDates
                                                      .start)} - ${DateFormat(
                                                      'dd.MM.y', "ru").format(
                                                      bookedDates.end)}",
                                                  style: TextStyle(
                                                      color: Colors.grey
                                                          .shade500)
                                              ))
                                      ]
                                  )
                                ]
                            )
                        ),
                      if (!house.info.possibleToBook(
                          CurrentRequestWidget
                              .of(context)
                              .request))
                        ListTile(
                            title: Align(
                                alignment: Alignment.centerRight,
                                child: Text(Strings.pages.requestOfferPage.changeDatesInOffer.l(context),
                                    style: Theme
                                        .of(context)
                                        .textTheme
                                        .button
                                        .merge(TextStyle(
                                        color: Colors.orange, fontSize: 16)))
                            ),
                            onTap: () async {
                              Navigator.push(
                                  context,
                                  CupertinoPageRoute(
                                      builder: (_) =>
                                          RequestCreate(
                                              requestId: CurrentRequestWidget
                                                  .of(context)
                                                  .request
                                                  .requestId
                                          )
                                  )
                              ).then((v) {
                                mindClass.updateEvent(
                                    'update_suitable_apartments_page');
                              });
                            }),
                      if (house.status.offerRequest &&
                          house.info.possibleToBook(
                              CurrentRequestWidget
                                  .of(context)
                                  .request))
                        ListTile(
                            title: Align(
                                alignment: Alignment.centerRight,
                                child: Text(
                                    Strings.pages.suitableApartmentsPage
                                        .makeOfferText
                                        .l(context),
                                    style: Theme
                                        .of(context)
                                        .textTheme
                                        .button
                                        .merge(TextStyle(
                                        color: Colors.orange, fontSize: 16)))
                            ),
                            onTap: () async {
                              /*
                              await mindClass.offerRequest(
                                  widget.user.uid,
                                  house.houseId,
                                  CurrentRequestWidget
                                      .of(context)
                                      .request
                                      .requestId);

                               */
                              setState(() {
                                widget.house.status.request =
                                    Status.send_request;
                              });
                              //mindClass.updateEvent("updateHouse");
                            }),
                      if (house.status.denyRequest)
                        ListTile(
                            title: Align(
                                alignment: Alignment.centerRight,
                                child: Text(
                                    Strings.pages.suitableApartmentsPage
                                        .cancelOfferText
                                        .l(context),
                                    style: Theme
                                        .of(context)
                                        .textTheme
                                        .button
                                        .merge(TextStyle(
                                        color: Colors.orange, fontSize: 16)))
                            ),
                            onTap: () async {
                              /*
                              await mindClass.denyRequest(
                                  widget.user.uid,
                                  house.houseId,
                                  CurrentRequestWidget
                                      .of(context)
                                      .request
                                      .requestId);

                               */
                              setState(() {
                                widget.house.status.request =
                                    Status.deny_request;
                              });
                            })
                    ]))

               */
            ]));
      }
      */
      final _counter = ValueNotifier<int>(0);
      var houseId = widget?.offer?.houseId?.value ?? widget.houseId;
      // var ownerId = widget?.conversationUsers?.ownerUser?.value?.split(';')?.first ?? '';
      var ownerId = widget?.offer?.userId?.value ?? widget.userId;
      CachedNetworkImage OwnerProfileImg;
      OwnerProfileImg = CachedNetworkImage(
          imageUrl: widget.userInformation.avatar.url,
          httpHeaders: widget.userInformation.avatar.syncHttpHeaders,
          imageBuilder: (context, imageProvider) => decoratedImage(imageProvider),
          placeholder: (context, url) =>
              Theme.of(context).platform == TargetPlatform.iOS ? CupertinoActivityIndicator() : CircularProgressIndicator(),
          errorWidget: (context, url, error) => Container(
              width: 40,
              height: 40,
              decoration: BoxDecoration(border: Border.all(color: Theme.of(context).primaryColor), shape: BoxShape.circle),
              child: Center(child: Icon(Icons.info_outline, color: Theme.of(context).primaryColor))));
      var datesIntersectionBool = ifDatesIntersection();
      return Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: Column(mainAxisSize: MainAxisSize.min, children: [
            GestureDetector(
                onTap: () {
                  Navigator.push(context,
                      CupertinoPageRoute(builder: (context) => OfferDetailsPage(edit: false, houseInformation: houseInformation, houseId: houseId)));
                },
                child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 15),
                    child: Card(
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16))),
                        color: Colors.white,
                        clipBehavior: Clip.antiAlias,
                        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                          Padding(padding: EdgeInsets.only(top: 20)),
                          // сообщение о пересечении дат

                          ListTile(
                              // onTap: () => openUserInfo(widget.user.key, widget.user, context),
                              onTap: () => openUserInfo(uid: ownerId, userInformation: widget.userInformation, context: context),
                              contentPadding: EdgeInsets.symmetric(vertical: 5).copyWith(left: 16),
                              title: Text.rich(TextSpan(children: <InlineSpan>[
                                TextSpan(text: widget?.userInformation?.username?.value ?? Strings.globalOnes.anonymousUser.l(context)),
                                /*if (DateTime.now().millisecondsSinceEpoch - widget.user.lastOnline <= 60000)*/
                                if (DateTime.now().millisecondsSinceEpoch - widget.userInformation.lastOnline.value.inMilliseconds <= 60000)
                                  WidgetSpan(
                                      alignment: PlaceholderAlignment.top,
                                      child: ClipOval(child: Container(width: 8, height: 8, color: Colors.greenAccent))),
                                /*TextSpan(text: ' • ${widget.user?.isConfirmedHumanReadable()}')

                                 */
                              ])),
                              subtitle: Text(
                                  (widget?.userInformation?.rating?.value?.toStringAsFixed(2) ?? Strings.pages.extra.userTile.noMarks.l(context))
                                      .toString()),
/*                              leading: CircleAvatar(backgroundColor: Colors.grey, backgroundImage: CachedNetworkImageProvider(widget.user.avatar))),*/
                              leading: OwnerProfileImg),
                          LayoutBuilder(builder: (context, constraints) {
                            return SizedBox(
                                height: MediaQuery.of(context).size.height * 1 / 6,
                                child: Stack(overflow: Overflow.visible, children: [
                                  Positioned(
                                    top: -11,
                                    bottom: 0,
                                    left: 0,
                                    right: 0,
                                    child: LayoutBuilder(builder: (context, constraints) {
                                      return SizedBox(
                                          height: MediaQuery.of(context).size.height * 1 / 6,
                                          child: AnimatedSwitcher(
                                            duration: kThemeAnimationDuration,
                                            child: photosList.isEmpty?
                                            Padding(
                                              padding: EdgeInsets.only(right: 8),
                                              child: Shimmer.fromColors(
                                                baseColor: Colors.grey[300],
                                                highlightColor: Colors.grey[100],
                                                child: Container(
                                                  width: constraints.maxWidth,
                                                  color: Colors.black)
                                              )) :
                                            ListView(
                                                physics: BouncingScrollPhysics(),
                                                scrollDirection: Axis.horizontal,
                                                shrinkWrap: true,
                                                children: [
                                                      for (var url in photosList)
                                                    Padding(
                                                        padding: EdgeInsets.only(right: 8),
                                                        child: ClipRRect(
                                                            //borderRadius: BorderRadius.circular(16),
                                                            child: Stack(children: <Widget>[
                                                          CachedNetworkImage(
                                                              fit: BoxFit.cover,
                                                              width: constraints.maxWidth,
                                                              // imageUrl: await url.smallImg.urlWithToken,
                                                              httpHeaders: url.smallImg.syncHttpHeaders,
                                                              imageUrl: url.smallImg.url
                                                          ),
                                                          Positioned.fill(child: Container(color: Colors.black.withOpacity(0.05)))
                                                        ])))
                                                ])
                                          ));
                                    }),
                                    /*
                                      child: ListView(physics: BouncingScrollPhysics(), scrollDirection: Axis.horizontal, shrinkWrap: true, children: [
                                        /*
                                            for (var url in house.houseInformation.photos.list)
                                          Padding(
                                              padding: EdgeInsets.only(right: 8),
                                              child: ClipRRect(
                                                  borderRadius: BorderRadius.circular(16),
                                                  child: Stack(children: <Widget>[
                                                    CachedNetworkImage(fit: BoxFit.cover, width: constraints.maxWidth, imageUrl: url),
                                                    Positioned.fill(child: Container(color: Colors.black.withOpacity(0.05)))
                                                  ]))),


                                         */

                                                    ///TODO добавить, если будет надо
//                                    if (house.info.photoUrls.length < 2)
//                                      Container(
//                                        height:
//                                            MediaQuery.of(context).size.height *
//                                                1 /
//                                                6,
//                                        width:
//                                            MediaQuery.of(context).size.width *
//                                                    2 /
//                                                    3 -
//                                                80,
//                                        child: Column(
//                                          mainAxisAlignment:
//                                              MainAxisAlignment.center,
//                                          crossAxisAlignment:
//                                              CrossAxisAlignment.center,
//                                          children: <Widget>[
//                                            Icon(
//                                              Icons.add_a_photo,
//                                              color: Theme.of(context)
//                                                  .primaryColor,
//                                            ),
//                                            Text(Strings.pages.requestOfferPage.askForMorePhotos.l(context)),
//                                            Text(
//                                                Strings.pages.requestOfferPage.askOwnerForPhotos.l(context)),
//                                                Strings.pages.requestOfferPage.askOwnerForPhotos.l(context)),
//                                          ],
//                                        ),
//                                      )ca
                                      ])

                                       */
                                  )
                                ]));
                          }),
                          Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Theme(
                                  data: ThemeData(iconTheme: IconThemeData(color: Colors.grey.shade500)),
                                  child: Column(mainAxisSize: MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                    Text((houseInformation.stations?.values?.map((e) => e?.key)?.toList() ?? [])?.join(', '),
                                        style: Theme.of(context).textTheme.subtitle1, maxLines: 3, overflow: TextOverflow.ellipsis),
                                    // Text('stations', style: Theme.of(context).textTheme.subtitle1, maxLines: 3, overflow: TextOverflow.ellipsis),
                                    spacer(0, 8),
                                    if (houseInformation?.address?.value?.isNotEmpty != null)
                                      Text(houseInformation.address.value, style: Theme.of(context).textTheme.subtitle2),
                                    spacer(0, 4),
                                    /*
                                                if (house.houseInformation.rating != null)
                                                  Row(
                                                      mainAxisSize: MainAxisSize
                                                          .min,
                                                      children: <Widget>[
                                                        Text(house.info.rating
                                                            .toString()),
                                                        Text(house.info.reviews
                                                            .length.toString())
                                                      ]
                                                  )
                                                else
                                                  Text(
                                                      Strings.pages.requestOfferPage.emptyRating.l(context)),

                                                 */
                                    Text(
                                        '${Strings.pages.requestOfferPage.priceFrom.l(context)} ${houseInformation.priceRange.startPosition.value} ${Strings.pages.requestOfferPage.priceTo.l(context)} ${houseInformation.priceRange.endPosition.value} ${Strings.pages.requestOfferPage.priceMeasure.l(context)}',
                                        style: Theme.of(context).textTheme.caption)
                                  ]))),
                          // if (house.houseInformation.description != 'yours')

                          // if (widget?.bookingIntersectionMessage == null)
                          Container(
                              width: double.maxFinite,
                              child: Wrap(alignment: WrapAlignment.end, children: <Widget>[

                                if (datesIntersectionBool)
                                  DecoratedBox(
                                    decoration: const BoxDecoration(color: Colors.red),
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(
                                          Strings.pages.requestOfferPage.datesIssueApologizing.l(context) +
                                              Strings.pages.requestOfferPage.bookingMessage.l(context),
                                          style: Theme.of(context).textTheme.bodyText1),
                                    ),
                                  ),

                                if (!datesIntersectionBool)
                                  FlatButton(
                                      padding: EdgeInsets.all(5),
                                      onPressed: () async {
                                        if (widget?.offer?.offerId?.value == null && widget?.offerId == null) {
                                          await Fluttertoast.showToast(
                                            textColor: Colors.white,
                                            backgroundColor: Colors.black54,
                                            msg: 'Поделиться предложением можно, только если владелец одобрит Вашу заявку',
                                            toastLength: Toast.LENGTH_SHORT,
                                            gravity: ToastGravity.BOTTOM,
                                            timeInSecForIosWeb: 2,
                                          );
                                        } else {
                                          var requestImages = houseInformation?.photos?.values?.toList();
                                          var imagesUint8List = await uint8ListOfImages(requestImages);

                                          await share(messageOfSharing(houseInformation, context), imagesUint8List, context);
                                        }
                                      },
                                      /*
                                                  onPressed: () async =>
                                                  await share(
                                                      house.info
                                                          .toExportableJson(),
                                                      house.info.photoUrls,
                                                      context)
                                                   */
                                      child: Icon(Icons.share)),

                                if (!datesIntersectionBool)
                                  FlatButton(
                                    padding: EdgeInsets.all(5),
                                    onPressed: widget?.offer?.offerId?.value == null && widget?.offerId == null
                                        ? () async {
                                            await Fluttertoast.showToast(
                                              textColor: Colors.white,
                                              backgroundColor: Colors.black54,
                                              msg: Strings.pages.requestOfferPage.needToMakeAnOffer.l(context),
                                              toastLength: Toast.LENGTH_SHORT,
                                              gravity: ToastGravity.BOTTOM,
                                              timeInSecForIosWeb: 2,
                                            );
                                          }
                                        : () async {
                                            if (widget?.conversationUsers?.chatId?.value != null) {
                                              enterInChat(widget?.conversationUsers?.chatId?.value);
                                              await Navigator.push(
                                                  context,
                                                  CupertinoPageRoute(
                                                      builder: (context) => Messages(
                                                            messageInstanceId: widget?.conversationUsers?.chatId?.value,
                                                            // noInfo: true,
                                                          )));
                                              return;
                                            }
                                            var msg = '';
                                            if (widget?.conversationUsers?.usersStatuses?.ownerUserStatus?.value?.split(';')?.first ==
                                                    'request_accept' ||
                                                widget?.conversationUsers?.usersStatuses?.ownerUserStatus?.value?.split(';')?.first ==
                                                    'request_booked') {
                                              var newChatID = await createChat(
                                                  widget.offer.userId.value ?? widget.userId, widget?.offer?.offerId?.value ?? widget.offerId);

                                              enterInChat(newChatID);
                                              await Navigator.push(
                                                  context,
                                                  CupertinoPageRoute(
                                                      builder: (context) => Messages(
                                                            messageInstanceId: newChatID,
                                                            // noInfo: true,
                                                          )));
                                            } else {
                                              if (widget?.conversationUsers?.usersStatuses?.ownerUserStatus?.value?.split(';')?.first ==
                                                  'request_denied') {
                                                msg = Strings.pages.requestOfferPage.ownerDeclinedRequest.l(context);
                                              } else {
                                                msg = Strings.pages.requestOfferPage.needToWaitForAnswer.l(context);
                                              }
                                              await Fluttertoast.showToast(
                                                textColor: Colors.white,
                                                backgroundColor: Colors.black54,
                                                msg: msg,
                                                toastLength: Toast.LENGTH_SHORT,
                                                gravity: ToastGravity.BOTTOM,
                                                timeInSecForIosWeb: 2,
                                              );
                                            }
                                          },
                                    child: Icon(Partnerum.help, size: 25.0),
                                  ),

                                // ? StreamBuilder<
                                //         ConversationUsers>(
                                //     stream: conversationUsers(
                                //         widget?.offer?.offerId
                                //             ?.value),
                                //     builder: (context, snapshot) {
                                //       // print('Chat id id: ${snapshot.data.chatId.value}');
                                //
                                //       var chatId = snapshot
                                //           ?.data?.chatId?.value;
                                //       var ownerUserStatus =
                                //           snapshot
                                //               ?.data
                                //               ?.usersStatuses
                                //               ?.ownerUserStatus
                                //               ?.value;
                                //       return FlatButton(
                                //         padding:
                                //             EdgeInsets.all(5),
                                //         onPressed: () async {
                                //           if (chatId != null) {
                                //             await Navigator.push(
                                //                 context,
                                //                 CupertinoPageRoute(
                                //                     builder:
                                //                         (context) =>
                                //                             Messages(
                                //                               messageInstanceId:
                                //                                   snapshot.data.chatId.value,
                                //                               // noInfo: true,
                                //                             )));
                                //             return;
                                //           }
                                //
                                //           var msg = '';
                                //           if (ownerUserStatus ==
                                //                   'request_accept' ||
                                //               ownerUserStatus ==
                                //                   'request_booked') {
                                //             chatId ??=
                                //                 await createChat(
                                //                     widget
                                //                         .offer
                                //                         .userId
                                //                         .value,
                                //                     widget
                                //                         ?.offer
                                //                         ?.offerId
                                //                         ?.value);
                                //
                                //             await Navigator.push(
                                //                 context,
                                //                 CupertinoPageRoute(
                                //                     builder:
                                //                         (context) =>
                                //                             Messages(
                                //                               messageInstanceId:
                                //                                   chatId,
                                //                               // noInfo: true,
                                //                             )));
                                //           } else {
                                //             if (ownerUserStatus ==
                                //                 'request_denied') {
                                //               msg =
                                //                   Strings.pages.requestOfferPage.ownerDeclinedRequest.l(context);
                                //             } else {
                                //               msg =
                                //                   Strings.pages.requestOfferPage.needToWaitForAnswer.l(context);
                                //             }
                                //             await Fluttertoast
                                //                 .showToast(
                                //               textColor:
                                //                   Colors.white,
                                //               backgroundColor:
                                //                   Colors.black54,
                                //               msg: msg,
                                //               toastLength: Toast
                                //                   .LENGTH_SHORT,
                                //               gravity:
                                //                   ToastGravity
                                //                       .BOTTOM,
                                //               timeInSecForIosWeb:
                                //                   2,
                                //             );
                                //           }
                                //         },
                                //         child: Icon(
                                //             Partnerum.help,
                                //             size: 25.0),
                                //       );
                                //     })
                                // : Container(),
                                Divider(),

                                if ((widget?.offer?.offerId?.value != null || widget?.offerId != null) && (!datesIntersectionBool))
                                  ListTile(
                                      onTap: () {
                                        if (widget?.conversationUsers?.usersStatuses?.requestUserStatus?.value == 'house_request_canceled') {
                                          requestUserStatusByOfferId(widget?.offer?.offerId?.value ?? widget?.offerId, 'house_request_sended');
                                        } else if (widget?.conversationUsers?.usersStatuses?.requestUserStatus?.value == 'house_request_sended' ||
                                            widget?.conversationUsers?.usersStatuses?.requestUserStatus?.value == 'request_booked') {
                                          declineRequestOffer(widget?.offer?.offerId?.value ?? widget?.offerId);
                                        }
                                      },
                                      title: Align(
                                          alignment: Alignment.centerRight,
                                          child: Text(
                                              (widget?.conversationUsers?.usersStatuses?.requestUserStatus?.value == 'house_request_canceled')
                                                  ? Strings.pages.suitableApartmentsPage.makeOfferText.l(context)
                                                  : Strings.pages.suitableApartmentsPage.cancelOfferText.l(context),
                                              style: Theme.of(context).textTheme.button.merge(TextStyle(color: Colors.orange, fontSize: 16))))),

                                // StreamBuilder(
                                //     // stream: requestUserStatusStream(
                                //     //     widget.offer.offerId.value),
                                //     stream: db
                                //         .root
                                //         .chatTopic
                                //         .offer2Room[widget
                                //             ?.offer?.offerId?.value]
                                //         .conversationUsers
                                //         .usersStatuses
                                //         .stream,
                                //     builder: (context, snapshot) {
                                //       // print(
                                //       //     'Error message: ${snapshot.error} OfferId type: ${widget?.offer?.offerId?.value.runtimeType}');
                                //       // print(snapshot.stackTrace);
                                //       // print('Offer id is not null: ${widget?.offer?.offerId?.value}');
                                //       if (snapshot.hasData &&
                                //           !snapshot.hasError) {
                                //         // print(
                                //         //     'Offer id is not null: ${widget?.offer?.offerId?.value}');
                                //         var currentRequestUserStatus =
                                //             snapshot
                                //                 ?.data
                                //                 ?.requestUserStatus
                                //                 ?.value;
                                //
                                //         // print('CurrentRequest status: ${currentRequestUserStatus.request_user_status.value}');
                                //         // print('User status: ${ currentRequestUserStatus}');
                                //         return ListTile(
                                //             onTap: () {
                                //               if (currentRequestUserStatus ==
                                //                   'house_request_canceled') {
                                //                 requestUserStatusByOfferId(
                                //                     widget
                                //                         ?.offer
                                //                         ?.offerId
                                //                         ?.value,
                                //                     'house_request_sended');
                                //               } else if (currentRequestUserStatus ==
                                //                       'house_request_sended' ||
                                //                   currentRequestUserStatus ==
                                //                       'request_booked') {
                                //                 declineRequestOffer(
                                //                     widget.offer);
                                //               }
                                //             },
                                //             title: Align(
                                //                 alignment: Alignment
                                //                     .centerRight,
                                //                 child: Text(
                                //                     (currentRequestUserStatus ==
                                //                             'house_request_canceled')
                                //                         ? Strings
                                //                             .pages
                                //                             .suitableApartmentsPage
                                //                             .makeOfferText
                                //                             .l(
                                //                                 context)
                                //                         : Strings
                                //                             .pages
                                //                             .suitableApartmentsPage
                                //                             .cancelOfferText
                                //                             .l(
                                //                                 context),
                                //                     style: Theme.of(
                                //                             context)
                                //                         .textTheme
                                //                         .button
                                //                         .merge(TextStyle(
                                //                             color: Colors.orange,
                                //                             fontSize: 16)))));
                                //       }
                                //       return Container();
                                //     }),

                                if ((widget?.offer?.offerId?.value == null && widget?.offerId == null) && (!datesIntersectionBool))
                                  ListTile(
                                      onTap: () {
                                        uploadRequestOffer(
                                            offer: widget.offer, requestInformation: widget.requestInformation, requestId: widget.requestId);
                                      },
                                      title: Align(
                                          alignment: Alignment.centerRight,
                                          child: Text(Strings.pages.suitableApartmentsPage.makeOfferText.l(context),
                                              style: Theme.of(context).textTheme.button.merge(TextStyle(color: Colors.orange, fontSize: 16))))),

                                if (datesIntersectionBool)
                                  ListTile(
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          CupertinoPageRoute(
                                            builder: (context) => RequestCreate(
                                              requestId: widget.requestId,
                                            ),
                                          ),
                                        ).then((v) {
                                          setState(() {});
                                        });
                                      },
                                      title: Align(
                                          alignment: Alignment.centerRight,
                                          child: Text('Изменить данные заявки',
                                              style: Theme.of(context).textTheme.button.merge(TextStyle(color: Colors.orange, fontSize: 16)))))

                                // ListTile(
                                //     onTap: () {
                                //       if (widget?.offer?.offerId?.value == null) {
                                //         uploadRequestOffer(
                                //             offer: widget.offer,
                                //             ownerId: widget.user.key,
                                //             houseId: widget.house.key,
                                //             requestInformation: widget.requestInformation,
                                //             houseInformation: widget.house.houseInformation);
                                //       } else {
                                //         declineRequestOffer(widget.offer);
                                //       }
                                //     },
                                //     title: Align(
                                //         alignment: Alignment.centerRight,
                                //         child: Text(
                                //             (widget?.offer?.offerId?.value == null)
                                //                 ? Strings.pages.suitableApartmentsPage.makeOfferText.l(context)
                                //                 : Strings.pages.suitableApartmentsPage.cancelOfferText.l(context),
                                //             style: Theme.of(context).textTheme.button.merge(TextStyle(color: Colors.orange, fontSize: 16)))))
                              ])),
                          if (widget.bookingIntersectionMessage != null)
                            Column(
                              children: [
                                Center(child: Text('${widget.bookingIntersectionMessage}', style: TextStyle(color: Colors.red))),
                                ListTile(
                                  title: Center(
                                    child: Text(
                                      Strings.pages.requestOfferPage.edit.l(context),
                                      style: TextStyle(
                                        color: Theme.of(context).primaryColor,
                                      ),
                                    ),
                                  ),
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      CupertinoPageRoute(
                                        builder: (context) => RequestCreate(firstLoad: true, requestId: widget.requestId),
                                      ),
                                    );
                                  },
                                )
                              ],
                            ),
/*
                          FutureBuilder(
                              future: datesIntersectionMessage(
                                  widget.requestInformation.datesRange,
                                  widget.offer),
                              builder: (context, snapshot) {
                                if (snapshot.connectionState == ConnectionState.done && !snapshot.hasError) {

                                }
                                return Container();
                              }),
*/
                          SizedBox(height: 8)
                        ])))),
            /*
                if (!house.info
                    .possibleToBook(CurrentRequestWidget
                    .of(context)
                    .request))
                  Padding(
                      padding: const EdgeInsets.only(top: 8.0).add(
                          EdgeInsets.symmetric(horizontal: 15)
                      ),
                      child: Card(
                          child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Text(
                                        Strings.pages.requestOfferPage.datesIssueApologizing.l(context)
                                            Strings.pages.requestOfferPage.bookingMessage.l(context),
                                        style: Theme
                                            .of(context)
                                            .textTheme
                                            .bodyText1
                                    ),
                                    Wrap(
                                        alignment: WrapAlignment.start,
                                        crossAxisAlignment: WrapCrossAlignment
                                            .start,
                                        children: [
                                          Chip(label: Text('23.05.23-23.06.23'))
                                        ]
                                    ),
                                    Text(
                                        Strings.pages.requestOfferPage.wantToChangeDates.l(context),
                                        style: Theme
                                            .of(context)
                                            .textTheme
                                            .subtitle2,
                                        textAlign: TextAlign.start
                                    ),
                                    Align(
                                        alignment: Alignment.bottomRight,
                                        child: FlatButton(
                                            child: Text(Strings.pages.requestOfferPage.yes.l(context)),
                                            onPressed: () {
                                              Navigator.push(
                                                  context,
                                                  CupertinoPageRoute(
                                                      builder: (_) =>
                                                          RequestCreate(
                                                              requestId:
                                                              CurrentRequestWidget
                                                                  .of(context)
                                                                  .request
                                                                  .requestId
                                                          )
                                                  )
                                              ).then((v) {
                                                mindClass.updateEvent(
                                                    'update_suitable_apartments_page');
                                              });
                                            }
                                        ))
                                  ]
                              )
                          )
                      )
                  )

                 */
          ]));
    }

    return houseElement(widget.houseInformation, widget.photosList, true);
/*
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(16))),
            clipBehavior: Clip.antiAlias,
            child: houseElement(widget.houseInformation, true)));

 */
  }
}
