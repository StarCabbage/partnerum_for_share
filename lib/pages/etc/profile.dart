import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:partnerum/head_module/gen_extensions/generated_str_extension.dart';

class Notifications extends StatefulWidget {
  @override
  _NotificationsState createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Title')),
        body: ListView(children: <Widget>[
          messages(),
          newApplications(),
          yourApartments(),
          yourApplications()
        ]));
  }

  Widget messages() {
    return Container(
        margin: EdgeInsets.all(4),
        width: double.infinity,
        height: 215,
        child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Column(children: <Widget>[
              Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                        padding: EdgeInsets.only(left: 10, top: 10),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(Strings.pages.notificationPage.messageNotifications.messages.l(context),
                                      style: TextStyle(fontSize: 20))),
                              SizedBox(height: 15),
                              Text('${Strings.pages.notificationPage.messageNotifications.message.l(context)} 1',
                                  style: TextStyle(
                                      fontSize: 15, color: Colors.black54)),
                              SizedBox(height: 5),
                              Text('${Strings.pages.notificationPage.messageNotifications.message.l(context)} 2',
                                  style: TextStyle(
                                      fontSize: 15, color: Colors.black54)),
                              SizedBox(height: 5),
                              Text('${Strings.pages.notificationPage.messageNotifications.message.l(context)} 3',
                                  style: TextStyle(
                                      fontSize: 15, color: Colors.black54))
                            ])),
                    Align(
                        alignment: Alignment.topRight,
                        child: Padding(
                            padding:
                                const EdgeInsets.only(top: 16, right: 16.0),
                            child: Container(
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle, color: Colors.blue),
                                height: 100,
                                width: 100)))
                  ]),
              SizedBox(height: 20),
              Divider(color: Colors.grey, height: 5),
              Expanded(
                  child: FlatButton(
                      onPressed: () {},
                      child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(Strings.pages.notificationPage.messageNotifications.openMore.l(context),
                              style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.orange,
                                  fontWeight: FontWeight.bold)))))
            ])));
  }

  Widget newApplications() {
    return Container(
        margin: EdgeInsets.all(4),
        width: double.infinity,
        height: 215,
        child: Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            child: Column(children: <Widget>[
              Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                        padding: EdgeInsets.only(left: 10, top: 10),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(Strings.pages.notificationPage.applicationsNotifications.newRequests.l(context),
                                      style: TextStyle(fontSize: 20))),
                              SizedBox(height: 15),
                              Text(Strings.pages.notificationPage.applicationsNotifications.moscowNeed.l(context),
                                  style: TextStyle(
                                      fontSize: 15, color: Colors.black54)),
                              SizedBox(height: 5),
                              Text(Strings.pages.notificationPage.applicationsNotifications.spbNeed.l(context),
                                  style: TextStyle(
                                      fontSize: 15, color: Colors.black54))
                            ])),
                    Align(
                        alignment: Alignment.topRight,
                        child: Padding(
                            padding:
                                const EdgeInsets.only(top: 16, right: 16.0),
                            child: Container(
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.purple),
                                height: 100,
                                width: 100)))
                  ]),
              SizedBox(height: 20),
              Divider(color: Colors.grey, height: 5),
              Expanded(
                  child: FlatButton(
                      onPressed: () {},
                      child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(Strings.pages.notificationPage.applicationsNotifications.allRequests.l(context),
                              style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.orange,
                                  fontWeight: FontWeight.bold)))))
            ])));
  }

  Widget yourApartments() {
    return Container(
        margin: EdgeInsets.all(4),
        width: double.infinity,
        height: 287,
        child: Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Padding(
                            padding: EdgeInsets.only(left: 10, top: 10),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Align(
                                      alignment: Alignment.topLeft,
                                      child: Text(Strings.pages.notificationPage.apartmentsNotifications.yourFlats.l(context),
                                          style: TextStyle(fontSize: 20))),
                                  SizedBox(height: 15),
                                  Text(Strings.pages.notificationPage.apartmentsNotifications.moscowNeed.l(context),
                                      style: TextStyle(
                                          fontSize: 15, color: Colors.black54)),
                                  SizedBox(height: 5),
                                  Text(Strings.pages.notificationPage.apartmentsNotifications.spbNeed.l(context),
                                      style: TextStyle(
                                          fontSize: 15, color: Colors.black54))
                                ])),
                        Align(
                            alignment: Alignment.topRight,
                            child: Padding(
                                padding:
                                    const EdgeInsets.only(top: 16, right: 16.0),
                                child: Container(
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Colors.green),
                                    height: 100,
                                    width: 100)))
                      ]),
                  Padding(
                      padding: const EdgeInsets.only(left: 10, top: 35.0),
                      child: Container(
                          width: 290,
                          child: Text(Strings.pages.notificationPage.apartmentsNotifications.allFlatsBooked.l(context),
                              style: TextStyle(
                                  fontSize: 16, color: Colors.black)))),
                  SizedBox(height: 20),
                  Divider(color: Colors.grey, height: 5),
                  Expanded(
                      child: FlatButton(
                          onPressed: () {},
                          child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(Strings.pages.notificationPage.apartmentsNotifications.management.l(context),
                                  style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.orange,
                                      fontWeight: FontWeight.bold)))))
                ])));
  }

  Widget yourApplications() {
    return Container(
        margin: EdgeInsets.all(4),
        width: double.infinity,
        height: 215,
        child: Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            child: Column(children: <Widget>[
              Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                        padding: EdgeInsets.only(left: 10, top: 10),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(Strings.pages.notificationPage.yourApplicationsNotifications.yourRequests.l(context),
                                      style: TextStyle(fontSize: 20))),
                              SizedBox(height: 15),
                              Text(Strings.pages.notificationPage.yourApplicationsNotifications.moscowNeed.l(context),
                                  style: TextStyle(
                                      fontSize: 15, color: Colors.black54)),
                              SizedBox(height: 5),
                              Text(Strings.pages.notificationPage.yourApplicationsNotifications.spbNeed.l(context),
                                  style: TextStyle(
                                      fontSize: 15, color: Colors.black54))
                            ])),
                    Align(
                        alignment: Alignment.topRight,
                        child: Padding(
                            padding:
                                const EdgeInsets.only(top: 16, right: 16.0),
                            child: Container(
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle, color: Colors.grey),
                                height: 100,
                                width: 100)))
                  ]),
              SizedBox(height: 20),
              Divider(color: Colors.grey, height: 5),
              Expanded(
                  child: FlatButton(
                      onPressed: () {},
                      child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(Strings.pages.notificationPage.yourApplicationsNotifications.edit.l(context),
                              style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.orange,
                                  fontWeight: FontWeight.bold)))))
            ])));
  }
}
