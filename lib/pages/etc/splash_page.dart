import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class SplashPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Scaffold(
        body: Center(
          child: SvgPicture.asset('assets/images/logo.svg',
              semanticsLabel: 'Partnerum'),
        )
      )
    );
  }
}
