import 'dart:async';
import 'package:db_partnerum/db_partnerum.dart';
import 'package:micro_utils/data_helper.dart';
import 'package:partnerum/utils/brain_module.dart';

@DataHelper()
class HomeHelper {
  static Future chatUsers() async {
    // var support = AvailableUser();
    // support.userInformation.uid = 'support';
    // support.userInformation.role = 'support';
    // support.userInformation.username = 'support';
    var avUsers = root.userData[mindClass.user.uid].availableUsers.future.then((value) {
      var avUsersList = value.values.toList();
      // avUsersList.add(support);
      return avUsersList;
    });
    return avUsers;
  }

  static FutureOr<String> userByID(String userID) async {
    var userInfo = await root.userData[userID].userInformation.username.future;
    return userInfo.value;
  }

  //
  // static String idByIndex(DbList<DbString> users, int index){
  //   print('id: ${users.values.elementAt(index).value}');
  //   return users.values.elementAt(index).value;
  // }

  static String nameByIndex(List<AvailableUser> users, int index) {
    return users.elementAt(index).userInformation.username.value;
  }

  static Duration lastOnlineByIndex(List<AvailableUser> users, int index) {
    return users.elementAt(index).userInformation.lastOnline.value;
  }

  static String idByIndex(List<AvailableUser> users, int index) {
    return users.elementAt(index).userInformation.uid.value;
  }

  static String roleByIndex(List<AvailableUser> users, int index) {
    var role = users.elementAt(index).userInformation.role.value;
    if (role == null) {
      return 'user';
    }
    return role;
  }

  static Future<String> newChat(String companion, String companionName) async {
    var newChat = root.chatTopic.userToUser.push;
    await newChat.header.type.set('user to user');
    newChat.users['0'] = (companion);
    newChat.users['1'] = (mindClass.user.uid);
    newChat.cid = newChat.key;
    // TODO исправить строку ниже. Это временное решение
    /*
    var newUserChat = root.userData[mindClass.user.uid].userChats.utu[newChat.key];
    await newUserChat.status.set(newChat.key);
    newUserChat.companionId = companion;
    newUserChat.companionName = 'MEME NAME';
    var newCompanionChat = root.userData[companion].userChats.utu[newChat.key];
    await newCompanionChat.status.set(newChat.key);
    newCompanionChat.companionId = mindClass.user.uid;
    newCompanionChat.companionName = 'AaAaA NaMe';
    // await newChat.upload();
    // print('&&&&&&&&&& ${newChat.key}');

     */
    return newChat.key;
  }
}

@DataHelper()
class BottomHelper {
  numberOfUnreadMessages() {
    return root.userData[mindClass.user.uid].unreadChats.stream.map((event) => event?.keys?.toList() ?? []);
  }
}
