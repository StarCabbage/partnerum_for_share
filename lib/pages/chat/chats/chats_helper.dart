import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:micro_utils/data_helper.dart';
import 'package:db_partnerum/db_partnerum.dart';
import 'package:partnerum/head_module/gen_extensions/generated_str_extension.dart';
import 'package:partnerum/utils/brain_module.dart';
import 'package:partnerum/head_module/gen_extensions/generated_files_extension.dart';

@DataHelper()
class ChatsHelper {
  List<String> utuList = [];
  List<String> otrList = [];

  Stream<UserChats> chats() {
    // var t = root.userData[mindClass.user.uid].chats.userToUser.push;
    // t.header = 'X';
    // print (t.runtimeType);
    // t.upload();
    // var s = root.userData[mindClass.user.uid].chats.userToUser.future;
    // print('s type: ${s.runtimeType}');
    // return s;
    // return root.userData[mindClass.user.uid].chats.userToUser.future;

    return root.userData[mindClass.user.uid].userChats.futureBasedStream;
    /*
    var allChats = await root.userData[mindClass.user.uid].userChats.future;
    var utu = allChats.utu?.values?.map((e) {
      utuList.add(e.key);
      return e;
    })?.toList() ?? [];

    // var uts = allChats.uts?.values?.map((e) => e)?.toList() ?? [];
    var otr = allChats.otr?.values?.map((e) {
      otrList.add(e.key);
      return e;
    })?.toList() ?? [];

    var result = [];
    result.addAll(utu);

    // result.addAll(uts);
    result.addAll(otr);

    // var chatsTypedList = await Future.wait([utu, uts, otr]);
    // print('utu: ${utu}');
    // print('uts: ${uts}');
    // print('otr: ${otr}');
    // for (var chatList in chatsTypedList){
    //   result.addAll(chatList);
    // }

    return result;

     */
  }

  Future<String> companionName(String chatID) async {
    // var users = await root.chatTopic.userToUser[chatID].users.future
    //     .then((value) => value.values.toList());
    /*
    var users = (await root.chatTopic.userToUser[chatID].users.future);
    print('users: ${users}');

     */
    // var companionID = '';
    // if (users[0].value == mindClass.user.uid) {
    //   companionID = users[1].value;
    // } else {
    //   companionID = users[0].value;
    // }
    var userName = (await root.userData['QCyzVldODYZdxZbVh7Ygj9siIZ22']
            .userInformation.username.future)
        .value;
    print(userName);
    return userName;
  }

  Future<List<UserInformation>> interlocutorNames(List interlocutorsId) async {
    var interlocutorsList = <UserInformation>[];
    // return interlocutorsList;
    for (var interlocutorId in interlocutorsId) {
      if (interlocutorId != null) {
        interlocutorsList.add((await root
                .userData[interlocutorId].userInformation.future));
      } else {
        interlocutorsList.add(null);
      }
    }
    return interlocutorsList;
  }

  Future<List> companionNames(List ids) async {
    var result = [];

    for (var id in ids) {
      if (id != null) {
        var usersID = (await root.chatTopic.userToUser[id].users.future
            .then((value) => value.values.map((e) => e.key).toList()));
        // var usersID = await root.chatTopic.userToUser[id].users.future;

        // var usersID = chatUsers.values.toList();

        var companionID = '';
        if (usersID[0] != mindClass.user.uid) {
          companionID = usersID[0];
        } else {
          companionID = usersID[1];
        }

        var userName =
            (await root.userData[companionID].userInformation.username.future)
                .value;
        result.add(userName);
      } else {
        result.add(null);
      }
    }

    return result;
  }

  Stream unreadChatsStream() {
    return root.userData[mindClass.user.uid].unreadChats.stream
        .asBroadcastStream();
  }

  SliverToBoxAdapter noChatsFound(BuildContext context) {
    return SliverToBoxAdapter(
        child: Padding(
            padding: const EdgeInsets.all(24.0),
            child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
              Container(
                  width: 300,
                  height: 300,
                  child: SvgPicture.asset(files.images.undraw.messagingSvg)),
              // Container(width: 300, height: 300, child: UnDraw(illustration: UnDrawIllustration.messaging, color: Theme.of(context).primaryColor)),
              ListTile(
                  title: Text(
                      Strings.pages.chatsPage.chatsHelper.noChats.l(context),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.grey,
                          fontSize: 14.0,
                          fontWeight: FontWeight.bold)))
            ])));
  }

  SliverToBoxAdapter loadingChats(context) {
    return SliverToBoxAdapter(
        child: Container(
            alignment: FractionalOffset.center,
            child: Center(
                child: Theme.of(context).platform == TargetPlatform.iOS
                    ? CupertinoActivityIndicator()
                    : CircularProgressIndicator())));
  }

  void enterInChat(String chatId) {
    root.userData[mindClass.user.uid].activeChats[chatId].set(true);
    if (utuList.contains(chatId)) {
      root.userData[mindClass.user.uid].userChats.utu[chatId].status.set('read');
    }
    else{
      root.userData[mindClass.user.uid].userChats.otr[chatId].status.set('read');
    }
    root.userData[mindClass.user.uid].unreadChats[chatId].remove();
  }

  Widget decoratedImage(ImageProvider imageProvider) {
    return Container(
        width: 50,
        height: 50,
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
                image: imageProvider,
                fit: BoxFit.cover,
                colorFilter:
                ColorFilter.mode(Colors.white, BlendMode.colorBurn))));
  }

  Widget noImageIcon(BuildContext context) {
    return Container(
        width: 50,
        height: 50,
        decoration: BoxDecoration(
            border: Border.all(color: Theme.of(context).primaryColor),
            shape: BoxShape.circle),
        child: Icon(Icons.info_outline, color: Theme.of(context).primaryColor));
  }

  Widget noDatePlug(BuildContext context) {
    // 0000.00.00 00.00
   return Text('Нет сообщений',
        style: Theme.of(context).textTheme.caption);
  }

  Future<DbObj> loadImageFuture(String userId) {
    return root.userData[userId].userInformation.avatar.future;
  }

  Future timestampFuture(String chatId){
    return root.chatTopic.userToUser[chatId].timestamp.future;
  }

  Future<DbString> usernameLoaderFuture(String userId){
    return root.userData[userId].userInformation.username.future;
  }

}
