import 'package:db_partnerum/db_partnerum.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:partnerum/head_module/gen_extensions/generated_str_extension.dart';
import 'package:partnerum/models/status_model.dart';

// import 'package:partnerum/models/requests_model.dart';
import 'package:partnerum/orders/orders/orders_page.dart';
import 'package:partnerum/pages/house/house_offers/house_offers_helper.dart';
import 'package:partnerum/pages/request/request_offer/request_offer_page.dart';
import 'package:partnerum/utils/brain_module.dart';
import 'package:partnerum/utils/theme_constants.dart';
import 'package:partnerum/widgets/house_card/house_card_widget.dart';
import 'package:shimmer/shimmer.dart';

// import '../request/request_offer_card.dart';
// import '../request/request_offer/request_offer_page.dart';
import 'notifications/notifications_helper.dart';

enum InstanceType { house, request, none }

class InstancePage extends StatefulWidget {
  final String offerObjectId;
  final UserNotification message;

  // or
  final String messageId;

  InstancePage({Key key, @required this.offerObjectId, this.message, this.messageId}) : super(key: key);

  @override
  _InstancePageState createState() => _InstancePageState();
}

class _InstancePageState extends State<InstancePage> with TickerProviderStateMixin, NotificationHelper, HouseOffersHelper {
  String userType;
  OfferObject offerObject;

  final ValueNotifier<UserNotification> _message = ValueNotifier<UserNotification>(null);

  OfferObject offerToRoom;

  @override
  void initState() {
    offerToRoom = root?.chatTopic?.offer2Room[widget?.offerObjectId];

    if (widget?.message != null) {
      _message.value = widget.message;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: RefreshIndicator(
            displacement: 80,
            onRefresh: () {
              return Future.delayed(Duration(milliseconds: 300));
            },
            child: FutureBuilder(
                future: notificationById(widget?.messageId),
                builder: (context, notificationSnapshot) {
                  if (notificationSnapshot != null && notificationSnapshot.hasData && !notificationSnapshot.hasError) {
                    _message.value = notificationSnapshot?.data;
                  }

                  return StreamBuilder(
                      stream: offerToRoom?.stream?.asBroadcastStream(),
                      initialData: offerToRoom,
                      builder: (context, snapshot) {
                        offerObject = snapshot?.data;
                        userType = currentUserType(offerObject);
                        print('StreamBuilder: streamBuilder');
                        return ScrollConfiguration(
                            behavior: noHighlightScrollBehavior(context),
                            child: CustomScrollView(slivers: [
                              SliverAppBar(pinned: true, title: Text('Подробнее' ?? Strings.pages.messageInstancePage.moreTopicDetails.l(context))),
                              // SliverAppBar(
                              //     pinned: true,
                              //     title: Text(_message
                              //             ?.value?.notification?.title?.value ??
                              //         Strings.pages.messageInstancePage
                              //             .moreTopicDetails
                              //             .l(context))),
                              if (widget?.messageId != null || widget?.message != null)
                                SliverToBoxAdapter(
                                    child: AnimatedSize(
                                        vsync: this,
                                        duration: kThemeAnimationDuration,
                                        child: AnimatedSwitcher(
                                            duration: kThemeAnimationDuration,
                                            child: _message.value != null
                                                ? Padding(
                                                    padding: const EdgeInsets.only(top: 8, left: 16, right: 16),
                                                    child: Card(
                                                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16))),
                                                        clipBehavior: Clip.antiAlias,
                                                        child: ListTile(
                                                            title: Row(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                                              Flexible(
                                                                child: Text(
                                                                  notificationTitleByObject(_message.value, context),
                                                                  overflow: TextOverflow.ellipsis,
                                                                  /*
                                                                      notifications[index]
                                                                      .getHumanReadableType()

                                                                       */
                                                                ),
                                                              ),
                                                              SizedBox(width: 8),
                                                              Text(DateFormat('yyyy.MM.dd HH:mm').format(DateTime.fromMillisecondsSinceEpoch(
                                                                  /*
                                                                                notifications[
                                                                                        index]
                                                                                    .timestamp

                                                                       */
                                                                  notificationTimeByObject(_message.value))), style: Theme.of(context).textTheme.caption)
                                                            ]),
                                                            contentPadding: EdgeInsets.all(8),
                                                            subtitle: Padding(
                                                              padding: const EdgeInsets.only(top: 4.0),
                                                              child: Column(mainAxisSize: MainAxisSize.min, children: [
                                                                Text(notificationBodyByObject(_message.value, context)),
                                                                SizedBox(height: 8.0),
                                                                // if(_message
                                                                //     ?.value
                                                                //     ?.notification
                                                                //     ?.image
                                                                //     ?.value !=
                                                                //     null &&
                                                                //     userType ==
                                                                //         'ownerUser')

                                                                // AnimatedSize(
                                                                //     duration:
                                                                //         kThemeAnimationDuration,
                                                                //     vsync:
                                                                //         this,
                                                                //     child:
                                                                //         AnimatedSwitcher(
                                                                //       duration:
                                                                //           kThemeAnimationDuration,
                                                                //       child: _message.value.notification.image.value == null ||
                                                                //               userType == null
                                                                //           ? Shimmer.fromColors(baseColor: Colors.grey[300], highlightColor: Colors.grey[100], child: ClipRRect(borderRadius: BorderRadius.circular(16), child: Container(height: 125, color: Colors.black)))
                                                                //           : userType == 'ownerUser'
                                                                //               ? ClipRRect(borderRadius: BorderRadius.circular(16), child: CachedNetworkImage(imageUrl: _message.value.notification.image.value, imageBuilder: (context, imageProvider) => Container(height: 125, decoration: BoxDecoration(image: DecorationImage(image: imageProvider, fit: BoxFit.cover)))))
                                                                //               : SizedBox.shrink(),
                                                                //     ))

                                                                //   AnimatedSize(
                                                                //       duration: kThemeAnimationDuration,
                                                                //       vsync: this,
                                                                //       child: userType ==
                                                                //           'requestUser' ? SizedBox.shrink() :
                                                                //               notificationImage !=
                                                                //           null
                                                                //           ? ClipRRect(
                                                                //           borderRadius: BorderRadius
                                                                //               .circular(
                                                                //               16),
                                                                //           child: CachedNetworkImage(
                                                                //               imageUrl: notificationImage,
                                                                //               imageBuilder: (
                                                                //                   context,
                                                                //                   imageProvider) =>
                                                                //                   Container(
                                                                //                       height: 125,
                                                                //                       decoration: BoxDecoration(
                                                                //                           image: DecorationImage(
                                                                //                               image: imageProvider,
                                                                //                               fit: BoxFit
                                                                //                                   .cover)))
                                                                //           ))
                                                                //           : Shimmer
                                                                //           .fromColors(
                                                                //           baseColor: Colors
                                                                //               .grey[300],
                                                                //           highlightColor: Colors
                                                                //               .grey[100],
                                                                //           child: ClipRRect(
                                                                //               borderRadius: BorderRadius
                                                                //                   .circular(
                                                                //                   16),
                                                                //               child: Container(
                                                                //                   height: 125,
                                                                //                   color: Colors
                                                                //                       .black))
                                                                //       )
                                                                //   )
                                                              ]),
                                                            ))))
                                                : Padding(
                                                    padding: const EdgeInsets.only(top: 8, left: 16, right: 16),
                                                    child: Shimmer.fromColors(
                                                      baseColor: Colors.grey[300],
                                                      highlightColor: Colors.grey[100],
                                                      child: Card(
                                                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16))),
                                                          clipBehavior: Clip.antiAlias,
                                                          child: ListTile(
                                                              title: Row(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                                                Flexible(
                                                                    child: Text(
                                                                  '_________',
                                                                  overflow: TextOverflow.ellipsis,
                                                                )),
                                                              ]),
                                                              contentPadding: EdgeInsets.all(8),
                                                              subtitle: Padding(
                                                                padding: const EdgeInsets.only(top: 4.0),
                                                                child: Column(mainAxisSize: MainAxisSize.min, children: [
                                                                  Text(
                                                                      'Пользователь ____________ подал заявку на вашу квартиру _________ _____ ____________ ______ ________ ________ ________ ________'),
                                                                  // Padding(
                                                                  //     padding: const EdgeInsets
                                                                  //             .only(
                                                                  //         top:
                                                                  //             8.0),
                                                                  //     child: ClipRRect(
                                                                  //         borderRadius: BorderRadius.circular(
                                                                  //             16),
                                                                  //         child: Container(height: 125,width: 0,color:Colors.transparent)))
                                                                ]),
                                                              ))),
                                                    ))))),
                              SliverToBoxAdapter(
                                child: AnimatedSize(
                                  duration: kThemeAnimationDuration,
                                  vsync: this,
                                  child: AnimatedSwitcher(
                                      duration: kThemeAnimationDuration,
                                      child:
                                          // (snapshot != null &&
                                          //         snapshot.hasData &&
                                          //         !snapshot.hasError)
                                          offerObject != null
                                              ? Padding(
                                                  padding: const EdgeInsets.only(top: 8, left: 16, right: 16),
                                                  child: Card(
                                                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16))),
                                                      child: ListTile(
                                                          contentPadding: EdgeInsets.all(8),
                                                          subtitle: Column(children: [
                                                            Text(
                                                                '${Strings.pages.messageInstancePage.yourStatus.l(context)}: ${StatusModel.convertStatus(yourStatus(offerObject)).toLowerCase()}'),
                                                            SizedBox(height: 4),
                                                            Text(
                                                                '${userType == 'ownerUser' ? Strings.pages.messageInstancePage.requestorStatus.l(context) : Strings.pages.messageInstancePage.ownerStatus.l(context)}: ${StatusModel.convertStatus(oppositeUserStatus(offerObject)).toLowerCase()}')
                                                          ]))))
                                              : Shimmer.fromColors(
                                                  baseColor: Colors.grey[300],
                                                  highlightColor: Colors.grey[100],
                                                  child: Padding(
                                                      padding: EdgeInsets.only(top: 8, left: 16, right: 16),
                                                      child: Card(
                                                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16))),
                                                          child: ListTile(
                                                              contentPadding: EdgeInsets.all(8),
                                                              subtitle: Column(children: [
                                                                Text('${Strings.pages.messageInstancePage.yourStatus.l(context)}: заявка подана'),
                                                                SizedBox(height: 4),
                                                                Text('${Strings.pages.messageInstancePage.ownerStatus.l(context)}: заявка одобрена')
                                                              ])))),
                                                )),
                                ),
                              ),
                              if (userType == 'ownerUser')
                                SliverToBoxAdapter(
                                    child: Padding(
                                        padding: const EdgeInsets.only(left: 16, right: 16, bottom: 8),
                                        child: FutureBuilder(
                                            future: db?.root?.userData[requestorId(offerObject)].future,
                                            builder: (context, snapshot) {
                                              var requestorData = snapshot?.data;
                                              return AnimatedSize(
                                                duration: kThemeAnimationDuration,
                                                vsync: this,
                                                child: AnimatedSwitcher(
                                                    duration: kThemeAnimationDuration,
                                                    child: (snapshot != null && snapshot.hasData && !snapshot.hasError)
                                                        ? HouseRequestCard(offerObject: offerObject, requestUser: requestorData)
                                                        : Padding(padding: const EdgeInsets.only(top: 22.0), child: loadingShimmer(300))),
                                              );
                                            }))),
                              if (userType == 'ownerUser')
                                SliverToBoxAdapter(
                                    child: Padding(
                                  padding: const EdgeInsets.only(left: 16, right: 16, top: 4, bottom: 24),
                                  child: FutureBuilder(
                                      future: root.userData[mindClass.user.uid].houses[ownerId(offerObject)[1]].future,
                                      builder: (context, houseSnapshot) {
                                        return FutureBuilder(
                                            future: photosList(offerObject?.houseCopy),
                                            builder: (context, photosData) {
                                              return AnimatedSwitcher(
                                                  duration: kThemeAnimationDuration,
                                                  child: houseSnapshot.data != null && photosData.data != null
                                                      ? HouseCard(house: houseSnapshot.data, photos: photosData.data, noEdit: true)
                                                      : Padding(padding: const EdgeInsets.all(4), child: loadingShimmer(270)));
                                            });
                                      }),
                                )),
                              if (userType == 'requestUser')
                                SliverToBoxAdapter(
                                    child: Padding(
                                        padding: const EdgeInsets.only(top: 8, bottom: 8),
                                        child: FutureBuilder(
                                            future: root?.userData[ownerId(offerObject)?.first].userInformation.future,
                                            builder: (context, userSnapshot) {
                                              return FutureBuilder(
                                                  future: photosList(offerObject?.houseCopy),
                                                  builder: (context, photosData) {
                                                    return AnimatedSize(
                                                      duration: kThemeAnimationDuration,
                                                      vsync: this,
                                                      child: AnimatedSwitcher(
                                                          duration: kThemeAnimationDuration,
                                                          child: (snapshot != null &&
                                                                  userSnapshot != null &&
                                                                  photosData != null &&
                                                                  snapshot.hasData &&
                                                                  userSnapshot.hasData &&
                                                                  photosData.hasData &&
                                                                  !snapshot.hasError)
                                                              ? RequestOffer(
                                                                  houseInformation: offerObject.houseCopy,
                                                                  userInformation: userSnapshot.data,
                                                                  requestInformation: offerObject.requestCopy,
                                                                  conversationUsers: offerObject.conversationUsers,
                                                                  photosList: photosData.data,
                                                                  userId: ownerId(offerObject)[0],
                                                                  houseId: ownerId(offerObject)[1],
                                                                  offerId: offerObject.key,
                                                                  bookedDates: [],
                                                                )
                                                              : Padding(
                                                                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                                                                  child: loadingShimmer(400))),
                                                    );
                                                  });
                                            }))),
                            ]));
                      });
                  ;
                })));
  }

  Shimmer loadingShimmer(double shimmerHeight) {
    return Shimmer.fromColors(
        baseColor: Colors.grey[300],
        highlightColor: Colors.grey[100],
        child: Container(decoration: BoxDecoration(borderRadius: BorderRadius.circular(16), color: Colors.black), height: shimmerHeight));
  }

// RequestOfferCard getCard(BuildContext buildContext,
//     [isReady = false, type, House house, Request request]) {
//   //if(idSplit[0] == mindClass.user.uid) return Container();
//   /*
//   return RequestOfferCard(
//       future: mindClass.userHouseByMessageInstance(messageInstanceId),
//       messId: messageInstanceId,
//       rType: type,
//       isReady: isReady,
//       show: (v) async {
//         if (isReady) {
//           await mindClass.offerRequest(
//               '', '', '', messageInstanceId, house, request);
//           return;
//         }
//         await mindClass.offerRequest(
//             '', '', '', messageInstanceId, house, request);
//       });
//
//    */
// }
}

//                   SliverToBoxAdapter(
//                       child: StreamBuilder(
//                           stream: mindClass.eventSteam.outStream,
//                           builder:
//                               (BuildContext context, AsyncSnapshot snapshot) {
//                             /*
//                             return FutureBuilder(
//                                 future: mindClass
//                                     .getInfoFromHeader(messageInstanceId),
//                                 builder: (BuildContext context,
//                                     AsyncSnapshot snapshot) {
//                                   if (snapshot.hasData) {
//                                     List listOfData = snapshot.data;
//                                     var housesRequest = HousesRequest(
//                                         null, null, messageInstanceId);
//                                     housesRequest.status = listOfData[2].status;
//                                     housesRequest.requestStateType =
//                                         listOfData[0];
//                                     housesRequest.request = listOfData[1];
//                                     var header = notification?.header;
//                                     if (header == null) {
//                                       header = Header(messageInstanceId);
//                                       header.house = listOfData[2];
//                                       header.request = listOfData[1];
//                                       header.status = listOfData[2].status;
//                                     }
//                                     return Column(
//                                         mainAxisSize: MainAxisSize.min,
//                                         crossAxisAlignment:
//                                             CrossAxisAlignment.start,
//                                         children: <Widget>[
//                                           if (notification != null &&
//                                               notification.body != null)
//                                             Padding(
//                                                 padding:
//                                                     const EdgeInsets.all(16.0),
//                                                 child: Container(
//                                                     decoration: BoxDecoration(
//                                                         border: Border.all(
//                                                             color:
//                                                                 Colors.black38,
//                                                             width: 1),
//                                                         borderRadius:
//                                                             BorderRadius.circular(
//                                                                 16)),
//                                                     child: Padding(
//                                                         padding:
//                                                             const EdgeInsets.all(
//                                                                 8.0),
//                                                         child: Text(
//                                                             notification.body ??
//                                                                 '')))),
//                                           if (notification == null ||
//                                               notification.body == null)
//                                             SizedBox(height: 16),
//                                           Padding(
//                                               padding:
//                                                   const EdgeInsets.symmetric(
//                                                       horizontal: 16),
//                                               child: Column(
//                                                   crossAxisAlignment:
//                                                       CrossAxisAlignment.start,
//                                                   mainAxisSize:
//                                                       MainAxisSize.min,
//                                                   children: <Widget>[
//                                                     Text(
//                                                         Strings.pages.messageInstancePage.flatCurrentState.l(context),
//                                                         style: Theme.of(context)
//                                                             .textTheme
//                                                             .title),
//                                                     SizedBox(height: 16),
//                                                     header.status.houseUid !=
//                                                             mindClass.user.uid
//                                                         ? HouseCard(
//                                                             house:
//                                                                 listOfData[2],
//                                                             noEdit: true)
//                                                         : HouseRequestCard(
//                                                             housesRequest:
//                                                                 housesRequest,
//                                                             backOff: false),
//                                                     SizedBox(height: 16),
//                                                     Text(
//                                                         Strings.pages.messageInstancePage.requestCurrentState.l(context),
//                                                         style: Theme.of(context)
//                                                             .textTheme
//                                                             .title),
// //
//                                                     header.status.houseUid ==
//                                                             mindClass.user.uid
//                                                         ? Padding(
//                                                             padding:
//                                                                 const EdgeInsets
//                                                                         .only(
//                                                                     top: 16.0),
//                                                             child: RequestCard(
//                                                                 editable: false,
//                                                                 justDemonstration:
//                                                                     true,
//                                                                 request:
//                                                                     housesRequest
//                                                                         .request))
//                                                         : CurrentRequestWidget(
//                                                             listOfData[1],
//                                                             child: RequestOffer(
//                                                                 house:
//                                                                     listOfData[
//                                                                         2],
//                                                                 messageInstanceId:
//                                                                     messageInstanceId,
//                                                                 user:
//                                                                     listOfData[
//                                                                         3]))
// //                                Text(
// //                                  Strings.pages.messageInstancePage.possibleActions.l(context),
// //                                  style: Theme.of(context).textTheme.title,
// //                                ),
// //                                SizedBox(
// //                                  height: 16,
// //                                ),
// //                                Text(
// //                                  listOfData[0],
// //                                  style: Theme.of(context).textTheme.title,
// //                                ),
//                                                   ]))
//                                         ]);
//                                   }
//                                   return SizedBox(
//                                       height: 2,
//                                       child: linearProgressIndicator);
//                                 });
//
//                              */
//                           }))
