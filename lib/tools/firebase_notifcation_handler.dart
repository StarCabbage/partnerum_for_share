import 'dart:async';
import 'package:another_flushbar/flushbar.dart';
import 'package:db_partnerum/db_partnerum.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:partnerum/pages/chat/messages/messages_page.dart';
import 'package:partnerum/pages/notifications/message_instance_page.dart';
import 'package:partnerum/pages/notifications/notifications/notifications_helper.dart';
import 'package:partnerum/pages/request/suitable_apartments/suitable_apartments_page.dart';
import 'package:partnerum/utils/brain_module.dart';

class FirebaseNotifications with NotificationHelper {
  final BuildContext _context;

  FirebaseNotifications(this._context) {
    setupNotifications();
  }

  AndroidNotificationChannel channel = AndroidNotificationChannel(
    'high_importance_channel', // id
    'High Importance Notifications', // title
    'This channel is used for important notifications.', // description
    importance: Importance.high,
  );

  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  FlutterLocalNotificationsPlugin();

  Future<void> setupNotifications() async {
    var token = await FirebaseMessaging.instance.getToken();
    await setUserToken(token);
    mindClass.currentNotificationToken = token;

    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
        AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);

    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
        alert: true, badge: true, sound: true);

    firebaseCloudMessagingListeners();
  }

  void firebaseCloudMessagingListeners() {
    // FirebaseMessaging.instance
    //     .getInitialMessage()
    //     .then((RemoteMessage message) {
    //   if (message != null) {
    //     print('Message 1 message 1 message 1');
    //     // Navigator.pushNamed(context, '/message',
    //     //     arguments: MessageArguments(message, true));
    //   }
    // });

    FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
      var notification = message.notification;
      var android = message.notification?.android;
      var notificationType = message.data['notificationType'];

      if (notification != null && android != null) {
        Future<void> showCustomFlushbar([Request requestObject]) async {
          await Flushbar(
              flushbarPosition: FlushbarPosition.TOP,
              title: notification.title,
              message: notification.body,
              margin: EdgeInsets.all(8),
              borderRadius: BorderRadius.circular(8),
              flushbarStyle: FlushbarStyle.FLOATING,
              icon: Icon(Icons.notifications_none,
                  size: 28.0, color: Theme.of(_context).primaryColor),
              duration: Duration(seconds: 8),
              // mainButton: FlatButton(
              //     onPressed: () {}, child: Text("Открыть".toUpperCase())),
              onTap: (data) async {

                launchNotification(message, requestObject);
              }).show(_context);
        }

        if (notificationType == 'accountRemove') {
          await launchAccountRemove(_context);
        } else if (notificationType == 'finishedApartmentsSearch') {
          var requestId = message.data['requestId'];
          var requestObject = await db
              .root.userData[mindClass.user.uid].requests[requestId].future;
          await showCustomFlushbar(requestObject);
        } else {
          await showCustomFlushbar();
        }
      }
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) async {
      var notificationType = message.data['notificationType'];

      if (notificationType == 'finishedApartmentsSearch') {
        var requestId = message.data['requestId'];
        var requestObject = await db
            .root.userData[mindClass.user.uid].requests[requestId].future;
        launchNotification(message, requestObject);
      } else {
        launchNotification(message);
      }
    });
  }

  void launchNotification(RemoteMessage message,
      [Request requestObject]) async {
    var notificationType = message.data['notificationType'];
    if (notificationType == 'status') {
      await launchStatusNotification(message, _context);
    } else if (notificationType == 'message') {
      await launchMessageNotification(message, _context);
    } else if (notificationType == 'finishedApartmentsSearch') {
      await launchFinishedApartmentsSearchNotification(_context, requestObject);
    } else if (notificationType == 'review') {
      await launchMessageNotification(message, _context);
    }
  }

  Future<void> launchStatusNotification(
      RemoteMessage message, BuildContext context) async {

    var notificationKey = message.data['notificationId'];
    var offerObjectkey = message.data['offerObjectId'];
    readNotificationByKey(notificationKey);

      await Navigator.push(
          context,
          CupertinoPageRoute(
              builder: (context) =>
                  InstancePage(
                      messageId:
                      notificationKey,
                      offerObjectId:
                      offerObjectkey)));
  }

  void enterInChat(String chatId) {
    root.userData[mindClass.user.uid].activeChats[chatId].set(true);
    root.userData[mindClass.user.uid].unreadChats[chatId].remove();
  }

  Future<void> launchMessageNotification(
      RemoteMessage message, BuildContext context) async {
    var chatId = message.data['chatId'];
    enterInChat(chatId);
    await Navigator.push(
        context,
        CupertinoPageRoute(
            builder: (context) => Messages(messageInstanceId: chatId)))
        .then((v) {
      mindClass.ramData['last_notification_instance'] = '';
    });
  }

  Future<void> launchFinishedApartmentsSearchNotification(
      BuildContext context, Request requestObject) async {
    await Navigator.push(
      context,
      CupertinoPageRoute(
          builder: (context) =>
              SearchingOfSuitableApartments(request: requestObject)),
    ).then((value) => mindClass.updateEvent('updateRequestScreen'));
  }

  Future<void> launchAccountRemove(BuildContext context) async {
    await removeToken();
    mindClass.user = null;
    await Navigator.pushNamedAndRemoveUntil(
        context, 'walk', (Route<dynamic> route) => false);
  }
}
