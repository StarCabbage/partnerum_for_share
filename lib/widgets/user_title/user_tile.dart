import 'package:cached_network_image/cached_network_image.dart';
import 'package:db_partnerum/db_partnerum.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:partnerum/head_module/gen_extensions/generated_str_extension.dart';

// import 'package:partnerum/models/houses_model.dart';
import 'package:partnerum/models/user_info_model.dart';
import 'package:partnerum/utils/theme_constants.dart';

import 'user_title_helper.dart';

class Rating {
  //Используется как чисто для юзера, так и чисто для дома конкретного.
  // Добавить возможность делать новости у себя на странице, которые будут распологаться после или перед ревью.
  final double rate;
  final int rateComments;
  final List<Review> reviews;

  Widget get humanReadableRating {
    if (rate != null) {
      return RichText(
          textAlign: TextAlign.start,
          text: TextSpan(
              text: '$rate',
              style:
                  TextStyle(color: Colors.orange, fontWeight: FontWeight.w400),
              children: [
                WidgetSpan(
                    child: RatingBar.builder(
                        initialRating: rate,
                        minRating: 0,
                        direction: Axis.horizontal,
                        allowHalfRating: true,
                        itemCount: 5,
                        ignoreGestures: true,
                        itemSize: 16,
                        itemBuilder: (context, _) =>
                            Icon(Icons.star, color: Colors.amber),
                        onRatingUpdate: (rating) {},
                        glow: false)),
                TextSpan(
                    text: '($rateComments)',
                    style: TextStyle(color: Colors.grey.shade500))
              ]));
    }
    return Text('Отзывы отсуствуют');
  }

/*
  static Rating fromJson(Strming rating, Map reviews) {
    var _rating = rating?.split('_');

    double _rate;
    int _rateComments;
    if (_rating != null) {
      _rate = double.parse(_rating[0]);
      _rateComments = int.parse(_rating[1]);
    }
    List<Review> _reviews;
    if (reviews != null) {
      _reviews = [];
      reviews.entries.forEach((element) {
        _reviews.add(Review.fromJson(element.value));
      });
    }
    return Rating(_rate, _rateComments, _reviews);
  }
*/
  Rating(this.rate, this.rateComments, [this.reviews = const []]);
}

class UserTile extends StatelessWidget with UserTitleHelper {
  final UserInformation userInformation;
  final String uid;
  final bool click;

  const UserTile({Key key, this.userInformation, this.uid, this.click = true})
      : super(key: key);

  // userByID(uid.split(';').first),
  @override
  Widget build(BuildContext context) {
    //var userInfo = userInformation?.userInformation;
    var userInfo = userInformation;
    // var documentStatus = userInformation?.document?.status?.value;
    var documentStatus = userInformation?.isConfirmed?.value ?? false;

    CachedNetworkImage userProfileImg;
    userProfileImg = CachedNetworkImage(
        imageUrl: userInfo.avatar.url,
        httpHeaders: userInfo.avatar.syncHttpHeaders,
        imageBuilder: (context, imageProvider) => decoratedImage(imageProvider),
        placeholder: (context, url) =>
            Theme.of(context).platform == TargetPlatform.iOS
                ? CupertinoActivityIndicator()
                : CircularProgressIndicator(),
        errorWidget: (context, url, error) => Container(
            width: 40,
            height: 40,
            decoration: BoxDecoration(
                border: Border.all(color: Theme.of(context).primaryColor),
                shape: BoxShape.circle),
            child: Center(
                child: Icon(Icons.info_outline,
                    color: Theme.of(context).primaryColor))));

    return ListTile(
        onTap: () {
          // if (click) openUserInfo(uid.split(';').first, user, context);
          if (click)
            openUserInfo(uid: uid, userInformation: userInfo, context: context);
        },
        contentPadding: EdgeInsets.symmetric(vertical: 5).copyWith(left: 16),
        title: Text.rich(TextSpan(children: <InlineSpan>[
          WidgetSpan(
              alignment: PlaceholderAlignment.top,
              child: Tooltip(
                  // message: '${user?.isConfirmedHumanReadable()}',
                  message: confirmCheck(userInfo),
                  child: Padding(
                    padding: const EdgeInsets.only(right: 4.0),
                    /*child: (user?.isConfirmed ?? false)*/
                    child: documentStatus == null ||
                            documentStatus == Keywords.inProgress.short
                        ? Icon(FontAwesomeIcons.userSecret,
                            size: 16, color: Colors.red)
                        : Icon(FontAwesomeIcons.userShield,
                            size: 16, color: Colors.green),
                    // child: (false)
                    //     ? Icon(FontAwesomeIcons.userShield,
                    //     size: 16, color: Colors.green)
                    //     : Icon(FontAwesomeIcons.userSecret,
                    //     size: 16, color: Colors.red)
                  ))),
          WidgetSpan(
              alignment: PlaceholderAlignment.top,
              child: SizedBox(
                width: 10,
              )),
          TextSpan(
              /*text: user.userInformation.username.value,*/
              text: userInfo?.username?.value ??
                  Strings.globalOnes.anonymousUser.l(context),
              style: Theme.of(context).textTheme.subtitle1.copyWith(
                  /*color: user.isUsernameExist() ? Colors.black : Colors.red)),*/
                  color: Colors.red)),
          /*if (DateTime.now().millisecondsSinceEpoch - user.lastOnline <= 60000)*/
          if (DateTime.now().millisecondsSinceEpoch -
                  userInfo.lastOnline.value.inMilliseconds <=
              60000)
            WidgetSpan(
                alignment: PlaceholderAlignment.top,
                child: ClipOval(
                    child: Container(
                        width: 8, height: 8, color: Colors.greenAccent)))
        ])),
        /*subtitle: user.rating.humanReadableRating,*/
        subtitle: Text((userInfo?.rating?.value != null
                ? 'Рейтинг: ' + userInfo?.rating?.value?.toStringAsFixed(2)
                : Strings.pages.extra.userTile.noMarks.l(context))
            .toString()),
        leading: userProfileImg
        // CircleAvatar(
        //   backgroundColor: Colors.orange.shade100,
        //   /*    backgroundImage: CachedNetworkImageProvider(user.userInformation.avatar.url)*/
        // )
        );
  }
}
