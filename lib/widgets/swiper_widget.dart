import 'package:flutter/material.dart';

import 'infinity_page_view.dart';

class Swiper extends StatefulWidget {
  final Widget pageWidget;

  const Swiper({Key key, this.pageWidget}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _Swiper();
}

class _Swiper extends State<Swiper> with SingleTickerProviderStateMixin {
  bool inited = false;

  mInfinityPageController infinityPageController =
      mInfinityPageController(initialPage: 0);

  var page = 0;
  BuildContext _context;
  AnimationController controller;
  Animation curve;
  Animation<int> alphaCount;
  Animation<double> alphaSize;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    curve = CurvedAnimation(parent: controller, curve: Curves.easeInOutBack);
    page = infinityPageController.realIndex;
    controller.addStatusListener((AnimationStatus animationStatus) {
      if (controller.isCompleted) {
        setState(() {
          animatePage();
        });
      }
    });
    _animParams = [AnimParams(), AnimParams()];
  }

  Widget mainWidget = Container();

  swiperMinus() {
    infinityPageController.animateToPage2(-1,
        duration: Duration(milliseconds: 300),
        curve: Curves.easeIn,
        context: _context);
  }

  swiperPlus() {
    infinityPageController.animateToPage2(1,
        duration: Duration(milliseconds: 300),
        curve: Curves.easeIn,
        context: _context);
  }

  double boxSize = 0;

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    boxSize =
        MediaQuery.of(context).size.width < MediaQuery.of(context).size.height
            ? MediaQuery.of(context).size.width
            : MediaQuery.of(context).size.height;

    boxSize = boxSize * 0.5;

    return InfinityPageView(
        itemCount: 2,
        onPageChanged: (p) {
          setState(() {
            mainWidget = widget.pageWidget;
          });
        },
        controller: infinityPageController,
        itemBuilder: (BuildContext context, int index) {
          var size =
              (index == infinityPageController.page && inited) ? boxSize : 0;
          //String val = mindClass.swipeCount.toString();
          if (controller.isAnimating && !controller.isCompleted) {
            //val = (alphaCount.value > 0 ? alphaCount.value : 0).toString();
            size = alphaSize.value > 0 ? alphaSize.value : 0;
          }
          return AnimatedSwitcher(
              duration: Duration(milliseconds: 300), child: mainWidget);
        });
  }

  AnimParams _newAnimParams;
  List<AnimParams> _animParams;

  Widget newPage = Container();
  double newBias;

  void renderNewPage() {
    _animParams[infinityPageController.page == 0 ? 1 : 0].toDefault();
    setState(() {});
  }

  void resetAnimation() {}

  void animatePage() {
    setState(() {
      _animParams[infinityPageController.page]
          .toGrowForm(Size(boxSize, boxSize));

      _animParams[infinityPageController.page == 0 ? 1 : 0].toDefault();
    });
  }
}

class AnimParams {
  double shatterIntensity = 0;
  Size growSize = Size(0, 0);

  Duration animationTime = Duration(milliseconds: 1000);

  void toDefault() {
    shatterIntensity = 0;
    growSize = Size(0, 0);

    animationTime = Duration(milliseconds: 1000);
  }

  void toGrowForm(size) {
    animationTime = Duration(milliseconds: 1000);
    growSize = size;
  }
}
