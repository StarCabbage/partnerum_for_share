import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:partnerum/utils/utils.dart';

class AppLocalizations {
  final Locale locale;

  AppLocalizations(this.locale);

  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  static const LocalizationsDelegate<AppLocalizations> delegate =
      _AppLocalizationsDelegate();

  Map<String, dynamic> _localizedStrings;

  Future<bool> load() async {
    // Load the language JSON file from the "lang" folder
    //print(locale.languageCode);
    var jsonString =
        await rootBundle.loadString('assets/langs/$locale.json');
    Map<String, dynamic> jsonMap = json.decode(jsonString);

    _localizedStrings = jsonMap;

    return true;
  }

  String translate(String key, [List args = const []]) {
    if (key.contains('.')) {
      var _localizedMap = _localizedStrings;
      List keyPoints = key.split('.');
      for (var i = 0, length = keyPoints.length - 1; i < length; i++) {
        _localizedMap = _localizedMap[keyPoints[i]];
      }
      return Utils.smartReplacer(
          _localizedMap[keyPoints.last].toString(), args);
    }
    return Utils.smartReplacer(_localizedStrings[key].toString(), args);
  }

  String t(String key, [List args = const []]) {
    return translate(key, args);
  }
}

String t(BuildContext context, String key, [List args = const []]) {
  return AppLocalizations.of(context).t(key, args);
}

class _AppLocalizationsDelegate
    extends LocalizationsDelegate<AppLocalizations> {
  const _AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) {
    // Include all of your supported language codes here TODO make simpler
    return ['en', 'sk', 'ru'].contains(locale.languageCode);
  }

  @override
  Future<AppLocalizations> load(Locale locale) async {
    var localizations = AppLocalizations(locale);
    await localizations.load();
    return localizations;
  }

  @override
  bool shouldReload(_AppLocalizationsDelegate old) => false;
}
