import 'dart:async';
import 'dart:collection';

import 'package:partnerum/models/rooms_model.dart';
import 'package:partnerum/models/user_info_model.dart';
import 'brain_module.dart';
import 'package:db_partnerum/db_partnerum.dart';

enum AdminVariables { adminsList }

extension admin on MindClass {
  List<PossibleUser> adminList([force = false]) {
    if (_checkUser()) return null;
    if (!futuresList.containsKey('adminList') &&
        (((ramData[AdminVariables.adminsList]??[]) as List).isEmpty || force)) {
      _streamController.add('upload');
      /*
      futuresList['adminList'] =
          rootReference.child('admins_list').once().then((value) async {
        Future<List<PossibleAdmin>> getUsersInfo(Map<String, int> keys) async {
          var users = <PossibleAdmin>[];
          for (MapEntry admin in keys.entries) {
            /*
            users.add(PossibleAdmin(admin.key, await userInfo(admin.key)));

             */
            users.last.level = admin.value;
          }
          return users;
        }

        if (value.value != null) {
          ramData[AdminVariables.adminsList] =
              await getUsersInfo(Map<String, int>.from(value.value));
          _streamController.add('done');
          futuresList.remove('adminList');
        }
      });
      */
    }
    return ramData[AdminVariables.adminsList] ?? [];
  }
  List<PossibleUser> usersList([force = false]) {
    if (_checkUser()) return null;
    if (!futuresList.containsKey('adminList') &&
        (((ramData[AdminVariables.adminsList]??[]) as List).isEmpty || force)) {
      _streamController.add('upload');
      /*
      futuresList['adminList'] =
          rootReference.child('admins_list').once().then((value) async {
        Future<List<PossibleAdmin>> getUsersInfo(Map<String, int> keys) async {
          var users = <PossibleAdmin>[];
          for (MapEntry admin in keys.entries) {
            /*
            users.add(PossibleAdmin(admin.key, await userInfo(admin.key)));

             */
            users.last.level = admin.value;
          }
          return users;
        }

        if (value.value != null) {
          ramData[AdminVariables.adminsList] =
              await getUsersInfo(Map<String, int>.from(value.value));
          _streamController.add('done');
          futuresList.remove('adminList');
        }
      });
      */
    }
    return ramData[AdminVariables.adminsList] ?? [];
  }

  get globalContactMode {
    if (_checkUser()) return null;
    return adminModel?.globalContactMode ?? false;
  }

  set globalContactMode(value) {
    if (_checkUser()) return null;
    /*
    rootReference
        .child(DatabasePaths.user_data)
        .child(user.uid)
        .child('user_info')
        .child('global_contact_mode')
        .set(value)
        .then((_) {
      if (value == true) {
        globalContactRole = Roles.first;
      } else {
        globalContactRole = null;
      }
    }).then((value) => update());

     */
  }

  String get globalContactRole {
    if (_checkUser()) return null;
    return adminModel?.globalContactRole;
  }

  set globalContactRole(value) {
    if (_checkUser()) return null;
    /*
    mindClass.rootReference
        .child('global_contacts')
        .child(mindClass.user.uid)
        .set(value)
        .then((value) => update());

     */
  }

  setStreamController(StreamController<dynamic> streamController) {
    _streamController = streamController;
  }

  update() async {
    _streamController.add('upload');
    adminModel = await this.getAdminModel();
    _streamController.add('done');
  }

  Future<AdminModel> getAdminModel([uid]) async {
    if (_checkUser()) return null;
    /*
    return AdminModel.fromJson((await rootReference
            .child(DatabasePaths.user_data)
            .child(uid ?? user.uid)
            .child('user_info')
            .once())
        .value);

     */
  }

  bool _checkUser() {
    return user == null;
  }
}

Map<String, Future> futuresList = {};
AdminModel adminModel;
StreamController _streamController;

class AdminModel extends UserModel {
  bool globalContactMode = false;
  String globalContactRole = Roles.first;
  List<PossibleUser> admins;
/*
  AdminModel.fromJson(json) : super.fromJson(json) {
    adminFromJson(json == null ? {} : Map.from(json));
    Future.wait([loadGlobalContactRole()])
        .then((value) => _streamController.add('done'));
  }
*/
  Future loadGlobalContactRole() async {
    /*
    globalContactRole = (await mindClass.rootReference
                .child('global_contacts')
                .child(mindClass.user.uid)
                .once())
            .value ??
        Roles.first;
    return Future.value();

     */
  }

  adminFromJson(Map<String, dynamic> json) {
    globalContactMode = json['global_contact_mode'];
  }
}

const List<String> Roles = ['Поддержка', 'Админ'];
