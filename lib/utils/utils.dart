import 'dart:async';

import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:vibration/vibration.dart';

class Utils {
  static String smartReplacer(String string, List data) {
    if (data.isNotEmpty) {
      var dataTime = List.from(data);
      dataTime.removeAt(0);
      return smartReplacer(
          string.replaceFirst('{}', data.first.toString()), dataTime);
    } else {
      return string;
    }
  }
}

Future<void> vibrate() => Vibration.vibrate(duration: 10);

Timer extraSlow() => Timer(Duration(seconds: 2), () {
      print('extraSlowAnimation');
      return timeDilation = 100.0;
    });
